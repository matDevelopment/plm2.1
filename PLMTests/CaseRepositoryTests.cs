using System;
using Thread = System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Moq;
using PLM;
using PLM.Models;
using PLM.Repositories;
using Xunit;
using PLM.Helpers;
using AutoMapper;

namespace PLMTests
{
    public class CaseRepositoryTests
    {
        private readonly PLMContext _context;

        public CaseRepositoryTests()
        {
        }

        [Fact]
        public async void IsNewCaseAdded_When_ProvidedWithCase()
        {
            var options = new DbContextOptionsBuilder<PLMContext>()
                .UseInMemoryDatabase(databaseName: "Add_writes_to_database")
                .Options;

            var optionsDeleted = new DbContextOptionsBuilder<PLMDeletedContext>()
                .UseInMemoryDatabase(databaseName: "Add_writes_to_database")
                .Options;

            var context = new PLMContext(options);

            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<AutoMapperProfiles>();
                cfg.AddProfile(new AutoMapperProfiles());
            });

            var mapper = new Mapper(config);

            var deletedContext = new PLMDeletedContext(optionsDeleted);

            var emailRepository = new Mock<EmailRepository>();

            var queryHelper = new QueryHelpers(context, mapper);

            var caseToAdd = new Case {
                Name = "Test",
                Description = "Test",
                StatusId = 2,
                PriorityId = 1,
                ClientId = 1,
                StartDate = new DateTime(2019, 1, 18),
                EndDate = new DateTime(2019, 1, 18),
                TimeEstimated = 0,
                TimeTaken = 0,
                CaseTagParentId = 1,
                CaseTagChildId = 1,
                TasksTime = 0,
                RepresentativeId = null,
                TemplateId = null,
                Order = "00-093",
                TasksCount = 0,
            };

            var service = await new CaseRepository(context, deletedContext, queryHelper).AddNewCase(caseToAdd);
            Assert.Equal(caseToAdd.Name, service.Name);
            Assert.Equal(caseToAdd.Order, service.Order);
        }
    }
}
