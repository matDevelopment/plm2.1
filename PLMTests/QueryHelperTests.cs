using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PLM;
using PLM.Helpers;
using Xunit;

namespace PLMTests.obj
{

    public class QueryHelperTests
    {
        [Fact]
        public void GetGroupIdsForUser_When_ProvidedWithUserId()
        {
            var options = new DbContextOptionsBuilder<PLMContext>()
                    .UseInMemoryDatabase(databaseName: "Add_writes_to_database")
                    .Options;

            var optionsDeleted = new DbContextOptionsBuilder<PLMDeletedContext>()
                .UseInMemoryDatabase(databaseName: "Add_writes_to_database")
                .Options;

            var context = new PLMContext(options);

             var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<AutoMapperProfiles>();
                cfg.AddProfile(new AutoMapperProfiles());
            });

            var group = new PLM.Models.Group { Name="Sample Group 1"};
            var group2 = new PLM.Models.Group { Name="Sample Group 2"};
            context.Groups.Add(group);
            context.Groups.Add(group2);
            context.SaveChanges();

            var user = new PLM.Models.User { Name="Sample User" };
            context.Users.Add(user);
            context.SaveChanges();

            context.GroupsUsers.Add(new PLM.Models.GroupUser { User = user, Group = group });
            context.GroupsUsers.Add(new PLM.Models.GroupUser { User = user, Group = group2});
            context.SaveChanges();
            var mapper = new Mapper(config);

            var queryHelper = new QueryHelpers(context, mapper);

            var ids = queryHelper.GetGroupsIdsForUser(user.Id);
            Assert.Equal(ids[0], group.Id);
            Assert.Equal(ids[1], group2.Id);
        }
    }
}