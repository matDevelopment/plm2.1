using PLM.DtosModels;

namespace PLM.Dtos
{
    public class DirectoryDto
    {
        public string Name { get; set; }
        public bool HasChildren { get; set; }
    }
}