using System.ComponentModel.DataAnnotations;

namespace PLM.Dtos
{
    public class UserForLoginDto
    {
        [Required]
        public string Name { get; set; }
        public string Password { get; set; }
    }
}