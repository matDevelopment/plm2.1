namespace PLM.Dtos
{
    public class RolesDto
    {
        public string Name { get; set; }
        public string[] Roles { get; set; }
    }
}