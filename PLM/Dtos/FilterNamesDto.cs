using System;

namespace PLM.Dtos
{
    public class FilterNamesDto
    {
        public string Priority { get; set; }
        public string Status { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? EcpDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? FinishTime { get; set; }
        public int? TimeEstimated { get; set; }
        public int? TimeTaken { get; set; }
        public string User { get; set; }
        public string Group { get; set; }
        public string Case { get; set; }
        public string Task { get; set; }
        public string TaskUser { get; set; }
        public string UserName { get; set; }
        public string CaseTagParent { get; set; }
        public string CaseTagChild { get; set; }
        public string TaskTagMain { get; set; }
        public string TaskTagAccessory { get; set; }
        public string TaskTagAccessoryMinor { get; set; }
        public string TeamLeader { get; set; }
        public string LastSeenUser { get; set; }
    }
}