using System.Collections.Generic;
using PLM.DtosModels;

namespace PLM.Dtos
{
    public class CaseTasksDto
    {
        public CaseDto Case { get; set; }
        public List<TaskDto> Tasks { get; set; }
        public List<EcpDto> Ecps { get; set; }
    }
}