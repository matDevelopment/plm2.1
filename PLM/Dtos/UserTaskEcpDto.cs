﻿using PLM.DtosModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PLM.Dtos
{
    public class UserTaskEcpDto
    {
        public UserDto User { get; set; }
        public List<TaskDto> Tasks { get; set; }
        public List<EcpDto> Ecps { get; set; }
    }
}
