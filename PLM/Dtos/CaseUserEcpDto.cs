using System.Collections.Generic;
using PLM.DtosModels;

namespace PLM.Dtos
{
    public class CaseUserEcpDto
    {
        public CaseDto Case { get; set; }
        public List<UserDto> Users { get; set; }
        public List<EcpDto> Ecps { get; set; }
    }
}