using System.Collections.Generic;
using PLM.DtosModels;

namespace PLM.Dtos
{
    public class UserEcpsDto
    {
        public int Id { get; set; }
        public UserDto User { get; set; }
        public List<EcpDto> Ecps { get; set; }
    }
        
}