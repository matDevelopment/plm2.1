using System.Collections.Generic;
using PLM.DtosModels;

namespace PLM.Dtos
{
    public class EcpsDto
    {
        EcpDto[] Ecps { get; set; }
    }
}