using System.Collections.Generic;
using PLM.DtosModels;

namespace PLM.Dtos
{
    public class UserWithPaymentPeriodsDto
    {
        public IEnumerable<PaymentPeriodDto> PaymentPeriods { get; set; }
    }
}