namespace PLM.Dtos
{
    public class UserIdWithGroupIdDto
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }
    }
}