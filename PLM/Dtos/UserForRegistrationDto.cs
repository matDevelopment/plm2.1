using System.ComponentModel.DataAnnotations;

namespace PLM.Dtos
{
    public class UserForRegistrationDto
    {
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Surname{ get; set; }
        public string Email { get; set; }
    }
}