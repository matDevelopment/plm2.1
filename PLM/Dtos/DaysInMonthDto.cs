using System.Collections.Generic;
using PLM.DtosModels;

namespace PLM.Dtos
{
    public class DaysInMonthDto
    {
        public int Year;
        public int Month; 
        public int? UserId;
        public int? TeamLeaderId;
    }
}