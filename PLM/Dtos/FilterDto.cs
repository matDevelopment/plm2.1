using System;

namespace PLM.Dtos
{
    public class FilterDto
    {
        public int? PriorityId { get; set; }
        public int? StatusId { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? EcpDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? FinishTime { get; set; }
        public int? TimeEstimated { get; set; }
        public int? TimeTaken { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public int? CaseId { get; set; }
        public int? TaskId { get; set; }
        public int? TaskUserId { get; set; }
        public string UserName { get; set; }
        public int? CaseTagParentId { get; set; }
        public int? CaseTagChildId { get; set; }
        public int? TaskTagMainId { get; set; }
        public int? TaskTagAccessoryId { get; set; }
        public int? TaskTagAccessoryMinorId { get; set; }
        public int? TeamLeaderId { get; set; }
        public int? LastSeenUser { get; set; }
    }
}