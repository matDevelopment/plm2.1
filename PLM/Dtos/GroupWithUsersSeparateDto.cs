using PLM.DtosModels;

namespace PLM.Dtos
{
    public class GroupWithUsersSeparateDto
    {
        public GroupDto Group { get; set; }
        public UserDto User { get; set; }
    }
}