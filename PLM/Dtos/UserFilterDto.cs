using System;

namespace PLM.Dtos
{
    public class UserFilterDto
    {
        public int? CaseId { get; set; }
        public int? TaskId { get; set; }
        public DateTime? Date { get; set; }
        public int? TeamLeader { get; set; }
        public int? GroupId { get; set; }
    }
}