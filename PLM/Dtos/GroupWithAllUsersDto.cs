﻿using PLM.DtosModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PLM.Dtos
{
    public class GroupWithAllUsersDto
    {
        public GroupDto Group { get; set; }
        public List<UserDto> Users{ get; set; }
        public List<EcpDto> Ecps{ get; set; }
        public UserDto TeamLeader { get; set; }
    }
}
