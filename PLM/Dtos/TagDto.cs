using PLM.BaseModels;

namespace PLM.Dtos
{
    public class TagDto : TagBase
    {
        public int CaseTagParentId { get; set; }
        public int TaskTagMainId { get; set; }
        public int TaskTagAccessoryId { get; set; }
    }
}