namespace PLM.Dtos
{
    public class FiltersDto
    {
        public FilterDto Filter { get; set; }
        public FilterDto SubFilter { get; set; }
    }
}