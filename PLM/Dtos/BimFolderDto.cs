namespace PLM.Dtos
{
    public class BimFolderDto
    {
        public int Id { get; set; }
        public string FolderId { get; set; }
    }
}