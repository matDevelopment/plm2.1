using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PLM.Models;

namespace PLM
{
    public class PLMDeletedContext : IdentityDbContext<User, Role, int,
    IdentityUserClaim<int>,
    UserRole, IdentityUserLogin<int>,
    IdentityRoleClaim<int>,
    IdentityUserToken<int>>
    {
        public PLMDeletedContext(DbContextOptions<PLMDeletedContext> options) : base(options) { }
        public DbSet<Case> Cases { get; set; }
        public DbSet<Models.Task> Tasks { get; set; }
        public DbSet<TaskUser> TasksUsers { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupUser> GroupsUsers { get; set; }
        public DbSet<Priority> Priorites { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<CaseTagParent> CaseTagParents { get; set; }
        public DbSet<CaseTagChild> CaseTagChildren { get; set; }
        public DbSet<TaskTagMain> TaskTagsMain { get; set; }
        public DbSet<TaskTagAccessory> TaskTagsAccessory { get; set; }
        public DbSet<TaskTagAccessoryMinor> TaskTagsAccessoryMinor { get; set; }
        public DbSet<Ecp> Ecps { get; set; }
        public DbSet<TimePeriod> TimePeriods { get; set; }
        public DbSet<Memo> Memos { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<PaymentPeriod> PaymentPeriods { get; set; }
        public DbSet<Representative> Representatives { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<TaskTemplate> TaskTemplates { get; set; }
        public DbSet<LastSeen> LastSeen { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserRole>(userRole =>
            {

                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            modelBuilder.Entity<Case>()
            .HasMany(c => c.Ecps).WithOne(e => e.Case).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Case>()
            .HasMany(c => c.Tasks).WithOne(t => t.Case).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<PLM.Models.Task>()
            .HasMany(t => t.Ecps).WithOne(e => e.Task).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Notification>()
                .HasOne(n => n.Case);
            modelBuilder.Entity<Notification>()
                .HasOne(n => n.Task);
            modelBuilder.Entity<Notification>()
                .HasOne(n => n.Memo);

            modelBuilder.Entity<TaskUser>()
                .HasKey(bc => new { bc.UserId, bc.TaskId });

            modelBuilder.Entity<TaskUser>()
                .HasOne(tu => tu.Task)
                .WithMany(t => t.TasksUsers)
                .HasForeignKey(tu => tu.TaskId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TaskUser>()
                .HasOne(bc => bc.User)
                .WithMany(c => c.TasksUsers)
                .HasForeignKey(bc => bc.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}