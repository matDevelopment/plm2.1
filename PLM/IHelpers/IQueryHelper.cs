using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.Models;

namespace PLM.IHelpers
{
    public interface IQueryHelper
    {
        List<int> GetGroupsIdsForUser(int userId);
        Task<List<User>> GetUsersInGroup(int groupId);
        Task<List<Group>> GetGroupsForUser(int userId);
        Task<List<User>> GetUsersForTeamLeader(int userId);
        Task<List<Case>> GetFilteredCases(FilterDto caseFilter);
        Task<List<TaskDto>> GetFilteredTasks(FilterDto taskFilter);
        Task<IEnumerable<Ecp>> GetEcps(FilterDto filter);
        Task<List<User>> getUsersInGroup(int groupId);
        List<TaskDto> GetUsersForTasks(IEnumerable<Models.Task> tasks);
        TaskDto GetUsersForTask(Models.Task task);
    }
}