using System;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PLM.Helpers;
using PLM.IHelpers;
using PLM.IRepositories;
using PLM.Models;
using PLM.Repositories;

namespace PLM
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var setting = new JsonSerializerSettings
            {
                Formatting = Newtonsoft.Json.Formatting.Indented, // Just for humans
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new DefaultContractResolver()
            };

            IdentityBuilder builder = services.AddIdentityCore<User>(opt =>
            {
                opt.Password.RequireDigit = false;
                opt.Password.RequiredLength = 4;
                opt.Password.RequireLowercase = false;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = false;
                opt.Tokens.PasswordResetTokenProvider = TokenOptions.DefaultEmailProvider;
            }
            ).AddDefaultTokenProviders();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII
                            .GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            ForgeCreditentials.ForgeClientId = Configuration.GetSection("Forge:FORGE_CLIENT_ID").Value;
            ForgeCreditentials.ForgeClientSecret = Configuration.GetSection("Forge:FORGE_CLIENT_SECRET").Value;
            ForgeCreditentials.ForgeCallbackUrl = Configuration.GetSection("Forge:FORGE_CALLBACK_URL").Value;
            ForgeCreditentials.Urls = Configuration.GetSection("Forge:ASPNETCORE_URLS").Value;
            ValuesHelper.Adress = Configuration.GetSection("Addresses:Address").Value;
            ValuesHelper.FileDirectory = Configuration.GetSection("Addresses:FileDirectory").Value;

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
            builder.AddEntityFrameworkStores<PLMContext>();
            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();
            builder.AddSignInManager<SignInManager<User>>();

          services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireAdminRole", policy => policy.RequireRole(RolesHelper.Admin));
                options.AddPolicy("RequireProjectManagerRole", policy => policy.RequireRole(RolesHelper.Admin, RolesHelper.ProjectManager));
                options.AddPolicy("RequireTeamLeaderRole", policy => policy.RequireRole(RolesHelper.Admin, RolesHelper.ProjectManager, RolesHelper.TeamLeader));
                options.AddPolicy("RequireUserRole", policy => policy.RequireRole(RolesHelper.Admin, RolesHelper.ProjectManager, RolesHelper.TeamLeader, RolesHelper.User));
            }
            );

            services.AddMvc(options =>
            {
               /*var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter(policy));*/
            })

            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                });
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "FrontEnd/dist";
            });
            services.AddDbContext<PLMContext>(x => x.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<PLMDeletedContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DeletedConnection")));

            services.AddTransient<Seed>();

            services.AddTransient<ICaseRepository, CaseRepository>();
            services.AddTransient<ITaskRepository, TaskRepository>();
            services.AddTransient<IFilterRepository, FilterRepository>();
            services.AddTransient<IClientRepository, ClientRepository>();
            services.AddTransient<IGroupRepository, GroupRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ITagRepository, TagRepository>();
            services.AddScoped<IAuthRepository, AuthRepository>();
            services.AddTransient<IEcpRepository, EcpRepository>();
            services.AddTransient<IMemoRepository, MemoRepository>();
            services.AddTransient<INotificationRepository, NotificationRepository>();
            services.AddTransient<IEmailRepository, EmailRepository>();
            services.AddTransient<ITablesChartsRepository, TablesChartsRepository>();
            services.AddTransient<IQueryHelper, QueryHelpers>();
            services.AddTransient<ITemplateRepository, TemplateRepository>();
            services.AddTransient<ILastSeenRepository, LastSeenRepository>();
            services.AddTransient<IFileRepository, FileRepository>();
            

            services.AddAutoMapper();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, Seed seeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            seeder.SeedDatabase();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "FrontEnd";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });


        }
    }
}
