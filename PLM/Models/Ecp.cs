using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class Ecp : EcpBase
    {
        public User User { get; set; }
        public Case Case { get; set; }
        public Task Task { get; set; }
        public virtual ICollection<EcpEcpTag> EcpsEcpTags { get; set; }
    }
}