using PLM.BaseModels;

namespace PLM.Models
{
    public class TaskTagAccessoryMinor : TagBase
    {
        public TaskTagAccessory TaskTagAccessory { get; set; }
        public int TaskTagAccessoryId { get; set; }
    }
}