using System;
using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class Task : TaskBase
    {   
        public Case Case { get; set; }
        public Priority Priority { get; set; }
        public Status Status { get; set; }
        public Group Group { get; set; }
        public ICollection<TaskUser> TasksUsers { get; set; }
        public TaskTagMain TaskTagMain { get; set; }
        public TaskTagAccessory TaskTagAccessory { get; set; }
        public TaskTagAccessoryMinor TaskTagAccessoryMinor { get; set; }
        public ICollection<Ecp> Ecps { get; set; }
        public ICollection<LastSeen> LastSeen { get; set; }
        public ICollection<Notification> Notifications { get; set; }
    }
}