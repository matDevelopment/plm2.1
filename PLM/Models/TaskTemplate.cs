using PLM.BaseModels;

namespace PLM.Models
{
    public class TaskTemplate : TaskTemplateBase
    {
        public Case Case { get; set; }
        public Template Template { get; set; }
        public Priority Priority { get; set; }
        public TaskTagMain TaskTagMain { get; set; }
        public TaskTagAccessory TaskTagAccessory { get; set; }
        public TaskTagAccessoryMinor TaskTagAccessoryMinor { get; set; }
    }
}