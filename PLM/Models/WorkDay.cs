namespace PLM.Models
{
    public class WorkDay
    {
        public bool Worked { get; set; }
        public string Time { get; set; }
    }
}