using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class CaseTagParent : TagBase
    {
        public List<CaseTagChild> CaseTagChildren { get; set; }
        public List<TaskTagAccessory> TaskTagsAccessory { get; set; }
        public List<TaskTagAccessoryMinor> TaskTagsAccessoryMinor { get; set; }
        public List<TaskTagMain> TaskTagsMain{ get; set; }
    }
}