using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class EcpTag : TagBase
    {
        public virtual ICollection<EcpEcpTag> EcpsEcpTags { get; set; }
    }
}