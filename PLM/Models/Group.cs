using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class Group : GroupBase
    {
        public virtual ICollection<GroupUser> GroupsUsers { get; set; }
    }
}