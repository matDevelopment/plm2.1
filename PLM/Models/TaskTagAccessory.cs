using PLM.BaseModels;

namespace PLM.Models
{
    public class TaskTagAccessory : TagBase
    {
        public TaskTagMain TaskTagMain { get; set; }
        public int TaskTagMainId { get; set; }
    }
}