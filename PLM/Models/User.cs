using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using PLM.BaseModels;

namespace PLM.Models
{
    public class User : IdentityUser<int>
    {
        public string Name { get; set; }
        public string Domain { get; set; }
        public string Avatar { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public ICollection<TaskUser> TasksUsers { get; set; }
        public ICollection<GroupUser> GroupsUsers { get; set; }
        public ICollection<Case> Cases { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<Notification> Notifications { get; set; }
        public ICollection<PaymentPeriod> PaymentPeriods { get; set; }
    }
}