using PLM.BaseModels;

namespace PLM.Models
{
    public class LastSeen : LastSeenBase
    {
        public Case Case { get; set; }
        public Task Task { get; set; }
        public User User { get; set; }
    }
}