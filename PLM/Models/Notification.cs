using PLM.BaseModels;

namespace PLM.Models
{
    public class Notification : NotificationBase
    {
        public Case Case { get; set; }
        public Task Task { get; set; }
        public Memo Memo { get; set; }
        public User User { get; set; }
    }
}