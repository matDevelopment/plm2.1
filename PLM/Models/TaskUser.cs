using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class TaskUser : TaskUserBase
    {
        public User User { get; set; }
        public Task Task { get; set; }
    }
}