namespace PLM.Models
{
    public class GroupUser
    {
        public int Id { get; set; }
        public virtual Group Group { get; set; }
        public virtual User User { get; set; }
    }
}