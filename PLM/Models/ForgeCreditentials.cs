namespace PLM.Models
{
    public static class ForgeCreditentials
    {
        public static string ForgeClientId { get; set; }
        public static string ForgeClientSecret { get; set; }
        public static string ForgeCallbackUrl { get; set; }
        public static string Urls { get; set; }
    }
}