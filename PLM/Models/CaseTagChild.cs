using PLM.BaseModels;

namespace PLM.Models
{
    public class CaseTagChild : TagBase
    {
        public CaseTagParent CaseTagParent { get; set; }
        public int CaseTagParentId { get; set; }
    }
}