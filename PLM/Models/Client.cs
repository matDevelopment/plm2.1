using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class Client : ClientBase
    { 
        public ICollection<Case> Cases { get; set; }
        public ICollection<Representative> Representatives { get; set; }
    }
}