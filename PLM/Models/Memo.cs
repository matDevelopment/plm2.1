using System;
using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class Memo: MemoBase
    {
          public Group Group { get; set; }
          public User Reporter { get; set; }
          public User Assignee { get; set; }
          public ICollection<Notification> Notifications { get; set; }
    }


}