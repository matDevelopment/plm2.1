using PLM.BaseModels;

namespace PLM.Models
{
    public class PaymentPeriod : PaymentPeriodBase
    {
        public User User { get; set; }
    }
}