using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class Representative : RepresentativeBase
    { 
        public Client Client { get; set; }
        public ICollection<Case> Cases { get; set; }
    }
}