namespace PLM.Models
{
    public class TimePeriod
    {
        public int Id { get; set; }
        public string StartTime { get; set; }
    }
}