namespace PLM.Models
{
    public class EcpEcpTag
    {
        public int Id { get; set; }
        public virtual Ecp Ecp { get; set; }
        public virtual EcpTag EcpTag { get; set; }
    }
}