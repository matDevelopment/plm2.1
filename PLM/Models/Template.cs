using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class Template : TemplateBase
    {
        public ICollection<Case> Cases { get; set; }
        public ICollection<TaskTemplate> TaskTemplates { get; set; }
        public Priority Priority { get; set; }
        public Client Client { get; set; }
        public Representative Representative { get; set; }
        public CaseTagParent CaseTagParent { get; set; }
        public CaseTagChild CaseTagChild { get; set; }
    }
}