using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class Priority : PriorityBase
    { 
        public ICollection<Case> Cases { get; set; }
        public ICollection<Task> Tasks { get; set; }
    }
}