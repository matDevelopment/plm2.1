using System;
using System.Collections;
using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.Models
{
    public class Case : CaseBase
    {

        public Priority Priority { get; set; }
        public Status Status { get; set; }
        public Client Client { get; set; }
        public Group Group { get; set; }
        public User User { get; set; }
        public Representative Representative { get; set; }
        public ICollection<Task> Tasks { get; set; }
        public ICollection<Ecp> Ecps { get; set; }
        public CaseTagParent CaseTagParent { get; set; }
        public CaseTagChild CaseTagChild { get; set; }
        public Template Template { get; set; }
        public ICollection<LastSeen> LastSeen { get; set; }
        public ICollection<TaskTemplate> TaskTemplates { get; set; }
        public ICollection<Notification> Notifications { get; set; }
    }
}
