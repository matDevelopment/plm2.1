using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PLM.IRepositories;
using PLM.Models;
using PLM.Dtos;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using PLM.DtosModels;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using PLM.Helpers;
using System.Linq;


namespace PLM.Controllers
{

    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IConfiguration _config;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;
        private readonly IAuthRepository _authRepository;
        private readonly PLMContext _context;
        private EmailSender _emailSender = new EmailSender();

        public AuthController(IConfiguration config, UserManager<User> userManager, SignInManager<User> signInManager, IMapper mapper, IAuthRepository authRepository, PLMContext context)
        {
            _config = config;
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _authRepository = authRepository;
            _context = context;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UserForRegistrationDto userForRegistrationDto)
        {

            var userToCreate = new User
            {
                Name = userForRegistrationDto.Name,
                UserName = userForRegistrationDto.Name,
                FirstName = userForRegistrationDto.FirstName,
                Surname = userForRegistrationDto.Surname,
                Domain = "MAT",
                Email = userForRegistrationDto.Email,
            };

            var password = "";
            password = password.GenerateRandomString();
            password = "qwert1";
            var result = await _userManager.CreateAsync(userToCreate, password);
            var user = await _userManager.FindByNameAsync(userForRegistrationDto.Name);
            await _userManager.AddToRoleAsync(user, RolesHelper.User);

            var userToReturn = _mapper.Map<UserDto>(userToCreate);

            if (result.Succeeded)
            {
                _emailSender.SendEmail(new Email
                {
                    Body = password,
                    Subject = "Hasło",
                    Recivers = new List<string>() {
                        userToCreate.Email
                    }
                });
                return Ok(new { password });
            };

            return BadRequest("Error");
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]UserForLoginDto userForLoginDto)
        {
            var user = await _userManager.FindByNameAsync(userForLoginDto.Name);

            var result = await _signInManager.CheckPasswordSignInAsync(user, userForLoginDto.Password, false);

            if (result.Succeeded)
            {
                var appUser = await _userManager.Users.FirstOrDefaultAsync(u => u.NormalizedUserName == userForLoginDto.Name.ToUpper());
                var userForReturn = _mapper.Map<UserDto>(appUser);
                var token = await GenerateJwtToken(appUser);
                return Ok(new
                {
                    token,
                    userForReturn
                });
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet("active-directory/add")]
        public async Task<IActionResult> AddFromActiveDirectory()
        {
            return null;
        }

        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] UserForChangePasswordDto user)
        {

            var editedUser = await _userManager.FindByNameAsync(user.Username);
            var response = await _userManager.ChangePasswordAsync(editedUser, user.OldPassword, user.NewPassword);
            if (response.Succeeded)
                return Ok();
            else
                return BadRequest("błędne hasło");
        }

        [HttpPost("delete-user")]
        public async Task<IActionResult> RemoveUser([FromBody] UserDto user)
        {
            var editedUser = await _userManager.FindByNameAsync(user.UserName);
            var response = await _userManager.DeleteAsync(editedUser);
            if (response.Succeeded)
                return Ok();
            else
                return BadRequest("błędne hasło");
        }


        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody] UserDto user)
        {
            var editedUser = await _userManager.FindByNameAsync(user.UserName);
            var password = "";
            password = password.GenerateRandomString();
            var token = await _userManager.GeneratePasswordResetTokenAsync(editedUser);
            var response = await _userManager.ResetPasswordAsync(editedUser, token, password);
            _emailSender.SendEmail(new Email
            {
                Body = password,
                Subject = "Hasło",
                Recivers = new List<string>() {
                        editedUser.Email
                    },
            });
            if (response.Succeeded)
                return Ok(password);
            else
                return BadRequest("błędne hasło");

        }
        private async Task<string> GenerateJwtToken([FromBody]User user)
        {

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Name)
            };

            var roles = await _userManager.GetRolesAsync(user);
            var groupsId = from gr in _context.Groups
                           join groupUser in _context.GroupsUsers on gr.Id equals groupUser.Group.Id
                           where groupUser.User.Id == user.Id
                           select new { Id = gr.Id.ToString() };

            var groupsIdString = "";
            foreach (var groupId in groupsId.ToList())
            {
                groupsIdString = groupsId.Count() > 1 ? groupsIdString + groupId.Id.ToString() + "," : groupId.Id.ToString();
            }

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
                claims.Add(new Claim(ClaimTypes.GroupSid, groupsIdString));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            //JWT Token descriptor: 

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token); ;
        }

    }
}