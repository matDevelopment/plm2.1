using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.IRepositories;
using PLM.Models;
using PLM.Helpers;

namespace PLM.Controllers
{
    [Authorize(Policy = "RequireUserRole")]
    [Route("api/[controller]")]
    public class CasesController : Controller
    {
        private readonly ICaseRepository _caseService;
        private readonly IMapper _mapper;
        private readonly IEmailRepository _emailRepository;
        private readonly IFileRepository _fileRepository;
        private readonly INotificationRepository _notificationRepository;

        public CasesController(ICaseRepository caseService, IMapper mapper, IEmailRepository emailRepository, IFileRepository fileRepository,
        IHostingEnvironment env, INotificationRepository notificationRepository)
        {
            _caseService = caseService;
            _mapper = mapper;
            _emailRepository = emailRepository;
            _fileRepository = fileRepository;
            _notificationRepository = notificationRepository;
        }

        [HttpGet()]
        public async Task<IActionResult> GetAllCases()
        {
            try
            {
                return Ok(await _caseService.GetCases());
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> AddCase([FromBody] CaseDto @case)
        {
            try
            {
                var caseToReturn = await _caseService.AddNewCase(_mapper.Map<Case>(@case));
                _emailRepository.CreateEmail("Nowy Projekt", "Został stworzony nowy projekt dla ciebie: <a href='google.com'>Przejdź do projektu</a>", caseToReturn.User);
                var notification = new Notification() {
                    Message = NotificationMessagesHelper.CaseMessage(caseToReturn.Name),
                    CaseId = caseToReturn.Id,
                    UserId = caseToReturn.UserId,
                    SendTime = DateTime.Now,
                    Seen = false
                };
                await _notificationRepository.AddNotification(notification);
                _fileRepository.AddDirectory(ValuesHelper.FileDirectory + "//" + caseToReturn.FolderId);
                return Ok(_mapper.Map<CaseDto>(caseToReturn));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }
        [HttpPost]
        [Route("filter")]
        public async Task<IActionResult> GetFilteredCases([FromBody] FilterDto caseFilter)
        {
            try
            {
                return Ok(_mapper.Map<IEnumerable<CaseDto>>(await _caseService.GetFilteredCases(caseFilter)));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting filtered cases" + exe.Message);
            }
        }
        [HttpGet]
        [Route("get/{id}")]
        public IActionResult GetCaseById(int id)
        {
            try
            {
                var casesToReturn = _mapper.Map<IEnumerable<CaseDto>>(_caseService.GetCase(id));
                return Ok(casesToReturn);
            }

            catch (Exception exe)
            {
                return BadRequest("Error during getting case" + exe.Message);
            }
        }

        [HttpPost]
        [Route("edit")]
        public async Task<IActionResult> EditCase([FromBody] CaseDto @case)
        {
            try
            {
                var caseToReturn = _mapper.Map<Case>(@case);
                _emailRepository.CreateEmail("Nowy Projekt", "Został stworzony nowy projekt dla ciebie: <a href='google.com'>Przejdź do projektu</a>", caseToReturn.User);
                return Ok(_mapper.Map<CaseDto>(await _caseService.EditCase(caseToReturn)));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting filtered cases" + exe.Message);
            }
        }

        [HttpPost]
        [Route("remove")]
        public async Task<IActionResult> RemoveCase([FromBody] CaseDto @case)
        {
            try
            {
                var caseToReturn = _mapper.Map<Case>(@case);
                _fileRepository.RemoveDirectory(ValuesHelper.FileDirectory + "//" + caseToReturn.FolderId);
                return Ok(_mapper.Map<CaseDto>(await _caseService.RemoveCase(caseToReturn)));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting filtered cases" + exe.Message);
            }
        }

        [HttpPost]
        [Route("status")]
        public async Task<IActionResult> ChangeStatus([FromBody] CaseDto @case)
        {
            try
            {
                return Ok(_mapper.Map<CaseDto>(await _caseService.ChangeStatus(_mapper.Map<Case>(@case))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting filtered cases" + exe.Message);
            }
        }

        [HttpPost]
        [Route("bim360")]
        public async Task<IActionResult> ChangeBimFolderId([FromBody] BimFolderDto BimFolder)
        {
            try
            {
                var caseModel = await _caseService.SetBimFolderId(BimFolder.Id, BimFolder.FolderId);
                return Ok("Bim Folder Added");
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting filtered cases" + exe.Message);
            }
        }
    }
}