using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Controllers
{
    [Authorize(Policy = "RequireUserRole")]
    [Route("api/[controller]")]
    public class EcpsController : Controller
    {
        private readonly IEcpRepository _ecpRepository;
        private readonly IMapper _mapper;

        public EcpsController(IEcpRepository ecpRepository, IMapper mapper)
        {
            _ecpRepository = ecpRepository;
            _mapper = mapper;
        }

        [HttpGet("time-periods")]
        public async Task<IActionResult> GetTimePeriods()
        {
            try
            {
                return Ok(await _ecpRepository.GetTimePeriods());
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddEcps([FromBody] List<Ecp> ecps)
        {
            try
            {
                var mappedEcps = _mapper.Map<List<Ecp>>(ecps);
                return Ok(await _ecpRepository.AddEcps(mappedEcps));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        public async Task<IActionResult> GetEcps([FromBody] FilterDto filter)
        {
            try
            {
                return Ok(_mapper.Map<IEnumerable<EcpDto>>(await _ecpRepository.GetEcps(filter)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAllEcps()
        {
            try
            {
                var mapped = _mapper.Map<IEnumerable<EcpDto>>(await _ecpRepository.GetAllEcps());
                return Ok(mapped);
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("remove")]
        public async Task<IActionResult> RemoveEcps([FromBody] EcpDto ecp)
        {
            try
            {
                return Ok(await _ecpRepository.RemoveEcp(_mapper.Map<Ecp>(ecp)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("date")]
        public IActionResult GetTodaysDate() {
              try
            {
                return Ok(_ecpRepository.GetTodaysDate());
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("days-in-month")]
        public async Task<IActionResult> GetReportedDaysInMonth([FromBody] DaysInMonthDto daysInMonth) {
              try
            {
                return Ok(await _ecpRepository.GetReportedDaysInMonth(daysInMonth.Year, daysInMonth.Month));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("days-in-month-user")]
        public async Task<IActionResult> GetReportedDaysInMonthForUser([FromBody] DaysInMonthDto daysInMonth) {
              try
            {
                return Ok(await _ecpRepository.GetReportedDaysInMonthForUser(daysInMonth.Year, daysInMonth.Month, daysInMonth.UserId.Value));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
    }
}