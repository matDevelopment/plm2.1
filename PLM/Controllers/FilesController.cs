using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PLM.Dtos;
using PLM.Helpers;
using PLM.IRepositories;

namespace PLM.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class FilesController : Controller
    {
        private readonly IFileRepository _fileRepository;

        public FilesController(IFileRepository fileRepository, IHostingEnvironment env)
        {
            _fileRepository = fileRepository;
        }

        [HttpPost()]
        [Route("get")]
        public IActionResult GetFilesInDirectory([FromBody] PathDto path)
        {
            try
            {
                if (path.Id == "//root")
                {
                    return Ok(_fileRepository.GetFilesAndDirectories(ValuesHelper.FileDirectory));
                }
                return Ok(_fileRepository.GetFilesAndDirectories(ValuesHelper.FileDirectory + path.Id));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPut()]
        [Route("upload")]
        public async Task<IActionResult> UploadFileAsync()
        {
            var file1 = Request;
            var file = Request.Form.Files[0];
            var pathToSave = ValuesHelper.FileDirectory;

            if (Request.Form.Files[0].Name != "//root")
                pathToSave = pathToSave + Request.Form.Files[0].Name;
            else 
                pathToSave = pathToSave +  "//";

            if (file.Length > 0)
            {
                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }
            else
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpPost]
        [Route("download")]
        public IActionResult DownloadFile([FromBody] PathDto path)
        {
            string fileName;
            fileName = Path.GetFileName(path.Id);

            var reg = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(Path.GetExtension(fileName).ToLower());
            string contentType = "application/unknown";

            if (reg != null)
            {
                string registryContentType = reg.GetValue("Content Type") as string;

                if (!String.IsNullOrWhiteSpace(registryContentType))
                {
                    contentType = registryContentType;
                }
            }

            return File( path.Id, contentType, fileName);
        }

    }
}