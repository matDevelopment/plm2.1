using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.Dtos;
using PLM.IRepositories;

namespace PLM.Controllers
{
    [Authorize(Policy = "RequireUserRole")]
    [Route("api/[controller]")]
    public class FiltersController : Controller
    {
        private readonly IFilterRepository _filterRepository;

        public FiltersController(IFilterRepository filterRepository)
        {
            _filterRepository = filterRepository;
        }

        [HttpGet("priorities")]
        public async Task<IActionResult> GetPriorities()
        {
            try
            {
                return Ok(await _filterRepository.GetPriorities());
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }

        [HttpGet("statuses")]
        public async Task<IActionResult> GetStatuses()
        {
            try
            {
                return Ok(await _filterRepository.GetStatuses());
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }

        [HttpPost("names")]
        public async Task<IActionResult> GetFilters([FromBody] FilterDto filter)
        {
            try
            {
                return Ok(await _filterRepository.GetFilters(filter));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting Filters: " + exe.Message);
            }
        }
    }
}