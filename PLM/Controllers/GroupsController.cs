using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Controllers
{
    [Authorize(Policy = "RequireUserRole")]
    [Route("api/[controller]")]
    public class GroupsController : Controller
    {
        private readonly IGroupRepository _groupRepository;
        private readonly IMapper _mapper;

        public GroupsController(IGroupRepository groupRepository, IMapper mapper)
        {
            _groupRepository = groupRepository;
            _mapper = mapper;
        }

        [HttpGet()]
        public async Task<IActionResult> GetGroups()
        {
            try
            {
                var group = await _groupRepository.GetGroups();
                var groupsToReturn = _mapper.Map<IEnumerable<GroupDto>>(group);
                return Ok(groupsToReturn);
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
        [HttpGet("users")]
        public async Task<IActionResult> GetGroupsWithUsers()
        {
            try
            {
                return Ok(await _groupRepository.GetGroupsWithUsers());
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("add")]
        public async Task<IActionResult> AddGroup([FromBody]GroupDto group)
        {
            try
            {
                return Ok(await _groupRepository.AddGroup(group));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("set/team-leader")]
        public IActionResult SetGroupTeamLeader([FromBody]GroupDto group)
        {
            try
            {
                return Ok(_groupRepository.SetGroupTeamLeader(group));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("remove/team-leader")]
        public IActionResult RemoveTeamLeaderRole([FromBody]GroupDto group)
        {
            try
            {
                return Ok(_groupRepository.RemoveTeamLeaderFromGroup(group));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("remove")]
        public IActionResult RemoveGroup([FromBody]GroupDto group)
        {
            try
            {
                return Ok(_groupRepository.RemoveGroup(group.Id));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("filtered")]
        public async Task<IActionResult> GetFilteredGroups([FromBody]FilterDto filter)
        {
            try
            {
                return Ok(await _groupRepository.GetFilteredGroups(filter));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("remove/user")]
        public async Task<IActionResult> RemoveUserFromGroup([FromBody]UserIdWithGroupIdDto ids)
        {
            try
            {
                return Ok(await _groupRepository.RemoveUserFromGroup(ids.UserId, ids.GroupId));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
    }
}