using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.DtosModels;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Controllers
{
    [Authorize(Policy = "RequireUserRole")]
    [Route("api/[controller]")]
    public class TagsController : Controller
    {
        private readonly ITagRepository _tagRepository;
        private readonly IMapper _mapper;

        public TagsController(ITagRepository tagRepository, IMapper mapper)
        {
            _tagRepository = tagRepository;
            _mapper = mapper;
        }
        [HttpGet("case/child/{id}")]
        public async Task<IActionResult> GetCaseTagsChild(int id)
        {
            try
            {
                return Ok(await _tagRepository.GetCaseTagsChild(id));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpGet("case/parent")]
        public async Task<IActionResult> GetCaseTagsParent()
        {
            try
            {
                return Ok(await _tagRepository.GetCaseTagsParent());
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpGet("task/main/{id}")]
        public async Task<IActionResult> GetTaskTagsMain(int id)
        {
            try
            {
                return Ok(await _tagRepository.GetTaskTagsMain(id));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpGet("task/accessory/{id}")]
        public async Task<IActionResult> GetTaskTagsAccessory(int id)
        {
            try
            {
                return Ok(await _tagRepository.GetTaskTagsAccessory(id));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpGet("task/accessory/minor/{id}")]
        public async Task<IActionResult> GetTaskTagsAccessoryMinor(int id)
        {
            try
            {
                return Ok(await _tagRepository.GetTaskTagsAccessoryMinor(id));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("case/parent/add")]
        public async Task<IActionResult> AddCaseTagsParent([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.AddCaseTagParent(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("case/child/add")]
        public async Task<IActionResult> AddCaseTagsChild([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.AddCaseTagChild(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/main/add")]
        public async Task<IActionResult> AddTaskTagsMain([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.AddTaskTagMain(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/accessory/add")]
        public async Task<IActionResult> AddTaskTagsAccessory([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.AddTaskTagAccessory(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/accessory/minor/add")]
        public async Task<IActionResult> AddTaskTagsAccessoryMinor([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.AddTaskTagAccessoryMinor(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("case/parent/remove")]
        public async Task<IActionResult> RemoveCaseTagsParent([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.RemoveCaseTagParent(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("case/child/remove")]
        public async Task<IActionResult> RemoveCaseTagsChild([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.RemoveCaseTagChild(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/main/remove")]
        public async Task<IActionResult> RemoveTaskTagsMain([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.RemoveTaskTagMain(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/accessory/remove")]
        public async Task<IActionResult> RemoveTaskTagsAccessory([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.RemoveTaskTagAccessory(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/accessory/minor/remove")]
        public async Task<IActionResult> RemoveTaskTagsAccessoryMinor([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.RemoveTaskTagAccessoryMinor(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("case/parent/edit")]
        public async Task<IActionResult> EditCaseTagsParent([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.EditCaseTagParent(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("case/child/edit")]
        public async Task<IActionResult> EditCaseTagsChild([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.EditCaseTagChild(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/main/edit")]
        public async Task<IActionResult> EditTaskTagsMain([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.EditTaskTagMain(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/accessory/edit")]
        public async Task<IActionResult> EditTaskTagsAccessory([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.EditTaskTagAccessory(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/accessory/minor/edit")]
        public async Task<IActionResult> EditTaskTagsAccessoryMinor([FromBody] TagDto tag)
        {
            try
            {
                return Ok(await _tagRepository.EditTaskTagAccessoryMinor(tag));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/accessory/copy")]
        public async Task<IActionResult> CopyTaskTagAccessoryStructure([FromBody] TagDto tag)
        {
            try
            {
                return Ok(_mapper.Map<TagDto>(await _tagRepository.CopyTaskTagAccessoryStructure(_mapper.Map<TaskTagAccessory>(tag))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/main/copy")]
        public async Task<IActionResult> CopyTaskTagMainStructure([FromBody] TagDto tag)
        {
            try
            {
                return Ok(_mapper.Map<TagDto>(await _tagRepository.CopyTaskTagMainStructure(_mapper.Map<TaskTagMain>(tag))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/accessory/minor/copy")]
        public async Task<IActionResult> CopyTaskTagAccessoryMinorStructure([FromBody] TagDto tag)
        {
            try
            {
                return Ok(_mapper.Map<TagDto>(await _tagRepository.CopyTaskTagAccessoryMinorStructure(_mapper.Map<TaskTagAccessoryMinor>(tag))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }
    }
}