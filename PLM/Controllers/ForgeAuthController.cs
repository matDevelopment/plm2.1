
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Autodesk.Forge;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using PLM.Models;

namespace PLM.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class ForgeAuthController : Controller
    {
        [HttpGet]
        [Route("token")]
        public async Task<AccessToken> GetPublicTokenAsync()
        {
            Credentials credentials = await Credentials.FromSessionAsync(Request.Cookies, Response.Cookies);

            if (credentials == null)
            {
                base.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return new AccessToken();
            }

            // return the public (viewables:read) access token
            return new AccessToken()
            {
                access_token = credentials.TokenPublic,
                expires_in = (int)credentials.ExpiresAt.Subtract(DateTime.Now).TotalSeconds
            };
        }

        /// <summary>
        /// Response for GetPublicToken
        /// </summary>
        public struct AccessToken
        {
            public void ForgeAuthController(string access_token, int expires_in)
            {
                this.access_token = access_token;
                this.expires_in = expires_in;
            }
            public string access_token { get; set; }
            public int expires_in { get; set; }
        }

        [HttpGet]
        [Route("signout")]
        public IActionResult Singout()
        {
            // finish the session
            Credentials.Signout(base.Response.Cookies);

            return Redirect("/");
        }

        [HttpGet]
        [Route("url")]
        public IActionResult GetOAuthURL()
        {
            // prepare the sign in URL
            Scope[] scopes = { Scope.DataRead };
            ThreeLeggedApi _threeLeggedApi = new ThreeLeggedApi();
            var clientId = ForgeCreditentials.ForgeClientId;
            var callBackUrl = ForgeCreditentials.ForgeCallbackUrl;
            string oauthUrl = _threeLeggedApi.Authorize(
              clientId,
              oAuthConstants.CODE,
              callBackUrl,
              new Scope[] { Scope.DataRead, Scope.DataCreate, Scope.DataWrite, Scope.ViewablesRead });

            return Json(oauthUrl);
        }

        [HttpGet]
        [Route("callback/oauth")] // see Web.Config FORGE_CALLBACK_URL variable
        public async Task<IActionResult> OAuthCallbackAsync(string code)
        {
            // create credentials form the oAuth CODE
            Credentials credentials = await Credentials.CreateFromCodeAsync(code, Response.Cookies);

            return Redirect("/auth");
        }

        [HttpGet]
        [Route("clientid")] // see Web.Config FORGE_CALLBACK_URL variable
        public dynamic GetClientID()
        {
            return new { id = ForgeCreditentials.ForgeClientId };
        }
    }

    /// <summary>
    /// Store data in session
    /// </summary>

}