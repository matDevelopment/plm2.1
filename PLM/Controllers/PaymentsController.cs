using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.IRepositories;

namespace PLM.Controllers
{
    [Authorize(Policy = "RequireUserRole")]
    [Route("api/[controller]")]
    public class PaymentsController : Controller
    {
        private readonly IFilterRepository _filterRepository;

        public PaymentsController(IFilterRepository filterRepository)
        {
            _filterRepository = filterRepository;
        }

        [HttpGet("priorities")]
        public async Task<IActionResult> GetPriorities()
        {
            try
            {
                return Ok(await _filterRepository.GetPriorities());
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }

        [HttpGet("statuses")]
        public async Task<IActionResult> GetStatuses()
        {
            try
            {
                return Ok(await _filterRepository.GetStatuses());
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }
    }
}