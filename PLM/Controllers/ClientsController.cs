using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.DtosModels;
using PLM.IRepositories;

namespace PLM.Controllers
{
    [Route("api/[controller]")]

    public class ClientsController : Controller
    {
        private readonly IClientRepository _clientRepository;
        private readonly IMapper _mapper;

        public ClientsController(IClientRepository clientRepository, IMapper mapper)
        {
            _clientRepository = clientRepository;
            _mapper = mapper;
        }

        [Authorize(Policy = "RequireUserRole")]
        [HttpGet()]
        public async Task<IActionResult> GetClients()
        {
             try
            {
                return Ok(await _clientRepository.GetClients());
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [Authorize(Policy = "RequireProjectManagerRole")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetClient(int id)
        {
             try
            {
                return Ok(await _clientRepository.GetClient(id));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }

        [Authorize(Policy = "RequireProjectManagerRole")]
        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> AddClient([FromBody] ClientDto client)
        {
             try
            {
                return Ok(await _clientRepository.AddNewClient(_mapper.Map<Models.Client>(client)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [Authorize(Policy = "RequireProjectManagerRole")]
        [HttpPost]
        [Route("remove")]
        public async Task<IActionResult> RemoveClient([FromBody] Models.Client client)
        {
             try
            {
                return Ok(await _clientRepository.RemoveClient(client));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
        [Authorize(Policy = "RequireProjectManagerRole")]
        [HttpPost]
        [Route("edit")]
        public async Task<IActionResult> EditClient([FromBody] Models.Client client)
        {
             try
            {
                return Ok(await _clientRepository.EditClient(client));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [Authorize(Policy = "RequireUserRole")]
        [HttpGet]
        [Route("representatives/{id}")]
        public async Task<IActionResult> GetRepresentatives(int id)
        {
             try
            {
                return Ok(await _clientRepository.GetRepresentativesAsync(id));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
        [Authorize(Policy = "RequireProjectManagerRole")]
        [HttpPost]
        [Route("representatives/add")]
        public async Task<IActionResult> AddRepresentative([FromBody] Models.Representative representative)
        {
             try
            {
                return Ok(await _clientRepository.AddRepresentativeAsync(representative));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [Authorize(Policy = "RequireProjectManagerRole")]
        [HttpPost]
        [Route("representatives/remove")]
        public async Task<IActionResult> RemoveRepresentative([FromBody] Models.Representative representative)
        {
             try
            {
                return Ok(await _clientRepository.RemoveRepresentativeAsync(representative));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
        
        [Authorize(Policy = "RequireProjectManagerRole")]
        [HttpPost]
        [Route("representatives/edit")]
        public async Task<IActionResult> EditRepresentative([FromBody] Models.Representative representative)
        {
             try
            {
                return Ok(await _clientRepository.EditRepresentativesAsync(representative));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
    }
}