using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.Helpers;
using PLM.IRepositories;
using PLM.Models;
using PLM.Repositories;
using Plm = PLM.Models;

namespace PLM.Controllers
{
    [Authorize(Policy = "RequireUserRole")]
    [Route("api/[controller]")]
    public class TasksController : Controller
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IMapper _mapper;
        private readonly IFileRepository _fileRepository;
        private readonly IHostingEnvironment _env;
        private readonly INotificationRepository _notificationRepository;
        private readonly IEmailRepository _emailRepository;

        public TasksController(ITaskRepository taskRepository, IMapper mapper, IFileRepository fileRepository,
        IHostingEnvironment env, INotificationRepository notificationRepository, IEmailRepository emailRepository)
        {
            _taskRepository = taskRepository;
            _mapper = mapper;
            _fileRepository = fileRepository;
            _env = env;
            _notificationRepository = notificationRepository;
            _emailRepository = emailRepository;
        }

        [HttpGet()]
        public async Task<IActionResult> GetTasks()
        {
            try
            {
                return Ok(await _taskRepository.GetTasks());
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpGet("task/{caseId}")]
        public async Task<IActionResult> GetTasksForCase(int caseId)
        {
            try
            {
                return Ok(await _taskRepository.GetTasks());
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all tasks" + exe.Message);
            }
        }
        [HttpGet("task/task/{taskId}")]
        public async Task<IActionResult> GetTasGetTaskById(int taskId)
        {
            try
            {
                return Ok(await _taskRepository.GetTaskById(taskId));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting task" + exe.Message);
            }
        }


        [HttpGet("user/task/{userId}/{caseId}")]
        public async Task<IActionResult> GetTasksForCaseForUser(int userId, int caseId)
        {
            try
            {
                return Ok(await _taskRepository.GetTasks());
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> AddTask([FromBody] TaskDto task)
        {
            try
            {
                var taskToReturn = await _taskRepository.AddTask(task);
                foreach (var user in taskToReturn.Users)
                {
                    var notification = new Notification()
                    {
                        Message = NotificationMessagesHelper.TaskMessage(taskToReturn.Name),
                        TaskId = taskToReturn.Id,
                        UserId = user.Id,
                        SendTime = DateTime.Now,
                        Seen = false
                    };
                    await _notificationRepository.AddNotification(notification);
                }
                _emailRepository.CreateEmail("Nowe Zadanie", "Zostało stworzone nowe zadanie dla ciebie: <a href='google.com'>Przejdź do projektu</a>", taskToReturn.Users);
                _fileRepository.AddDirectory(ValuesHelper.FileDirectory + "//" + taskToReturn.FolderId);
                return Ok(taskToReturn);
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost]
        [Route("edit")]
        public async Task<IActionResult> EditTask([FromBody] TaskDto task)
        {
            try
            {
                return Ok(await _taskRepository.EditTask(task));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }

        [HttpPost]
        [Route("filter")]
        public async Task<IActionResult> GetFilteredTasks([FromBody] FilterDto taskFilter)
        {
            try
            {
                var tasksToReturn = await _taskRepository.GetFilteredTasks(taskFilter);
                return Ok(tasksToReturn);
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting filtered cases" + exe.Message);
            }
        }


        [HttpPost]
        [Route("remove")]
        public async Task<IActionResult> RemoveCase([FromBody] TaskDto task)
        {
            try
            {
                var taskToReturn = await _taskRepository.RemoveTask(task);
                _fileRepository.RemoveDirectory(ValuesHelper.FileDirectory + "//" + task.FolderId);
                return Ok(taskToReturn);
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting filtered cases" + exe.Message);
            }
        }

        [HttpPost]
        [Route("status")]
        public async Task<IActionResult> ChangeStatus([FromBody] TaskDto task)
        {
            try
            {
                return Ok(_mapper.Map<TaskDto>(await _taskRepository.ChangeStatus(_mapper.Map<Plm.Task>(task))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting filtered cases" + exe.Message);
            }
        }

        [HttpPost]
        [Route("bim360")]
        public async Task<IActionResult> ChangeBimFolderId([FromBody] BimFolderDto BimFolder)
        {
            try
            {
                var task = await _taskRepository.SetBimFolderId(BimFolder.Id, BimFolder.FolderId);
                return Ok("Bim Folder Added");
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting filtered cases" + exe.Message);
            }
        }
    }
}
