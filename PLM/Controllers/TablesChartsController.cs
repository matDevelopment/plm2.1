using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.Dtos;
using PLM.IRepositories;
using PLM.Helpers;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Text;

namespace PLM.Controllers
{
    [AllowAnonymous]
    [Route("api/tables")]
    public class TablesChartsController : Controller
    {
        private readonly ITablesChartsRepository _tablesChartsRepository;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;


        public TablesChartsController(ITablesChartsRepository tablesChartsRepository, IMapper mapper, IHostingEnvironment env)
        {
            _tablesChartsRepository = tablesChartsRepository;
            _mapper = mapper;
            _hostingEnvironment = env;
        }

        [HttpPost()]
        [Route("users")]
        public async Task<IActionResult> GetCasesWithUsersWithEcp([FromBody] FiltersDto filters)
        {
            try
            {
                return Ok(await _tablesChartsRepository.GetCasesWithUsers(filters.Filter, filters.SubFilter));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
        [HttpPost()]
        [Route("tasks")]
        public async Task<IActionResult> GetTasksWithUsersWithEcp([FromBody] FiltersDto filters)
        {
            try
            {
                return Ok(await _tablesChartsRepository.GetUsersWithTasks(filters.Filter, filters.SubFilter));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("groups")]
        public async Task<IActionResult> GetGroupsWithUsers([FromBody] FiltersDto filters)
        {
            try
            {
                return Ok(await _tablesChartsRepository.GetUsersForGroups(filters.Filter, filters.SubFilter));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("case-tasks")]
        public async Task<IActionResult> GetCasesWithTasks([FromBody] FiltersDto filters)
        {
            try
            {
                return Ok(await _tablesChartsRepository.GetCasesWithTasks(filters.Filter, filters.SubFilter));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("export-json")]
        public async Task<ActionResult> ExportExcelUsersWithTasks()
        {
            try
            {
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    byte[] fileMemoryStream = await _tablesChartsRepository.ExportJson(await reader.ReadToEndAsync(), _hostingEnvironment);
                    return File(fileMemoryStream, "application/vnd.ms-excel", "exceljson.xlsx");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}