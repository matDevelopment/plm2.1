using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.IHelpers;
using PLM.IRepositories;
using PLM.Models;
using PLM.Repositories;
using Plm = PLM.Models;

namespace PLM.Controllers
{
    [Route("api/[controller]")]
    public class NotificationsController : Controller
    {

        private readonly INotificationRepository _notificationRepository;
        private readonly IQueryHelper _queryHelper;
        private readonly IMapper _mapper;

        public NotificationsController(INotificationRepository notificationRepository, IQueryHelper queryHelper, IMapper mapper)
        {
            _notificationRepository = notificationRepository;
            _queryHelper = queryHelper;
            _mapper = mapper;
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddNotification([FromBody] NotificationDto notification)
        {
            try
            {
                return Ok(_mapper.Map<NotificationDto>(await _notificationRepository.AddNotification(_mapper.Map<Notification>(notification))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all cases" + exe.Message);
            }
        }
        [AllowAnonymous]
        [HttpGet("all/{userId}")]
        public async Task<IActionResult> GetNotificationForUser(int userId)
        {
            try
            {
                return Ok( _mapper.Map<IEnumerable<NotificationDto>>(await _notificationRepository.GetNotificationsForUser(userId)));
            }
            catch (Exception exe)
            {
                ModelState.AddModelError("general error", "Error during getting all notifications " + exe.Message);

                return BadRequest("Error during getting all notifications " + exe.Message);
            }
        }

        [HttpGet("unseen/{userId}")]
        public async Task<IActionResult> GetUnseenNotificationForUser(int userId)
        {
            try
            {
                return Ok(_mapper.Map<IEnumerable<NotificationDto>>(await _notificationRepository.GetUnseenNotificationsForUser(userId)));
            }
            catch (Exception exe)
            {
                ModelState.AddModelError("general error", "Error during getting all notifications " + exe.Message);

                return BadRequest("Error during getting all notifications " + exe.Message);
            }
        }

        [HttpGet("seen/{userId}")]
        public async Task<IActionResult> GetSeenNotificationsForUser(int userId)
        {
            return Ok(_mapper.Map<IEnumerable<NotificationDto>>(await _notificationRepository.GetSeenNotificationsForUser(userId)));
        }

        [HttpGet("set/seen/{notificationId}")]
        public async Task<IActionResult> SetNotificationToSeen(int notificationId)
        {
            return Ok(_mapper.Map<NotificationDto>(await _notificationRepository.GetSeenNotificationsForUser(notificationId)));
        }

        [AllowAnonymous]
        [HttpGet("Outlook")]
        public IActionResult OutlookCalendar()
        {
            return Ok();
        }
    }
}