using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.Helpers;
using PLM.IRepositories;
using PLM.Repositories;
using Plm = PLM.Models;

namespace PLM.Controllers
{
    [Authorize(Policy = "RequireUserRole")]
    [Route("api/[controller]")]
    public class MemosController : Controller
    {
        private readonly IMemoRepository _memoRepository;
        private readonly IMapper _mapper;
        private readonly IFileRepository _fileRepository;
        public MemosController(IMemoRepository memoRepository, IMapper mapper, IFileRepository fileRepository,
        IHostingEnvironment env)
        {
            _memoRepository = memoRepository;
            _mapper = mapper;
            _fileRepository = fileRepository;
        }

        [HttpGet("{memoId}")]
        public async Task<IActionResult> GetMemo(int memoId)
        {
            try
            {
                return Ok(await _memoRepository.GetMemo(memoId));
            }
            catch (Exception exe)
            {
                return BadRequest($"Error during getting memo with id {memoId}" + exe.Message);
            }
        }

        [HttpGet("{parentType}/{parentId}")]
        public async Task<IActionResult> GetMemosForParent(int parentId, string parentType)
        {
            try
            {
                return Ok(await _memoRepository.GetMemosForParent(parentId, parentType));
            }
            catch (Exception exe)
            {
                return BadRequest($"Error during getting memo for {parentType} with id {parentId}" + exe.Message);
            }
        }

        [HttpPost("new")]
        public async Task<IActionResult> AddNewMemo([FromBody] Plm.Memo newMemo)
        {
            try
            {
                var memo = await _memoRepository.AddNewMemo(newMemo);
                _fileRepository.AddDirectory(ValuesHelper.FileDirectory + "//" + memo.FolderId);
                return Ok(memo);
            }
            catch (Exception exe)
            {
                return BadRequest($"Error during createion  of memo" + exe.Message);
            }
        }
        [HttpPost()]
        public async Task<IActionResult> Update([FromBody] Plm.Memo updatedMemo)
        {
            try
            {
                return Ok(await _memoRepository.UpdateMemo(updatedMemo));
            }
            catch (Exception exe)
            {
                return BadRequest($"Error during update of memo" + exe.Message);
            }
        }
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteMemo([FromBody] Plm.Memo deletedMemo)
        {
            try
            {
                var memo = await _memoRepository.DeleteMemo(deletedMemo);
                 _fileRepository.RemoveDirectory(ValuesHelper.FileDirectory + "//" + memo.FolderId);
                return Ok(memo);
            }
            catch (Exception exe)
            {
                return BadRequest($"Error during deleting of memo" + exe.Message);
            }
        }
    }


}