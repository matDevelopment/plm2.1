using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PLM.IRepositories;
using PLM.DtosModels;
using PLM.Dtos;
using PLM.Models;
using PLM.Helpers;

namespace PLM.Controllers
{
    
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UsersController(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        [HttpGet()]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                return Ok(await _userRepository.GetUsers());
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("group/{groupId}")]
        public async Task<IActionResult> GetUsersForGroup(int groupId)
        {
            try
            {
                var users = await _userRepository.GetUsersForGroup(groupId);
                var usersToReturn = _mapper.Map<IEnumerable<UserDto>>(users);
                return Ok(usersToReturn);
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
        [HttpPost("group/add")]
        public async Task<IActionResult> AddUserToGroup([FromBody] GroupWithUsersSeparateDto groupWithUsers)
        {
            try
            {
                return Ok(await _userRepository.AddUserToGroup(_mapper.Map<User>(groupWithUsers.User), _mapper.Map<Group>(groupWithUsers.Group)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("group/remove")]
        public IActionResult RemoveUserFromGroup([FromBody] GroupWithUsersSeparateDto groupWithUsers)
        {
            try
            {
                _userRepository.RemoveUserFromGroup(_mapper.Map<User>(groupWithUsers.User), _mapper.Map<Group>(groupWithUsers.Group));
                return Ok();
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("roles")]
        public async Task<IActionResult> ChangeRoles([FromBody] RolesDto roles)
        {
            try
            {
                var result = await _userRepository.ChangeRole(roles.Name, roles.Roles);
                return Ok();
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("get/roles")]
        public async Task<IActionResult> GetRoles()
        {
            try
            {
                return Ok(await _userRepository.GetRoles());
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("user/{name}")]
        public async Task<IActionResult> GetUsersForUser(string name)
        {
            try
            {
                return Ok(_mapper.Map<IEnumerable<UserDto>>(await _userRepository.GetUsersForUser(name)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("ecp")]
        public async Task<IActionResult>  GetUsersForEcpDay([FromBody] FilterDto filter) {
            try
            {
                return Ok(_mapper.Map<IEnumerable<UserDto>>(await _userRepository.GetUsersForEcpDay(filter.Date ?? DateTime.Now, filter.UserName)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("filtered")]
        public async Task<IActionResult> GetFilteredUsers([FromBody] FilterDto filter) {
            try
            {
                return Ok(_mapper.Map<IEnumerable<UserDto>>(await _userRepository.GetFilteredUsers(filter)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("team-leader")]
        public async Task<IActionResult> GetTeamLeaders()
        {
            try
            {
                return Ok(_mapper.Map<IEnumerable<UserDto>>(await _userRepository.GetTeamLeaders()));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost("edit")]
        public async Task<IActionResult> EditUser([FromBody] UserDto user) {
            try
            {
                return Ok(_mapper.Map<UserDto>(await _userRepository.EditUser(_mapper.Map<User>(user))));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            try
            {
                return Ok(_mapper.Map<UserDto>(await _userRepository.GetUser(id)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
    }
}