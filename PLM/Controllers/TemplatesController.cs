using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.DtosModels;
using PLM.Models;
using PLM.Repositories;

namespace PLM.Controllers
{
  //  [Authorize(Policy = "RequireUserRole")]
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class TemplatesController : Controller
    {
        private readonly ITemplateRepository _templateRepository;
        private readonly IMapper _mapper;

        public TemplatesController(ITemplateRepository templateRepository, IMapper mapper)
        {
            _templateRepository = templateRepository;
            _mapper = mapper;
        }

        [HttpGet()]
        public async Task<IActionResult> GetTemplates()
        {
            try
            {
                return Ok(_mapper.Map<IEnumerable<Template>>(await _templateRepository.GetTemplates()));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTemplate(int id)
        {
            try
            {
                return Ok(_mapper.Map<Template>(await _templateRepository.GetTemplate(id)));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("add")]
        public async Task<IActionResult> AddTemplate([FromBody] TemplateDto template)
        {
            try
            {
                return Ok(_mapper.Map<Template>(await _templateRepository.AddTemplate(_mapper.Map<Template>(template))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }
        [HttpPost()]
        [Route("edit")]
        public async Task<IActionResult> EditTemplate([FromBody] TemplateDto template)
        {
            try
            {
                return Ok(_mapper.Map<Template>(await _templateRepository.EditTemplate(_mapper.Map<Template>(template))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }
        [HttpPost()]
        [Route("remove")]
        public async Task<IActionResult> RemoveTemplate([FromBody] TemplateDto template)
        {
            try
            {
                return Ok(_mapper.Map<Template>(await _templateRepository.RemoveTemplate(_mapper.Map<Template>(template))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }

        
        [HttpGet("task")]
        public async Task<IActionResult> GetTaskTemplates()
        {
            try
            {
                return Ok(_mapper.Map<IEnumerable<TaskTemplate>>(await _templateRepository.GetTaskTemplates()));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }

        [HttpGet("task/{id}")]
        public async Task<IActionResult> GetTaskTemplate(int id)
        {
            try
            {
                return Ok(_mapper.Map<TaskTemplate>(await _templateRepository.GetTaskTemplate(id)));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }

        [HttpPost()]
        [Route("task/add")]
        public async Task<IActionResult> AddTaskTemplate([FromBody] TaskTemplateDto template)
        {
            try
            {
                return Ok(_mapper.Map<TaskTemplate>(await _templateRepository.AddTaskTemplate(_mapper.Map<TaskTemplate>(template))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }
        [HttpPost()]
        [Route("task/edit")]
        public async Task<IActionResult> EditTaskTemplate([FromBody] TaskTemplateDto template)
        {
            try
            {
                return Ok(_mapper.Map<TaskTemplate>(await _templateRepository.EditTaskTemplate(_mapper.Map<TaskTemplate>(template))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }
        [HttpPost()]
        [Route("task/remove")]
        public async Task<IActionResult> RemoveTaskTemplate([FromBody] TaskTemplateDto template)
        {
            try
            {
                return Ok(_mapper.Map<TaskTemplate>(await _templateRepository.RemoveTaskTemplate(_mapper.Map<TaskTemplate>(template))));
            }
            catch (Exception exe)
            {
                return BadRequest("Error during getting all priorities" + exe.Message);
            }
        }
        [HttpPost()]
        [Route("case/copy")]
        public async Task<IActionResult> CopyCaseToTemplate([FromBody] CaseDto CaseDto)
        {
            try
            {
                return Ok(_mapper.Map<Template>(await _templateRepository.CopyCaseToTemplate(CaseDto.Id)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
        [HttpPost()]
        [Route("task/copy")]
        public async Task<IActionResult> CopyTaskToTaskTemplate([FromBody] TaskDto TaskDto)
        {
            try
            {
                return Ok(_mapper.Map<Template>(await _templateRepository.CopyTaskToTaskTemplate(TaskDto.Id)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
    }
}