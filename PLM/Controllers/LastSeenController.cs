using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLM.DtosModels;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Controllers
{
    [AllowAnonymous]
    [Route("api/last-seen")]
    public class LastSeenController : Controller
    {
        private readonly ILastSeenRepository _lastSeenRepository;
        private readonly IMapper _mapper;

        public LastSeenController(ILastSeenRepository lastSeenRepository, IMapper mapper)
        {
            _lastSeenRepository = lastSeenRepository;
            _mapper = mapper;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetLastSeen(int userId)
        {
            try
            {
                return Ok(_mapper.Map<IEnumerable<LastSeenDto>>(await _lastSeenRepository.GetLastSeen(userId)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }

        [HttpPost()]
        [Route("add")]
        public async Task<IActionResult> AddLastSeem([FromBody] LastSeenDto lastSeen)
        {
            try
            {
                return Ok(await _lastSeenRepository.AddLastSeen(_mapper.Map<LastSeen>(lastSeen)));
            }
            catch (Exception exe)
            {
                return BadRequest(exe.Message);
            }
        }
    }
}