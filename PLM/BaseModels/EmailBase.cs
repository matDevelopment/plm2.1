using System.Collections.Generic;

namespace PLM.BaseModels
{
    public class EmailBase
    {
        public List<string> Recivers { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Link { get; set; }
        public int ParentId { get; set; }
    }
}