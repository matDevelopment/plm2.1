namespace PLM.BaseModels
{
    public class FolderCountBase
    {
        public int Id { get; set; }
        public int Amount { get; set; }
    }
}