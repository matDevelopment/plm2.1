namespace PLM.BaseModels
{
    public class GroupBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public int? TeamLeaderId { get; set; }
        public int UsersAmount { get; set; }
    }
}