using System;

namespace PLM.BaseModels
{
    public class CaseBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Order { get; set; }
        public int TasksCount { get; set; }
        public int MemoCount { get; set; }
        public string Description { get; set; }
        public int? StatusId { get; set; } 
        public int? PriorityId { get; set; }
        public int? ClientId { get; set; }
        public int? UserId { get; set; }
        public int? GroupId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public float TimeEstimated { get; set; }
        public float TimeTaken { get; set; }
        public int TasksTime { get; set; }
        public int? CaseTagParentId { get; set; }
        public int? CaseTagChildId { get; set; }
        public int? NotificationId { get; set; }
        public int? RepresentativeId { get; set; }
        public int? TemplateId { get; set; }
        public string FolderId { get; set; }
        public string BimFolderId { get; set; }
    }
}