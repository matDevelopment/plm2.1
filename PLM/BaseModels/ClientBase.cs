namespace PLM.BaseModels
{
    public class ClientBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int Nip { get; set; }
    }
}