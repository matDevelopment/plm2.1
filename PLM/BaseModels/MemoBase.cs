using System;

namespace PLM.BaseModels
{
    public class MemoBase
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int? GroupId { get; set; }
        public DateTime Time { get; set; }
        public int ReporterId { get; set; }
        public int? AssigneeId { get; set; }
        public int NotificationId { get; set; }
        public string FolderId { get; set; }
    }

}