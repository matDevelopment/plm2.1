using System;

namespace PLM.BaseModels
{
    public class EcpBase
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? CaseId { get; set; }
        public int? TaskId { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartTime { get; set; }
        private DateTime finishTime;
        public DateTime FinishTime { get { return finishTime; } set {
            finishTime = value;
            var timeWorked = finishTime - StartTime;
            TimeWorkedHours = timeWorked.Hours;
            TimeWorkedMinutes = timeWorked.Minutes;
        }}
        public int TimeWorkedHours { get; set; }
        public int TimeWorkedMinutes { get; set; }
    }
}