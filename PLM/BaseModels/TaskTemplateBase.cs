namespace PLM.BaseModels
{
    public class TaskTemplateBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TaskName { get; set; }
        public int? CaseId { get; set; }
        public int? TemplateId { get; set; }
        public int? PriorityId { get; set; }
        public float TimeEstimated { get; set; }
        public int? TaskTagMainId { get; set; }
        public int? TaskTagAccessoryId { get; set; }
        public int? TaskTagAccessoryMinorId { get; set; }
        public int CaseTagId { get; set; }
        public int NameCount { get; set; }
    }
}