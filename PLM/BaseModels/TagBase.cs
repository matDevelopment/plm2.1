namespace PLM.BaseModels
{
    public class TagBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}