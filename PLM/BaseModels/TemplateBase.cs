namespace PLM.BaseModels
{
    public class TemplateBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Order { get; set; }
        public string CaseName { get; set; }
        public int? PriorityId { get; set; }
        public int? ClientId { get; set; }
        public float TimeEstimated { get; set; }
        public int? CaseTagParentId { get; set; }
        public int? CaseTagChildId { get; set; }
        public int? RepresentativeId { get; set; }
        public int CasesCount { get; set; }
    }
}