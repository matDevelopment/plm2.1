using System;

namespace PLM.BaseModels
{
    public class NotificationBase
    {
        public int Id { get; set; }
        public int? TaskId { get; set; }
        public int? CaseId { get; set; }
        public int? MemoId { get; set; }
        public string Message { get; set; }
        public DateTime SendTime { get; set; }
        public bool Seen { get; set; }
        public int? UserId { get; set; }
    }
}