using System;

namespace PLM.BaseModels
{
    public class PaymentPeriodBase
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime StarDate { get; set; }
        public DateTime FinishDate { get; set; }
        public int Brutto { get; set; }
        public int Netto { get; set; }
    }
}