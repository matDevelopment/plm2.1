using System;

namespace PLM.BaseModels
{
    public class TaskBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Order { get; set; }
        public string Description { get; set; }
        public int? StatusId { get; set; } 
        public int? PriorityId { get; set; }
        public int? CaseId { get; set; }
        public int? GroupId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public float TimeEstimated { get; set; }
        public float TimeTaken { get; set; }
        public int? TaskTagMainId { get; set; }
        public int? TaskTagAccessoryId { get; set; }
        public int? TaskTagAccessoryMinorId { get; set; }
        public int? NotificationId { get; set; }
        public int? DestinatedEmployees { get; set; }
        public string FolderId { get; set; }
        public string BimFolderId { get; set; }
        public int MemoCount { get; set; }
    }
}