namespace PLM.BaseModels
{
    public class TaskUserBase
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int UserId { get; set; }
    }
}