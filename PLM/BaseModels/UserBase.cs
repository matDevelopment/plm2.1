namespace PLM.BaseModels
{
    public class UserBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Domain { get; set; }
        public string Avatar { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public bool IsVisible { get; set; }
    }
}