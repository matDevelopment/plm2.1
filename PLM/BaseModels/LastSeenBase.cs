using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PLM.BaseModels
{
    public class LastSeenBase
    {
        public int Id { get; set; }
        public int? CaseId { get; set; }
        public int? TaskId { get; set; }
        public int UserId { get; set; }
        public string Type { get; set; }
    }

}