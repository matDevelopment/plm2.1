using System;

namespace PLM.Helpers
{
    public static class NotificationMessagesHelper
    {
        public static string CaseMessage(string caseName) {
            return String.Format("Nowy projekt - {0} - został do ciebie przypisany", caseName);
        }

        public static string TaskMessage(string taskName) {
            return String.Format("Nowe zadanie - {0} - został do ciebie przypisane", taskName);
        }

        public static string MemoMessage(string memoName) {
            return  String.Format("Nowa notatka - {0} - została do ciebie przypisana", memoName);
        }

    }
}