namespace PLM.Helpers
{
    public class RolesHelper
    {
        public const string Admin = "Administrator";
        public const string ProjectManager = "Menadżer Projektu";
        public const string TeamLeader = "Lider Zespołu";
        public const string User = "Użytkownik";
    }
}