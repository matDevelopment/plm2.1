using AutoMapper;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.Models;

namespace PLM.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();

            CreateMap<User, UserWithPaymentPeriodsDto>();
            CreateMap<UserWithPaymentPeriodsDto, User>();

            CreateMap<Group, GroupDto>();
            CreateMap<GroupDto, Group>();

            CreateMap<CaseTagChild, CaseTagChildDto>();
            CreateMap<CaseTagChildDto, CaseTagChild>();

            CreateMap<CaseTagParent, CaseTagParentDto>();
            CreateMap<CaseTagParentDto, CaseTagParent>();

            CreateMap<Status, StatusDto>();
            CreateMap<StatusDto, Status>();

            CreateMap<Priority, PriorityDto>();
            CreateMap<PriorityDto, Priority>();

            CreateMap<Client, ClientDto>();
            CreateMap<ClientDto, Client>();

            CreateMap<Case, CaseDto>();
            CreateMap<CaseDto, Case>();

            CreateMap<CaseTagChild, CaseTagChildDto>();
            CreateMap<CaseTagChildDto, CaseTagChild>();

            CreateMap<CaseTagParent, CaseTagParentDto>();
            CreateMap<CaseTagParentDto, CaseTagParent>();

            CreateMap<Task, TaskDto>();
            CreateMap<TaskDto, Task>();

            CreateMap<TaskTagMain, TaskTagMainDto>();
            CreateMap<TaskTagMainDto, TaskTagMain>();

            CreateMap<TaskTagAccessory, TaskTagAccessoryDto>();
            CreateMap<TaskTagAccessoryDto, TaskTagAccessory>();

            CreateMap<TaskTagAccessoryMinor, TaskTagAccessoryMinorDto>();
            CreateMap<TaskTagAccessoryMinorDto, TaskTagAccessoryMinor>();

            CreateMap<Ecp, EcpDto>();
            CreateMap<EcpDto, Ecp>();

            CreateMap<PaymentPeriod, PaymentPeriodDto>();
            CreateMap<PaymentPeriodDto, PaymentPeriod>();

            CreateMap<Representative, RepresentativeDto>();
            CreateMap<RepresentativeDto, Representative>();

            CreateMap<Template, TemplateDto>();
            CreateMap<TemplateDto, Template>();

            CreateMap<TaskTemplate, TaskTemplateDto>();
            CreateMap<TaskTemplateDto, TaskTemplate>();

            CreateMap<Notification, NotificationDto>();
            CreateMap<NotificationDto, Notification>();

            CreateMap<Memo, MemoDto>();
            CreateMap<MemoDto, Memo>();

            CreateMap<DtosModels.TagDto, TaskTagMain>();
            CreateMap<TaskTagMain, DtosModels.TagDto>();

            CreateMap<DtosModels.TagDto, TaskTagAccessory>();
            CreateMap<TaskTagAccessory, DtosModels.TagDto>();

            CreateMap<DtosModels.TagDto, TaskTagAccessoryMinor>();
            CreateMap<TaskTagAccessoryMinor, DtosModels.TagDto>();

            CreateMap<LastSeenDto, LastSeen>();
            CreateMap<LastSeen, LastSeenDto>();
        }
    }
}