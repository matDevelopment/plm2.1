using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using PLM.Models;

namespace PLM.Helpers
{
    public class EmailSender
    {
        public string GetIPAddress()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[1];

            return ipAddress.ToString();
        }
        public void SendEmail(Email email)
        {
            object userToken = new object();
            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            foreach (var reciver in email.Recivers)
            {
                message.To.Add(reciver);
            }
          
            message.Subject = email.Subject;
            message.From = new MailAddress("testusermatnet@gmail.com");
            message.Body = email.Body + "<br>" + email.Link; 

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",               
                EnableSsl = true,                
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("testusermatnet", "!!qaz123")
            };
            smtp.SendAsync(message, userToken);
        }
    }
}