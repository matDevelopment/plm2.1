using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using PLM.IHelpers;
using PLM.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PLM.Dtos;
using PLM.DtosModels;

namespace PLM.Helpers
{
    public class QueryHelpers : IQueryHelper
    {
        private readonly PLMContext _context;
        private readonly IMapper _mapper;

        public QueryHelpers(PLMContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<int> GetGroupsIdsForUser(int userId)
        {
            var groupsId = from gr in _context.Groups
                           join groupUser in _context.GroupsUsers on gr.Id equals groupUser.Group.Id
                           where groupUser.User.Id == userId
                           select gr.Id;
            return groupsId.ToList();
        }

        public async Task<List<User>> GetUsersInGroup(int groupId)
        {
            var userInGroup = from user in _context.Users
                              join groupUser in _context.GroupsUsers on user.Id equals groupUser.User.Id
                              where groupUser.Group.Id == groupId
                              select _mapper.Map<User>(user);
            return await userInGroup.ToListAsync();
        }

        public async Task<List<Group>> GetGroupsForUser(int userId)
        {
            var groupsForUser = from groupModel in _context.Groups
                                join groupUser in _context.GroupsUsers on groupModel.Id equals groupUser.Group.Id
                                where groupUser.User.Id == userId
                                select _mapper.Map<Group>(groupModel);
            return await groupsForUser.ToListAsync();
        }

        public async Task<List<User>> GetUsersForTeamLeader(int userId)
        {
            var usersForTeamLeader = from user in _context.Users
                                     join groupUser in _context.GroupsUsers on user.Id equals groupUser.User.Id
                                     where groupUser.Group.TeamLeaderId == userId
                                     select _mapper.Map<User>(user);

            var userForTeamLeaderCleared = new List<User>();

            userForTeamLeaderCleared.Add(await _context.Users.FirstAsync(u => u.Id == userId));

            foreach (var userForTeamLeader in usersForTeamLeader)
            {
                if (userForTeamLeaderCleared.FirstOrDefault(u => u.Id == userForTeamLeader.Id) == null)
                    userForTeamLeaderCleared.Add(userForTeamLeader);
            }
            return userForTeamLeaderCleared;
        }

        public async Task<List<Group>> GetGroupsForTeamLeader(int teamLeaderId)
        {
            return await _context.Groups.Where(g => g.TeamLeaderId == teamLeaderId).ToListAsync();
        }

        public async Task<List<Case>> GetFilteredCases(FilterDto caseFilter)
        {
            IEnumerable<Case> cases = _context.Cases.Include(c => c.Client)
                .Include(c => c.Priority)
                .Include(c => c.Status)
                .Include(c => c.User)
                .Include(c => c.Group)
                .Include(c => c.Representative)
                .Include(c => c.CaseTagParent)
                .Include(c => c.CaseTagChild);

            if (caseFilter.TaskUserId != null)
            {
                cases = await GetCasesWhereUserIsInTask(caseFilter.TaskUserId.Value, cases.ToList());
            }

            if (caseFilter.LastSeenUser != null) {
                cases = GetLastSeenCases(caseFilter.LastSeenUser.Value);
            }

            if (caseFilter.TeamLeaderId != null)
            {
                var casesForTeamLeader = new List<Models.Case>();
                var groups = await GetGroupsForTeamLeader(caseFilter.TeamLeaderId.Value);
                foreach (var groupForUser in await GetGroupsForUser(caseFilter.TeamLeaderId.Value))
                {
                    if (groups.FirstOrDefault(g => g.Id == groupForUser.Id) == null)
                        groups.Add(groupForUser);
                }
                var users = await GetUsersForTeamLeader(caseFilter.TeamLeaderId.Value);

                foreach (var group in groups)
                {
                    var casesInGroup = cases.Where(c => c.GroupId == group.Id);
                    foreach (var caseIngroup in casesInGroup)
                    {
                        if (casesForTeamLeader.FirstOrDefault(c => c.Id == caseIngroup.Id) == null)
                            casesForTeamLeader.Add(caseIngroup);
                    }
                }
                foreach (var user in users)
                {
                    var casesForUser = cases.Where(c => c.UserId == user.Id);
                    foreach (var caseForUser in casesForUser)
                    {
                        if (casesForTeamLeader.FirstOrDefault(c => c.Id == caseForUser.Id) == null)
                            casesForTeamLeader.Add(caseForUser);
                    }
                }

                cases = casesForTeamLeader;
            }

            if (caseFilter.UserId != null)
            {
                List<Case> tempCases = cases.Where(c => c.UserId == caseFilter.UserId).ToList();
                foreach (var group in await GetGroupsForUser(caseFilter.UserId.Value))
                {
                    tempCases.AddRange(cases.Where(c => c.GroupId == group.Id).ToList());
                };
                cases = tempCases;
            }

            if (caseFilter.CaseId != null)
                cases = cases.Where(c => c.Id == caseFilter.CaseId);

            if (caseFilter.CaseTagParentId != null)
                cases = cases.Where(c => c.CaseTagParentId == caseFilter.CaseTagParentId);

            if (caseFilter.CaseTagChildId != null)
                cases = cases.Where(c => c.CaseTagChildId == caseFilter.CaseTagChildId);

            if (caseFilter.StatusId != null)
                cases = cases.Where(c => c.StatusId == caseFilter.StatusId);

            if (caseFilter.PriorityId != null)
                cases = cases.Where(c => c.PriorityId == caseFilter.PriorityId);

            if (caseFilter.StartDate != null)
                cases = cases.Where(c => c.StartDate >= caseFilter.StartDate);

            if (caseFilter.EndDate != null)
                cases = cases.Where(c => c.EndDate <= caseFilter.EndDate);

            if (caseFilter.TimeTaken != null)
                cases = cases.Where(c => c.TimeTaken >= caseFilter.TimeTaken);

            if (caseFilter.CaseId != null)
                cases = cases.Where(c => c.Id == caseFilter.CaseId);

            if (caseFilter.TimeEstimated != null)
                cases = cases.Where(c => c.TimeEstimated <= caseFilter.TimeEstimated);

            if (caseFilter.GroupId != null)
                cases = cases.Where(c => c.GroupId == caseFilter.GroupId);

            return cases.ToList();
        }

        public async Task<List<Case>> GetCasesWhereUserIsInTask(int UserId, List<Case> cases)
        {
            var tasks = GetTasksForUser(await _context.Tasks.Include(t => t.TasksUsers).ToListAsync(), UserId);
            var casesCopy = cases;
            var casesForTasks = new List<Models.Case>();
            foreach (var task in tasks)
            {
                if (casesForTasks.FirstOrDefault(c => c.Id == task.CaseId) == null)
                {
                    casesForTasks.Add(casesCopy.First(c => c.Id == task.CaseId));
                    casesCopy.Remove(casesCopy.First(c => c.Id == task.CaseId));
                }
            }
            cases = casesForTasks;
            return cases;
        }

        public async Task<List<TaskDto>> GetFilteredTasks(FilterDto taskFilter)
        {
            IEnumerable<PLM.Models.Task> tasks = _context.Tasks
            .Include(t => t.Case)
            .Include(t => t.Group)
            .Include(t => t.TasksUsers)
            .Include(t => t.Case.Client)
            .Include(t => t.Case.Priority)
            .Include(t => t.Case.Status)
            .Include(t => t.Case.User)
            .Include(t => t.Case.Group)
            .Include(t => t.Case.CaseTagParent)
            .Include(t => t.Case.CaseTagChild)
            .Include(t => t.Case.Representative)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority)
            .Include(t => t.Status);
        
            if (taskFilter.UserId != null)
            {
                tasks = GetTasksForUser(tasks, taskFilter.UserId.Value);
            }

            if (taskFilter.LastSeenUser != null) {
                tasks = GetLastSeenTasks(taskFilter.LastSeenUser.Value);
            }

            if (taskFilter.GroupId != null) {
                var users = await GetUsersInGroup(taskFilter.GroupId.Value);
                List<Models.Task> tempTasks = new List<Models.Task>();
                foreach (var user in users)
                {
                    var tsks = GetTasksForUser(tasks, user.Id);
                    foreach (var tsk in tsks) {
                        if (tempTasks.FirstOrDefault(t => t.Id == tsk.Id) == null)
                            tempTasks.Add(tsk);
                    }
                }
                tasks = tempTasks;
            }

            if (taskFilter.TeamLeaderId != null)
            {
                var users = await GetUsersForTeamLeader(taskFilter.TeamLeaderId.Value);
                List<Models.Task> tempTasks = new List<Models.Task>();
                foreach (var user in users)
                {
                    var tsks = tasks.Where(t => t.TasksUsers.FirstOrDefault(tu => tu.UserId == user.Id) != null).ToList();
                    foreach (var tsk in tsks) {
                        if (tempTasks.FirstOrDefault(t => t.Id == tsk.Id) == null)
                            tempTasks.Add(tsk);
                    }
                }
                tasks = tempTasks;
            }

            if (taskFilter.TaskId != null)
                tasks = tasks.Where(t => t.Id == taskFilter.TaskId);

            if (taskFilter.TaskTagMainId != null)
                tasks = tasks.Where(t => taskFilter.TaskTagMainId == t.TaskTagMainId);

            if (taskFilter.TaskTagAccessoryId != null)
                tasks = tasks.Where(t => t.TaskTagAccessoryId == taskFilter.TaskTagAccessoryId);

            if (taskFilter.TaskTagAccessoryMinorId != null)
                tasks = tasks.Where(t => t.TaskTagAccessoryMinorId == taskFilter.TaskTagAccessoryMinorId);

            if (taskFilter.StatusId != null)
                tasks = tasks.Where(c => c.StatusId == taskFilter.StatusId);

            if (taskFilter.PriorityId != null)
                tasks = tasks.Where(c => c.PriorityId == taskFilter.PriorityId);

            if (taskFilter.StartDate != null)
                tasks = tasks.Where(c => c.StartDate >= taskFilter.StartDate);

            if (taskFilter.EndDate != null)
                tasks = tasks.Where(c => c.EndDate <= taskFilter.EndDate);

            if (taskFilter.TimeTaken != null)
                tasks = tasks.Where(c => c.TimeTaken >= taskFilter.TimeTaken);

            if (taskFilter.TimeEstimated != null)
                tasks = tasks.Where(c => c.TimeEstimated <= taskFilter.TimeEstimated);

            if (taskFilter.CaseId != null)
                tasks = tasks.Where(t => t.CaseId == taskFilter.CaseId);

            return GetUsersForTasks(tasks);
        }

        public IEnumerable<Models.Task> GetTasksForUser(IEnumerable<Models.Task> tasks, int userId) {
            return tasks.Where(t => t.TasksUsers.FirstOrDefault(tu => tu.UserId == userId) != null).ToList();
        }

        public async Task<IEnumerable<Ecp>> GetEcps(FilterDto filter)
        {
            IEnumerable<Ecp> ecps = _context.Ecps
            .Include(e => e.Case)
            .Include(e => e.Task)
            .Include(e => e.Task.Case)
            .Include(e => e.User);

            if (filter.TeamLeaderId != null)
            {
                var users = await GetUsersForTeamLeader(filter.TeamLeaderId.Value);
                var tempEcps = new List<Ecp>();
                foreach (var user in users)
                {
                    tempEcps.AddRange(ecps.Where(e => tempEcps.FirstOrDefault(ec => ec.Id == e.Id) == null && e.UserId == user.Id).ToList());
                }
                ecps = tempEcps;
            }
            if (filter.UserId != null)
                ecps = ecps.Where(e => e.UserId == filter.UserId);
            if (filter.CaseId != null)
                ecps = ecps.Where(e => e.CaseId == filter.CaseId);
            if (filter.TaskId != null)
                ecps = ecps.Where(e => e.TaskId == filter.TaskId);
            if (filter.EcpDate != null)
                ecps = ecps.Where(e => e.Date == filter.EcpDate);
            if (filter.StartTime != null)
                ecps = ecps.Where(e => e.StartTime >= filter.StartTime);
            if (filter.FinishTime != null)
                ecps = ecps.Where(e => e.FinishTime <= filter.FinishTime);

            return ecps.ToList();
        }

        public async Task<List<User>> getUsersInGroup(int groupId)
        {
            var userInGroup = from user in _context.Users
                              join groupUser in _context.GroupsUsers on user.Id equals groupUser.User.Id
                              where groupUser.Group.Id == groupId
                              select _mapper.Map<User>(user);
            return await userInGroup.ToListAsync();
        }

        public List<TaskDto> GetUsersForTasks(IEnumerable<Models.Task> tasks)
        {
            var tasksDtos = new List<TaskDto>();
            foreach (var task in tasks)
            {
                var userInTask = from user in _context.Users
                                 join groupUser in _context.TasksUsers on user.Id equals groupUser.User.Id
                                 where groupUser.Task.Id == task.Id
                                 select _mapper.Map<UserDto>(user);
                var TaskDto = _mapper.Map<TaskDto>(task);
                TaskDto.Users = userInTask.ToList();
                tasksDtos.Add(TaskDto);
            }
            return tasksDtos;
        }

        public TaskDto GetUsersForTask(Models.Task task)
        {
                var userInTask = from user in _context.Users
                                 join groupUser in _context.TasksUsers on user.Id equals groupUser.User.Id
                                 where groupUser.Task.Id == task.Id
                                 select _mapper.Map<UserDto>(user);
                var TaskDto = _mapper.Map<TaskDto>(task);
                TaskDto.Users = userInTask.ToList();
            return TaskDto;
        }
        
        public List<Models.Task> GetLastSeenTasks(int UserId) {
            var lastSeenList = _context.LastSeen
            .Include(t => t.Task.Case)
            .Include(t => t.Task.Group)
            .Include(t => t.Task.TasksUsers)
            .Include(t => t.Task.Case.Client)
            .Include(t => t.Task.Case.Priority)
            .Include(t => t.Task.Case.Status)
            .Include(t => t.Task.Case.User)
            .Include(t => t.Task.Case.Group)
            .Include(t => t.Task.Case.CaseTagParent)
            .Include(t => t.Task.Case.CaseTagChild)
            .Include(t => t.Task.Case.Representative)
            .Include(t => t.Task.TaskTagMain)
            .Include(t => t.Task.TaskTagAccessory)
            .Include(t => t.Task.TaskTagAccessoryMinor)
            .Include(t => t.Task.Priority)
            .Include(t => t.Task.Status)
            .Where(l => l.UserId == UserId && l.TaskId != null);
            var tasks = new List<Models.Task>();
            foreach (var lastSeen in lastSeenList) {
                tasks.Add(lastSeen.Task);
            }
            return tasks;
        }

        public List<Models.Case> GetLastSeenCases(int UserId) {
            var lastSeenList = _context.LastSeen
                .Include(c => c.Case.Client)
                .Include(c => c.Case.Priority)
                .Include(c => c.Case.Status)
                .Include(c => c.Case.User)
                .Include(c => c.Case.Group)
                .Include(c => c.Case.Representative)
                .Include(c => c.Case.CaseTagParent)
                .Include(c => c.Case.CaseTagChild)
                .Where(l => l.UserId == UserId && l.CaseId != null);
            var cases = new List<Models.Case>();
            foreach (var lastSeen in lastSeenList) {
                cases.Add(lastSeen.Case);
            }
            return cases;
        }
    }
}