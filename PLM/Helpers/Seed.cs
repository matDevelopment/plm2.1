using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using PLM.Models;
using Plm = PLM.Models;

namespace PLM.Helpers
{
    public class Seed
    {
        private readonly PLMContext _context;
        private readonly Microsoft.AspNetCore.Identity.UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private List<Case> Cases = new List<Case>();
        private List<Plm.Task> Tasks = new List<Plm.Task>();
        private List<Priority> Priorities = new List<Priority>();
        private List<Status> Statuses = new List<Status>();
        private List<Client> Clients = new List<Client>();
        private List<User> Users = new List<User>();
        private List<Group> Groups = new List<Group>();
        private List<GroupUser> GroupsUsers = new List<GroupUser>();
        private List<CaseTagParent> CaseTagParents = new List<CaseTagParent>();
        private List<CaseTagChild> CaseTagChildren = new List<CaseTagChild>();
        private List<TaskTagMain> TaskTagsMain = new List<TaskTagMain>();
        private List<TaskTagAccessory> TaskTagsAccessory = new List<TaskTagAccessory>();
        private List<TaskTagAccessoryMinor> TaskTagsAccessoryMinor = new List<TaskTagAccessoryMinor>();
        private List<TimePeriod> TimePeriods = new List<TimePeriod>();
        private List<Representative> Representatives = new List<Representative>();
        private List<TaskUser> TasksUsers = new List<TaskUser>();
        public Seed(PLMContext context, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        private void SeedCases()
        {
            var case1 = new Case
            {
                Name = "ew_czas_obiekt_11",
                Order = "09145-00",
                Description = "Przykładowy projekt ewidencji czasu pracy.",
                ClientId = 1,
                StatusId = 2,
                PriorityId = 1,
                CaseTagParentId = 1,
                CaseTagChildId = null,
                UserId = 1,
                GroupId = null,
                StartDate = new DateTime(2018,12,3,7,0,0),
                EndDate = new DateTime(2019,3,2,17,0,0),
                TimeEstimated = 790,
                TimeTaken = 0,
            };
            Cases.Add(case1);
        }

        public void SeedTaskUsers() {
            TasksUsers.Add(new TaskUser {
                UserId = 6,
                TaskId = 1,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 7,
                TaskId = 2,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 8,
                TaskId = 3,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 9,
                TaskId = 4,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 10,
                TaskId = 5,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 11,
                TaskId = 6,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 12,
                TaskId = 6,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 13,
                TaskId = 6,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 14,
                TaskId = 6,
            });
               TasksUsers.Add(new TaskUser {
                UserId = 15,
                TaskId = 7,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 16,
                TaskId = 8,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 17,
                TaskId = 9,
            });
            TasksUsers.Add(new TaskUser {
                UserId = 18,
                TaskId = 10,
            });
        }

        private void SeedTasks()
        {
            Tasks.Add(new Plm.Task
            {
                Name = "Zad. konstr. poziom 0",
                Order = "09145-00-00",
                Description = "Obliczanie wytrzymałości ścian nośnych.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 1,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2018,12,3,8,0,0),
                EndDate = new DateTime(2018,12,19,16,0,0),
                TimeEstimated = 100,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });

            Tasks.Add(new Plm.Task
            {
                Name = "Zad. konstr. poziom 1",
                Order = "09145-00-01",
                Description = "Wykonanie obliczeń nośności stropów.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 1,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2018,12,19,16,0,0),
                EndDate = new DateTime(2019,1,15,16,0,0),
                TimeEstimated = 150,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });

            Tasks.Add(new Plm.Task
            {
                Name = "Zad. konstr. poziom 2",
                Order = "09145-00-02",
                Description = "Opracowanie konstrukcji przybudówki garażowej.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 1,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2019,1,15,16,0,0),
                EndDate = new DateTime(2019,2,8,16,0,0),
                TimeEstimated = 150,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });

            Tasks.Add(new Plm.Task
            {
                Name = "Zad. Arch. poziom 0",
                Order = "09145-00-03",
                Description = "Pomiary, projekty koncepcyjne oraz finalny umiejscowienia okien.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 2,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2018,12,19,8,0,0),
                EndDate = new DateTime(2019,3,2,16,0,0),
                TimeEstimated = 80,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });

            Tasks.Add(new Plm.Task
            {
                Name = "Zad. Arch. poziom 1",
                Order = "09145-00-04",
                Description = "Projekt kręconych schodów oraz windy.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 2,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2019,1,15,8,0,0),
                EndDate = new DateTime(2019,1,31,16,0,0),
                TimeEstimated = 100,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });

            Tasks.Add(new Plm.Task
            {
                Name = "Zad. Arch. poziom 2",
                Order = "09145-00-05",
                Description = "Projekt fasady budynku.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 2,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2019,2,11,8,0,0),
                EndDate = new DateTime(2019,2,19,16,0,0),
                TimeEstimated = 50,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });
            
            Tasks.Add(new Plm.Task
            {
                Name = "Zad. Inst. poziom 0",
                Order = "09145-00-06",
                Description = "Instalacja systemu automatyzacji okien.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 3,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2019,1,2,8,0,0),
                EndDate = new DateTime(2019,1,10,16,0,0),
                TimeEstimated = 50,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });
            
            Tasks.Add(new Plm.Task
            {
                Name = "Zad. Inst. poziom 1",
                Order = "09145-00-07",
                Description = "Instalacja windy.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 3,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2019,1,31,8,0,0),
                EndDate = new DateTime(2019,2,12,16,0,0),
                TimeEstimated = 60,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });
            
            Tasks.Add(new Plm.Task
            {
                Name = "Zad. Inst. poziom 2",
                Order = "09145-00-08",
                Description = "Instalacja urządzeń sanitarnych.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 3,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2019,2,19,8,0,0),
                EndDate = new DateTime(2019,2,27,8,0,0),
                TimeEstimated = 50,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });

            Tasks.Add(new Plm.Task
            {
                Name = "Obiór",
                Order = "09145-00-10",
                Description = "Odbiór projektu.",
                StatusId = 2,
                PriorityId = 1,
                CaseId = 1,
                TaskTagMainId = 4,
                GroupId = null,
                TaskTagAccessoryId = null,
                TaskTagAccessoryMinorId = null,
                StartDate = new DateTime(2019,2,27,8,0,0),
                EndDate = new DateTime(2019,2,27,8,0,0),
                TimeEstimated = 0,
                TimeTaken = 0,
                DestinatedEmployees = 1,
            });
        }

        private void SeedTags()
        {
            CaseTagParents.Add(new CaseTagParent {
                Name = "Wielobranżowa",
            });
            CaseTagChildren.Add(new CaseTagChild {
                Name = "Budowlany",
                CaseTagParentId = 1,
            });
            CaseTagChildren.Add(new CaseTagChild {
                Name = "Koncepcyjny",
                CaseTagParentId = 1,
            });
            CaseTagChildren.Add(new CaseTagChild {
                Name = "Warsztatowy",
                CaseTagParentId = 1,
            });
            TaskTagsMain.Add(new TaskTagMain {
                Name = "Konstrukcje",
                CaseTagParentId = 1,
            });
            TaskTagsMain.Add(new TaskTagMain {
                Name = "Architektura",
                CaseTagParentId = 1,
            });
            TaskTagsMain.Add(new TaskTagMain {
                Name = "Instalacje",
                CaseTagParentId = 1,
            });
            TaskTagsMain.Add(new TaskTagMain {
                Name = "Zakonczenie Prac",
                CaseTagParentId = 1,
            });
            TaskTagsAccessory.Add(new TaskTagAccessory {
                Name = "Poziom 1",
                TaskTagMainId = 1,
            });
            TaskTagsAccessory.Add(new TaskTagAccessory {
                Name = "Poziom 2",
                TaskTagMainId = 1,
            });
            TaskTagsAccessory.Add(new TaskTagAccessory {
                Name = "Poziom 3",
                TaskTagMainId = 1,
            });
            TaskTagsAccessoryMinor.Add(new TaskTagAccessoryMinor {
                Name = "Poziom 1",
                TaskTagAccessoryId = 1,
            });
            TaskTagsAccessoryMinor.Add(new TaskTagAccessoryMinor {
                Name = "Poziom 2",
                TaskTagAccessoryId = 1,
            });
            TaskTagsAccessoryMinor.Add(new TaskTagAccessoryMinor {
                Name = "Poziom 3",
                TaskTagAccessoryId = 1,
            });
        }

        private void SeedPriorities()
        {
            var priority1 = new Priority
            {
                Name = "Normalny",
                Color = "white",
                BackgroundColor = "rgb(4, 134, 4)",
                PosX = 0,
                PosY = 50,
            };
            Priorities.Add(priority1);

            var priority2 = new Priority
            {
                Name = "Wysoki",
                Color = "#222222",
                BackgroundColor = "#FF6859",
                PosX = 0,
                PosY = 100,
            };
            Priorities.Add(priority2);
        }
        private void SeedUsersAndGroups()
        {

            var roles = new List<Role> {
                new Role{Name = RolesHelper.User},
                new Role{Name = RolesHelper.TeamLeader},
                new Role{Name = RolesHelper.ProjectManager},
                new Role{Name = RolesHelper.Admin}
            };

            foreach (var role in roles)
            {
                _roleManager.CreateAsync(role).Wait();
            }

            var user1 = new User
            {
                UserName = "sz.urbanik",
                Name = "sz.urbanik",
                FirstName = "Szymon",
                Surname = "Urbanik",
                Email = "sz.urbanik@mat.net.pl"
            };
            _userManager.CreateAsync(user1, "qwert1").Wait();
            _userManager.AddToRoleAsync(user1, RolesHelper.Admin).Wait();
            var user2 = new User
            {
                UserName = "m.buczek",
                Name = "m.buczek",
                FirstName = "Michał",
                Surname = "Buczek",
                Email = "m.buczek@mat.net.pl"
            };
            _userManager.CreateAsync(user2, "qwert1").Wait();
            _userManager.AddToRoleAsync(user2, RolesHelper.Admin).Wait();
            var user3 = new User
            {
                UserName = "m.labuda",
                Name = "m.labuda",
                FirstName = "Michał",
                Surname = "Łabuda",
                Email = "m.labuda@mat.net.pl"
            };
            _userManager.CreateAsync(user3, "qwert1").Wait();
            _userManager.AddToRoleAsync(user3, RolesHelper.Admin).Wait();
            var user4 = new User
            {
                UserName = "tomek",
                Name = "tomek",
                FirstName = "Tomasz",
                Surname = "Musialik",
                Email = "t.musialik@mat.net.pl"
            };
            _userManager.CreateAsync(user4, "qwert1").Wait();
            _userManager.AddToRoleAsync(user4, RolesHelper.Admin).Wait();
            var user5 = new User
            {
                UserName = "piotr",
                Name = "piotr",
                FirstName = "Piotr",
                Surname = "Łabuda",
                Email = "p.labuda@mat.net.pl"
            };
            _userManager.CreateAsync(user5, "qwert1").Wait();
            _userManager.AddToRoleAsync(user5, RolesHelper.Admin).Wait();

            var user6 = new User
            {
                UserName = "konst_1",
                Name = "konst_1",
                FirstName = "Adrian",
                Surname = "Nowak",
                Email = "konstruktor1@mat.net.pl"
            };
            _userManager.CreateAsync(user6, "qwert1").Wait();
            _userManager.AddToRoleAsync(user6, RolesHelper.Admin).Wait();

            var user7 = new User
            {
                UserName = "konst_2",
                Name = "konst_2",
                FirstName = "Barbara",
                Surname = "Kowalska",
                Email = "konstruktor2@mat.net.pl"
            };
            _userManager.CreateAsync(user7, "qwert1").Wait();
            _userManager.AddToRoleAsync(user7, RolesHelper.User).Wait();

            var user8 = new User
            {
                UserName = "konst_3",
                Name = "konst_3",
                FirstName = "Czesław",
                Surname = "Miodek",
                Email = "konstruktor3@mat.net.pl"
            };
            _userManager.CreateAsync(user8, "qwert1").Wait();
            _userManager.AddToRoleAsync(user8, RolesHelper.User).Wait();
            
            var user9 = new User
            {
                UserName = "konst_4",
                Name = "konst_4",
                FirstName = "Dawid",
                Surname = "Habera",
                Email = "konstruktor4@mat.net.pl"
            };
            _userManager.CreateAsync(user9, "qwert1").Wait();
            _userManager.AddToRoleAsync(user9, RolesHelper.User).Wait();

            var user10 = new User
            {
                UserName = "konst_5",
                Name = "konst_5",
                FirstName = "Edmund",
                Surname = "Kowal",
                Email = "konstruktor5@mat.net.pl"
            };
            _userManager.CreateAsync(user10, "qwert1").Wait();
            _userManager.AddToRoleAsync(user10, RolesHelper.User).Wait();
            
            var user11 = new User
            {
                UserName = "konst_6",
                Name = "konst_6",
                FirstName = "Feliks",
                Surname = "Orzeszek",
                Email = "konstruktor6@mat.net.pl"
            };
            _userManager.CreateAsync(user11, "qwert1").Wait();
            _userManager.AddToRoleAsync(user11, RolesHelper.User).Wait();
            
            var user12 = new User
            {
                UserName = "arch_1",
                Name = "arch_1",
                FirstName = "Gustaw",
                Surname = "Lampkowski",
                Email = "architekt1@mat.net.pl"
            };
            _userManager.CreateAsync(user12, "qwert1").Wait();
            _userManager.AddToRoleAsync(user12, RolesHelper.User).Wait();

            var user13 = new User
            {
                UserName = "arch_2",
                Name = "arch_2",
                FirstName = "Hubert",
                Surname = "Stopiński",
                Email = "architekt2@mat.net.pl"
            };
            _userManager.CreateAsync(user13, "qwert1").Wait();
            _userManager.AddToRoleAsync(user13, RolesHelper.User).Wait();
            
            var user14 = new User
            {
                UserName = "arch_3",
                Name = "arch_3",
                FirstName = "Ilona",
                Surname = "Różalska",
                Email = "architekt3@mat.net.pl"
            };
            _userManager.CreateAsync(user14, "qwert1").Wait();
            _userManager.AddToRoleAsync(user14, RolesHelper.User).Wait();
            
            var user15 = new User
            {
                UserName = "arch_4",
                Name = "arch_4",
                FirstName = "Jan",
                Surname = "Nowak",
                Email = "architekt4@mat.net.pl"
            };
            _userManager.CreateAsync(user15, "qwert1").Wait();
            _userManager.AddToRoleAsync(user15, RolesHelper.User).Wait();
            
            var user16 = new User
            {
                UserName = "arch_5",
                Name = "arch_5",
                FirstName = "arch",
                Surname = "arch",
                Email = "architekt5@mat.net.pl"
            };
            _userManager.CreateAsync(user16, "qwert1").Wait();
            _userManager.AddToRoleAsync(user16, RolesHelper.User).Wait();
            
            var user17 = new User
            {
                UserName = "inst_1",
                Name = "inst_1",
                FirstName = "Konrad",
                Surname = "Jaroś",
                Email = "instalator1@mat.net.pl"
            };
            _userManager.CreateAsync(user17, "qwert1").Wait();
            _userManager.AddToRoleAsync(user17, RolesHelper.User).Wait();
                        
            var user18 = new User
            {
                UserName = "inst_2",
                Name = "inst_2",
                FirstName = "Laurencjusz",
                Surname = "Stoicki",
                Email = "instalator2@mat.net.pl"
            };
            _userManager.CreateAsync(user18, "qwert1").Wait();
            _userManager.AddToRoleAsync(user18, RolesHelper.User).Wait();
                                    
            var user19 = new User
            {
                UserName = "inst_3",
                Name = "inst_3",
                FirstName = "Łucja",
                Surname = "Pasieka",
                Email = "instalator3@mat.net.pl"
            };
            _userManager.CreateAsync(user19, "qwert1").Wait();
            _userManager.AddToRoleAsync(user19, RolesHelper.User).Wait();
                                    
            var user20 = new User
            {
                UserName = "inst_4",
                Name = "inst_4",
                FirstName = "Michał",
                Surname = "Bułka",
                Email = "instalator4@mat.net.pl"
            };
            _userManager.CreateAsync(user20, "qwert1").Wait();
            _userManager.AddToRoleAsync(user20, RolesHelper.User).Wait();

            var grupaKonstruktorow = new Group{
                Name = "Konstruktorzy",
                TeamLeaderId = null,
                UsersAmount = 6,
            };

            Groups.Add(grupaKonstruktorow);

            var grupaArchitektow = new Group{
                Name = "Architekci",
                TeamLeaderId = null,
                UsersAmount = 5,
            };

            Groups.Add(grupaArchitektow);

            var grupaInstalatorow = new Group{
                Name = "Instalatorzy",
                TeamLeaderId = null,
                UsersAmount = 4
            };

            Groups.Add(grupaInstalatorow);

            GroupsUsers.Add(new GroupUser {
                Group = grupaKonstruktorow,
                User = user6,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaKonstruktorow,
                User = user7,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaKonstruktorow,
                User = user8,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaKonstruktorow,
                User = user9,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaKonstruktorow,
                User = user10,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaKonstruktorow,
                User = user11,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaArchitektow,
                User = user12,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaArchitektow,
                User = user13,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaArchitektow,
                User = user14,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaArchitektow,
                User = user15,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaArchitektow,
                User = user16,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaInstalatorow,
                User = user17,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaInstalatorow,
                User = user18,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaInstalatorow,
                User = user19,
            });
            GroupsUsers.Add(new GroupUser {
                Group = grupaInstalatorow,
                User = user20,
            });
        }

        private void SeedClients()
        {
            Clients.Add(
                new Client
                {
                    Name = "Autodesk Inc",
                    Email = "autodesk@autodesk.com",
                    PhoneNumber = "603670864",
                    Nip = 653452432
                }
            );
            Clients.Add(
                new Client
                {
                    Name = "PolBud",
                    Email = "PolBud@polbud.net.pl",
                    PhoneNumber = "603670865",
                    Nip = 813452456
                }
            );
            Clients.Add(
               new Client
               {
                   Name = "Architects Inc",
                   Email = "architects@architects.com",
                   PhoneNumber = "603670867",
                   Nip = 903452492
               }
           );
            Representatives.Add(
                new Representative {
                    Name = "Jan",
                    Surname = "Mateja",
                    Email = "jan.mateja@autodesk.com",
                    Phone = "443434345",
                    ClientId = 1,
                }
            );
            Representatives.Add(
                new Representative {
                    Name = "Janina",
                    Surname = "Buczek",
                    Email = "janina.buczek@autodesk.com",
                    Phone = "43556656",
                    ClientId = 1,
                }
            );
            Representatives.Add(
                new Representative {
                    Name = "Michał",
                    Surname = "Nowak",
                    Email = "miachal.nowak@architekts.com",
                    Phone = "35435456",
                    ClientId = 3
                }
            );
        }

        private void SeedStatuses()
        {
            var status1 = new Status
            {
                Name = "Nowe",
                Color = "white",
                BackgroundColor = "rgb(4, 134, 4)",
                PosX = 0,
                PosY = 50,
            };
            Statuses.Add(status1);
            var status2 = new Status
            {
                Name = "W Toku",
                Color = "#222222",
                BackgroundColor = "#FFCF44",
                PosX = 0,
                PosY = 100,
            };
            Statuses.Add(status2);
            var status3 = new Status
            {
                Name = "Zakończone",
                Color = "#222222",
                BackgroundColor = "#d4d4d4",
                PosX = 0,
                PosY = 150,
            };
            Statuses.Add(status3);
        }

        public void SeedFolderCount() {
            _context.FolderCount.Add(new FolderCount { Amount = 1 });
        }

        public void SeedDatabase()
        {
            var status = _context.Statuses.FirstOrDefault(s => s.Id == 1);
            if (status == null)
            {
                SeedPriorities();
                SeedStatuses();
                SeedCases();
                SeedClients();
                SeedTasks();
                SeedUsersAndGroups();
                SeedTaskUsers();
                SeedTags();
                SeedFolderCount();
                _context.Statuses.AddRange(Statuses);
                _context.Priorites.AddRange(Priorities);
                _context.Clients.AddRange(Clients);
                _context.CaseTagParents.AddRange(CaseTagParents);
                _context.SaveChanges();
                _context.Representatives.AddRange(Representatives);
                _context.CaseTagChildren.AddRange(CaseTagChildren);
                _context.TaskTagsMain.AddRange(TaskTagsMain);
                _context.Groups.AddRange(Groups);
                _context.SaveChanges();
                 _context.TaskTagsAccessory.AddRange(TaskTagsAccessory);
                _context.Cases.AddRange(Cases);
                _context.SaveChanges();
                _context.TaskTagsAccessoryMinor.AddRange(TaskTagsAccessoryMinor);
                _context.SaveChanges();
                _context.GroupsUsers.AddRange(GroupsUsers);
                _context.Tasks.AddRange(Tasks);
                _context.SaveChanges();
                _context.TasksUsers.AddRange(TasksUsers);
                _context.SaveChanges();
            }
        }
    }
}