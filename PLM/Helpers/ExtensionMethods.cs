using System;
using System.Security.Cryptography;
using System.Text;

namespace PLM.Helpers
{
    public static class ExtensionMethods
    {
        public static int WordCount(this ref Int32 number) {
            return number = number - 1;
        }

        public static String GenerateRandomString(this string str)
        {
            char[] chars = new char[24];
            chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetBytes(data);
            data = new byte[24];
            crypto.GetBytes(data);
            StringBuilder result = new StringBuilder(24);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            str = result.ToString();
            return str;
        }   

    }
}