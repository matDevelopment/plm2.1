using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface IMemoRepository
    {
        Task<Memo> AddNewMemo(Memo newMemo);
        Task<IEnumerable<Memo>> GetMemosForParent(int id, string type);
        Task<Memo> UpdateMemo(Memo updatedMemo);
        Task<Memo> DeleteMemo(Memo deletedMemo);
        Task<Memo> GetMemo(int memoId);
        Task<IEnumerable<Memo>>  GetMemosForUser(int id);
        Task<IEnumerable<Memo>>  GetMemosForParentForUser(int parentId, string type, int userId);
    }
}