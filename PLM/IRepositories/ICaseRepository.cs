using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.Dtos;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface ICaseRepository
    {
        Task<IEnumerable<Case>> GetCases();
        Task<List<Case>> GetFilteredCases(FilterDto caseFilter);
        Task<Case> RemoveCase(Case @case);
        Task<Case> GetCase(int id);
        Task<Case> EditCase(Case @case);
        Task<Case> AddNewCase(Case @case);
        Task<Case> ChangeStatus(Case @case);
        Task<Case> SetBimFolderId(int caseId, string FolderId);
    } 
}