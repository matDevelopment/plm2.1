using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface IGroupRepository
    {
        Task<IEnumerable<Group>> GetGroups();
        Task<IEnumerable<Group>> GetGroupsForUser(int id);
        Task<IEnumerable<GroupDto>> GetGroupsWithUsers();
        Task<GroupDto> AddGroup(GroupDto groupWithUsers);
        Task<Group> RemoveGroup(int id);
        GroupDto SetGroupTeamLeader(GroupDto group);
        GroupDto RemoveTeamLeaderFromGroup(GroupDto group);
        Task<GroupDto> RemoveUserFromGroup(int userId, int groupId);
        Task<IEnumerable<Group>> GetGroupsForTeamLeader(int teamLeaderId);
        Task<IEnumerable<GroupDto>> GetFilteredGroups(FilterDto filter);
    }
}