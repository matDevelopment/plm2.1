using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using PLM.Dtos;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface ITablesChartsRepository
    {
        Task<List<User>> GetUsersForCase(int caseId, int? userId, int? groupId);
        Task<List<CaseUserEcpDto>> GetCasesWithUsers(FilterDto filter, FilterDto subFilter);
        Task<CaseUserEcpDto> GetCaseWithUsers(FilterDto filter, FilterDto subFilter, Case _case);
        Task<List<Models.Task>> GetTasksForUser(FilterDto subFilter);
        Task<UserTaskEcpDto> GetUserWithTasks(FilterDto filter, FilterDto subFilter, User _user);
        Task<List<UserTaskEcpDto>> GetUsersWithTasks(FilterDto filter, FilterDto subFilter);
        Task<List<CaseTasksDto>> GetCasesWithTasks(FilterDto filter, FilterDto subFilter);
        Task<List<GroupWithAllUsersDto>> GetUsersForGroups(FilterDto filter, FilterDto subfilter);
        Task<byte[]> ExportJson(string json, IHostingEnvironment env);
    }
}