using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.Dtos;
using PLM.Models;
using Plm = PLM.Models;

namespace PLM.Repositories
{
    public interface ITemplateRepository
    {
        Task<IEnumerable<Template>> GetTemplates();
        Task<Template> GetTemplate(int id);
        Task<Template> AddTemplate(Template template);
        Task<Template> EditTemplate(Template template);
        Task<Template> RemoveTemplate(Template template);
        Task<IEnumerable<TaskTemplate>> GetTaskTemplates();
        Task<TaskTemplate> GetTaskTemplate(int id);
        Task<TaskTemplate> AddTaskTemplate(TaskTemplate template);
        Task<TaskTemplate> EditTaskTemplate(TaskTemplate template);
        Task<TaskTemplate> RemoveTaskTemplate(TaskTemplate template);
        Task<Template> CopyCaseToTemplate(int caseId);
        Task<TaskTemplate> CopyTaskToTaskTemplate(int caseId);
    }
}