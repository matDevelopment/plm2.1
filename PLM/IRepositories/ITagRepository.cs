using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.DtosModels;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface ITagRepository
    {
        Task<IEnumerable<CaseTagParent>> GetCaseTagsParent();
        Task<IEnumerable<CaseTagChild>> GetCaseTagsChild(int id);
        Task<IEnumerable<TaskTagMain>> GetTaskTagsMain(int id);
        Task<IEnumerable<TaskTagAccessory>> GetTaskTagsAccessory(int id);
        Task<IEnumerable<TaskTagAccessoryMinor>> GetTaskTagsAccessoryMinor(int id);
        Task<CaseTagParent> AddCaseTagParent(TagDto tag);
        Task<CaseTagChild> AddCaseTagChild(TagDto tag);
        Task<TaskTagMain> AddTaskTagMain(TagDto tag);
        Task<TaskTagAccessory> AddTaskTagAccessory(TagDto tag);
        Task<TaskTagAccessoryMinor> AddTaskTagAccessoryMinor(TagDto tag);
        Task<CaseTagParent> RemoveCaseTagParent(TagDto tag);
        Task<CaseTagChild> RemoveCaseTagChild(TagDto tag);
        Task<TaskTagMain> RemoveTaskTagMain(TagDto tag);
        Task<TaskTagAccessory> RemoveTaskTagAccessory(TagDto tag);
        Task<TaskTagAccessoryMinor> RemoveTaskTagAccessoryMinor(TagDto tag);
        Task<CaseTagParent> EditCaseTagParent(TagDto tag);
        Task<CaseTagChild> EditCaseTagChild(TagDto tag);
        Task<TaskTagMain> EditTaskTagMain(TagDto tag);
        Task<TaskTagAccessory> EditTaskTagAccessory(TagDto tag);
        Task<TaskTagAccessoryMinor> EditTaskTagAccessoryMinor(TagDto tag);
        Task<TaskTagAccessory> CopyTaskTagAccessoryStructure(TaskTagAccessory taskTagAccessory, int taskTagMainId = 0);
        Task<TaskTagMain> CopyTaskTagMainStructure(TaskTagMain tag);
        Task<TaskTagAccessoryMinor> CopyTaskTagAccessoryMinorStructure(TaskTagAccessoryMinor taskTagAccessoryMinor, int taskTagAccessoryId = 0);
    }
}