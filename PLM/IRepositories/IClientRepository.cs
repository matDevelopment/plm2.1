using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface IClientRepository
    {
        Task<IEnumerable<Client>> GetClients();
        Task<Client> GetClient(int id);
        Task<Client> AddNewClient(Client client);
        Task<Client> RemoveClient(Client client);
        Task<Client> EditClient(Client client);
        Task<IEnumerable<Representative>> GetRepresentativesAsync(int id);
        Task<Representative> AddRepresentativeAsync(Representative representative);
        Task<Representative> RemoveRepresentativeAsync(Representative representative);
        Task<Representative> EditRepresentativesAsync(Representative representative);
    }
}