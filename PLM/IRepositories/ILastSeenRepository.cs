using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.DtosModels;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface ILastSeenRepository
    {
        Task<IEnumerable<LastSeenDto>> GetLastSeen(int userId);
        Task<LastSeen> AddLastSeen(LastSeen lastSeen);
    }
}