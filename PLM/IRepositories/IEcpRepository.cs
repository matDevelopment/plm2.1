using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.Dtos;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface IEcpRepository
    {
        Task<IEnumerable<TimePeriod>> GetTimePeriods();
        Task<IEnumerable<Ecp>> AddEcps(List<Ecp> ecps);
        Task<IEnumerable<Ecp>> GetEcps(FilterDto filter);
        Task<IEnumerable<Ecp>> GetAllEcps();
        Task<Ecp> RemoveEcp(Ecp ecp);
        DateTime GetTodaysDate();
        Task<IEnumerable<bool>> GetReportedDaysInMonth(int year, int month);
        Task<IEnumerable<WorkDay>> GetReportedDaysInMonthForUser(int year, int month, int userId);
    }
}