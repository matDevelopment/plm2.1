using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.DtosModels;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface INotificationRepository
    {
        Task<Notification> AddNotification(Notification notification);
        Task<IEnumerable<Notification>> GetNotificationsForUser(int userId);
        Task<IEnumerable<Notification>> GetUnseenNotificationsForUser(int userId);
        Task<IEnumerable<Notification>> GetSeenNotificationsForUser(int userId);
    }
}