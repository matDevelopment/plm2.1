using System.Collections.Generic;
using System.IO;
using PLM.Dtos;

namespace PLM.IRepositories
{
    public interface IFileRepository
    {
        void AddDirectory(string path);
        void AddFile(string path);
        void RemoveFile(string path);
        void RemoveDirectory(string path);
        List<DirectoryDto> GetFilesAndDirectories(string path);
        FileStream DownloadFile(string path);
    }
}