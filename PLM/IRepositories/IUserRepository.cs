using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.Dtos;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<Object>> GetUsers();
        Task<IEnumerable<User>> GetUsersForGroup(int id);
        Task<IEnumerable<User>> GetAllUserWithVisibility();
        void RemoveUserFromGroup(User user, Group group);
        Task<bool> ChangeRole(string userName, string[] RoleNames);
        Task<IEnumerable<Role>> GetRoles();
        Task<User> AddUser(User user);
        Task<User> EditUser(User user);
        Task<User> AddUserToGroup(User user, Group group);
        IEnumerable<User> GetUsersFromGroupForTeamLeader(int userId);
        Task<IEnumerable<User>> GetUsersForEcpDay(DateTime date, string userName);
        IEnumerable<User> GetTeamLeaderUsersForEcpDay(int teamLeaderId, DateTime date);
        Task<IEnumerable<User>> GetUsersForUser(string userName);
        Task<List<User>> GetFilteredUsers(FilterDto filterDto);
        Task<List<User>> GetTeamLeaders();
        Task<User> GetUser(int id);
    }
}