using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.Dtos;
using PLM.DtosModels;
using Plm = PLM.Models;

namespace PLM.Repositories
{
    public interface ITaskRepository
    {
        Task<IEnumerable<TaskDto>> GetTasks();
        Task<List<TaskDto>> GetFilteredTasks(FilterDto taskFilter);
        Task<IEnumerable<Models.Task>> GetTaskForEcp(DateTime date);
        Task<Plm.Task> GetTaskById(int id);
        Task<TaskDto> AddTask(TaskDto task);
        Task<TaskDto> EditTask(TaskDto task);
        Task<TaskDto> RemoveTask(TaskDto task);
        Task<Models.Task> ChangeStatus(Models.Task task);
        Task<Models.Task> SetBimFolderId(int taskId, string FolderId);
    }
}