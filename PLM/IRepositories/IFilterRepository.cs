using System.Collections.Generic;
using System.Threading.Tasks;
using PLM.Dtos;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface IFilterRepository
    {
         Task<IEnumerable<Status>> GetStatuses();
         Task<IEnumerable<Priority>> GetPriorities();
         Task<FilterNamesDto> GetFilters(FilterDto filter);
    }
}