using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using PLM.BaseModels;
using PLM.DtosModels;
using PLM.Models;

namespace PLM.IRepositories
{
    public interface IEmailRepository
    {
        void SendEmail(MailMessage message);
        void CreateEmail(string subject, string messageBody, User user);
        void CreateEmail(string subject, string messageBody, IEnumerable<UserDto> user);
    }
}