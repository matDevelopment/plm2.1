using PLM.BaseModels;
using PLM.Models;

namespace PLM.DtosModels
{
    public class NotificationDto : NotificationBase
    {
        public CaseDto Case { get; set; }
        public TaskDto Task { get; set; }
        public MemoDto Memo { get; set; }
        public User User { get; set; }
    }
}