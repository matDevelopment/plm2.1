using System.Collections.Generic;
using PLM.BaseModels;
using PLM.Models;

namespace PLM.DtosModels
{
    public class CaseDto : CaseBase
    {
        public ClientDto Client { get; set; }
        public PriorityDto Priority { get; set; }
        public StatusDto Status { get; set; }
        public GroupDto Group { get; set; }
        public UserDto User { get; set; }
        public CaseTagChildDto CaseTagChild { get; set; }
        public CaseTagParentDto CaseTagParent { get; set; }
        public RepresentativeDto Representative { get; set; }
        public IEnumerable<LastSeenDto> LastSeen { get; set; }
    }
}