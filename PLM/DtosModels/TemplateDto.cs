using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.DtosModels
{
    public class TemplateDto : TemplateBase
    {
        public PriorityDto Priority { get; set; }
        public ClientDto Client { get; set; }
        public RepresentativeDto Representative { get; set; }
        public CaseTagParentDto CaseTagParent { get; set; }
        public CaseTagChildDto CaseTagChild { get; set; }
        public List<TaskTemplateDto> TaskTemplates { get; set; }
    }
}