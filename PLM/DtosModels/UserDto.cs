using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.DtosModels
{
    public class UserDto : UserBase
    {
        public string Email { get; set; }
    }
}