using PLM.BaseModels;

namespace PLM.DtosModels
{
    public class EcpDto : EcpBase
    {
        public CaseDto Case { get; set; }
        public TaskDto Task { get; set; }
        public UserDto User { get; set; }
    }
}