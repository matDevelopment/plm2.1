using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.DtosModels
{
    public class GroupDto : GroupBase
    {
        public IEnumerable<UserDto> Users { get; set; }
    }
}