using PLM.BaseModels;

namespace PLM.DtosModels
{
    public class TaskTemplateDto : TaskTemplateBase
    {
        public CaseDto Case { get; set; }
        public PriorityDto Priority { get; set; }
        public TaskTagMainDto TaskTagMain { get; set; }
        public TaskTagAccessoryDto TaskTagAccessory { get; set; }
        public TaskTagAccessoryMinorDto TaskTagAccessoryMinor { get; set; }
    }
}