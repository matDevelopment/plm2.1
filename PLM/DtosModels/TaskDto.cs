

using System.Collections.Generic;
using PLM.BaseModels;

namespace PLM.DtosModels
{
    public class TaskDto : TaskBase
    {
        public CaseDto Case { get; set; }
        public PriorityDto Priority { get; set; }
        public StatusDto Status { get; set; }
        public IEnumerable<UserDto> Users { get; set; }
        public GroupDto Group { get; set; }
        public TaskTagMainDto TaskTagMain { get; set; }
        public TaskTagAccessoryDto TaskTagAccessory { get; set; }
        public TaskTagAccessoryMinorDto TaskTagAccessoryMinor { get; set; }
        public IEnumerable<LastSeenDto> LastSeen { get; set; }
    }
}