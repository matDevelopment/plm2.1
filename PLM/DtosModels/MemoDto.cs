using PLM.BaseModels;

namespace PLM.DtosModels
{
    public class MemoDto: MemoBase
    {
        public GroupDto Group { get; set; }
          public UserDto Reporter { get; set; }
          public UserDto Assignee { get; set; }
    }
}