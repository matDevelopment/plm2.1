using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PLM.Dtos;
using PLM.Helpers;
using PLM.IHelpers;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    public class EcpRepository : IEcpRepository
    {
        private readonly PLMContext _context;
        private readonly IQueryHelper _queryHelper;

        public EcpRepository(PLMContext context, IQueryHelper queryHelper)
        {
            _context = context;
            _queryHelper = queryHelper;
        }

        public void subtractTime(List<Ecp> ecps)
        {
            var hours = 0;
            float minutes = 0;
            foreach (var ecp in ecps)
            {
                if (ecp.CaseId != null && ecp.TaskId != null)
                {
                    var @case = _context.Cases.First(c => c.Id == ecp.CaseId);
                    var task = _context.Tasks.First(c => c.Id == ecp.TaskId);
                    TimeSpan interval = ecp.FinishTime - ecp.StartTime;
                    hours = interval.Hours;
                    minutes = (interval.Minutes * 100)/60;
                    @case.TimeTaken = @case.TimeTaken - hours - float.Parse(("0." + minutes.ToString()), CultureInfo.InvariantCulture.NumberFormat);
                    task.TimeTaken = task.TimeTaken - hours - float.Parse(("0." + minutes.ToString()), CultureInfo.InvariantCulture.NumberFormat);
                }
            }
        }

        public void addTime(List<Ecp> ecps)
        {
            var hours = 0;
            float minutes = 0;
            foreach (var ecp in ecps)
            {
                if (ecp.CaseId != null && ecp.TaskId != null)
                {
                    var @case = _context.Cases.First(c => c.Id == ecp.CaseId);
                    var task = _context.Tasks.First(c => c.Id == ecp.TaskId);
                    TimeSpan interval = ecp.FinishTime - ecp.StartTime;
                    hours = interval.Hours;
                    minutes = (interval.Minutes * 100)/60;
                    @case.TimeTaken = @case.TimeTaken + hours + float.Parse(("0." + minutes.ToString()), CultureInfo.InvariantCulture.NumberFormat);
                    task.TimeTaken = task.TimeTaken + hours + float.Parse(("0." + minutes.ToString()), CultureInfo.InvariantCulture.NumberFormat);
                }
            }
        }

        public async Task<IEnumerable<Ecp>> AddEcps(List<Ecp> ecps)
        {

            List<Ecp> ecpsFromDb = await _context.Ecps.Where(e => e.Date == ecps[0].Date).Where(e => e.UserId == ecps[0].UserId).ToListAsync();
            subtractTime(ecpsFromDb);
            addTime(ecps);

            if (ecps.Count < ecpsFromDb.Count)
            {
                for (var i = 0; i < ecps.Count; i++)
                {
                    ecpsFromDb[i].UserId = ecps[i].UserId;
                    ecpsFromDb[i].CaseId = ecps[i].CaseId;
                    ecpsFromDb[i].TaskId = ecps[i].TaskId;
                    ecpsFromDb[i].StartTime = ecps[i].StartTime;
                    ecpsFromDb[i].FinishTime = ecps[i].FinishTime;
                    ecpsFromDb[i].Description = ecps[i].Description;
                }

                for (var i = (ecpsFromDb.Count - 1); i > (ecps.Count - 1); i--)
                {
                    _context.Ecps.Attach(ecpsFromDb[i]);
                    _context.Ecps.Remove(ecpsFromDb[i]);
                }
            }
            else if (ecps.Count > ecpsFromDb.Count)
            {
                for (var i = 0; i < ecpsFromDb.Count; i++)
                {
                    ecpsFromDb[i].UserId = ecps[i].UserId;
                    ecpsFromDb[i].CaseId = ecps[i].CaseId;
                    ecpsFromDb[i].TaskId = ecps[i].TaskId;
                    ecpsFromDb[i].StartTime = ecps[i].StartTime;
                    ecpsFromDb[i].FinishTime = ecps[i].FinishTime;
                    ecpsFromDb[i].Description = ecps[i].Description;
                }

                for (var i = ecpsFromDb.Count; i < ecps.Count; i++)
                {
                    await _context.Ecps.AddAsync(
                        new Ecp
                        {
                            UserId = ecps[i].UserId,
                            CaseId = ecps[i].CaseId,
                            TaskId = ecps[i].TaskId,
                            Date = ecps[i].Date,
                            StartTime = ecps[i].StartTime,
                            FinishTime = ecps[i].FinishTime,
                            Description = ecps[i].Description
                        }
                    );
                }
            }
            else
            {
                for (var i = 0; i < ecpsFromDb.Count; i++)
                {
                    ecpsFromDb[i].UserId = ecps[i].UserId;
                    ecpsFromDb[i].CaseId = ecps[i].CaseId;
                    ecpsFromDb[i].TaskId = ecps[i].TaskId;
                    ecpsFromDb[i].StartTime = ecps[i].StartTime;
                    ecpsFromDb[i].FinishTime = ecps[i].FinishTime;
                    ecpsFromDb[i].Description = ecps[i].Description;
                }
            }

            await _context.SaveChangesAsync();
            return ecpsFromDb;

        }

        public async Task<IEnumerable<Ecp>> GetEcps(FilterDto filter)
        {
            
            var ecps = await _queryHelper.GetEcps(filter);
            return ecps;
        }

        public async Task<IEnumerable<Ecp>> GetAllEcps()
        {
            return await _context.Ecps.Include(e => e.Case).Include(e => e.Task).Include(e => e.User).ToListAsync();

        }

        public async Task<IEnumerable<TimePeriod>> GetTimePeriods()
        {
            return await _context.TimePeriods.ToListAsync();
        }

        public async Task<Ecp> RemoveEcp(Ecp ecp)
        {
            var ecpsToRemove = await _context.Ecps
            .Where(e => e.Date == ecp.Date)
            .Where(e => e.UserId == ecp.UserId).ToListAsync();
            var hours = 0;
            float minutes = 0;
            foreach(var ecpToRemove in ecpsToRemove) {
                if (ecpToRemove.CaseId != null && ecpToRemove.TaskId != null) {
                    TimeSpan timeToSubtract = ecpToRemove.FinishTime - ecpToRemove.StartTime;
                     minutes = (timeToSubtract.Minutes * 100)/60;
                    hours = timeToSubtract.Hours;
                    _context.Cases.First(c => c.Id == ecpToRemove.CaseId).TimeTaken = _context.Cases.First(c => c.Id == ecpToRemove.CaseId).TimeTaken - hours - float.Parse(("0." + minutes.ToString()), CultureInfo.InvariantCulture.NumberFormat);
                    _context.Tasks.First(t => t.Id == ecpToRemove.TaskId).TimeTaken = _context.Tasks.First(t => t.Id == ecpToRemove.TaskId).TimeTaken - hours - float.Parse(("0." + minutes.ToString()), CultureInfo.InvariantCulture.NumberFormat);
                }
                _context.Ecps.Attach(ecpToRemove);
                _context.Ecps.Remove(ecpToRemove);
            }
            _context.SaveChanges();
            return ecp;
        }

        public DateTime GetTodaysDate() {
            return DateTime.Now;
        }

        public async Task<IEnumerable<bool>> GetReportedDaysInMonth(int year, int month)
        {
            int daysInMonth = DateTime.DaysInMonth(year, month);
            bool[] days = new bool[daysInMonth];
            for (var i = 1; i < daysInMonth; i++) {
                var date = new DateTime(year, month, i);
                if (await _context.Ecps.FirstOrDefaultAsync(e => e.Date == date) != null)
                    days[i - 1] = true;
                else
                    days[i - 1] = false;
            }
            return days;
        }

        public async Task<IEnumerable<WorkDay>> GetReportedDaysInMonthForUser(int year, int month, int userId)
        {
            int daysInMonth = DateTime.DaysInMonth(year, month);
            WorkDay[] days = new WorkDay[daysInMonth];
            for (var i = 1; i <= daysInMonth; i++) {
                var date = new DateTime(year, month, i);
                var ecps = await _context.Ecps.Where(e => e.Date == date && e.UserId == userId).ToListAsync();
                var TimeWorkedHours = 0;
                var TimeWorkedMinutes = 0;
                ecps.ForEach(ecp => {
                    TimeWorkedHours += ecp.TimeWorkedHours;
                    TimeWorkedMinutes += ecp.TimeWorkedMinutes;
                });
                days[i - 1] = new WorkDay();
                if (TimeWorkedHours != 0 || TimeWorkedMinutes != 0) {
                    days[i - 1].Worked = true;
                    var hours = "0" + TimeWorkedHours.ToString();
                    var minutes = "0" + TimeWorkedMinutes.ToString();
                    hours = hours.Length == 3 ? hours.Substring(1) : hours;
                    minutes = minutes.Length == 3 ? minutes.Substring(1) : minutes;
                    days[i - 1].Time = hours + "h : " + minutes + "m";
                }    
                else {
                    days[i - 1].Worked = false;
                    days[i - 1].Time = null;
                }
                    
            }
            return days;
        }

       
    }

}