using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PLM.DtosModels;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly PLMContext _context;

        public NotificationRepository(PLMContext context)
        {
            _context = context;
        }

        public async Task<Notification> AddNotification(Notification notification)
        {
            await _context.Notifications.AddAsync(notification);
            await _context.SaveChangesAsync();
            return notification;
        }

        public async Task<IEnumerable<Notification>> GetNotificationsForUser(int userId)
        {
            return await _context.Notifications.Where(n => n.UserId == userId).ToListAsync();
        }

        public async Task<IEnumerable<Notification>> GetSeenNotificationsForUser(int userId)
        {
            return await _context.Notifications.Where(n => n.UserId == userId && n.Seen == true).ToListAsync();
        }

        public async Task<IEnumerable<Notification>> GetUnseenNotificationsForUser(int userId)
        {
            return await _context.Notifications.Where(n => n.UserId == userId && n.Seen == false).ToListAsync();
        }
    }
}