using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PLM.IRepositories;
using PLM.Models;
using System;
using System.Linq;
using PLM.Dtos;
using AutoMapper;
using PLM.DtosModels;
using PLM.Helpers;
using PLM.IHelpers;

namespace PLM.Repositories
{
    public class GroupRepository : IGroupRepository
    {
        private readonly PLMContext _context;
        private readonly IMapper _mapper;
        private readonly IQueryHelper _queryHelper;

        public GroupRepository(PLMContext context, IMapper mapper, IQueryHelper queryHelper)
        {
            _context = context;
            _mapper = mapper;
            _queryHelper = queryHelper;
        }

        public async Task<GroupDto> AddGroup(GroupDto groupWithUsers)
        {
            var group = new Group { Name = groupWithUsers.Name, TeamLeaderId = groupWithUsers.TeamLeaderId };
            await _context.Groups.AddAsync(group);
            await _context.SaveChangesAsync();

            foreach (var user in groupWithUsers.Users)
            {
                await _context.GroupsUsers.AddAsync(new GroupUser { Group = group, User = await _context.Users.FirstAsync(u => u.Id == user.Id) });
            }

            await _context.SaveChangesAsync();
            return groupWithUsers;
        }

        public bool CheckIfUserIsTeamLeader(int UserId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Group> RemoveGroup(int id)
        {

            var groupToRemove = await _context.Groups.FirstAsync(g => g.Id == id);
            var groupsUsers = await _context.GroupsUsers.Where(gu => gu.Group.Id == groupToRemove.Id).ToListAsync();
            foreach (var groupUser in groupsUsers)
            {
                _context.GroupsUsers.Attach(groupUser);
                _context.GroupsUsers.Remove(groupUser);
            }
            _context.Groups.Attach(groupToRemove);
            _context.Groups.Remove(groupToRemove);
            await _context.SaveChangesAsync();
            return groupToRemove;
        }

        public void EditGroup(Group group, int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IEnumerable<Group>> GetGroups()
        {
            return await _context.Groups.ToListAsync();
        }

        public Task<IEnumerable<Group>> GetGroupsForTeamLeader(int teamLeaderId)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<Group>> GetGroupsForUser(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool CompareIds(User u, int id)
        {
            return u.Id == id;
        }

        public GroupDto RemoveTeamLeaderFromGroup(GroupDto group)
        {
            var editedGroup = _context.Groups.First(g => g.Id == group.Id);
            editedGroup.TeamLeaderId = null;
            _context.SaveChanges();
            return group;
        }
        public GroupDto SetGroupTeamLeader(GroupDto group)
        {
            var editedGroup = _context.Groups.First(g => g.Id == group.Id);
            editedGroup.TeamLeaderId = group.TeamLeaderId;
            _context.SaveChanges();
            return group;
        }

        public async Task<IEnumerable<GroupDto>> GetGroupsWithUsers()
        {
            var groups = (IEnumerable<Group>)await _context.Groups.ToListAsync();

            var groupsWithUsers = new List<GroupDto>();

            foreach (var group in groups)
            {
                var groupWithUser = new GroupDto();
                groupWithUser.Id = group.Id;
                groupWithUser.Name = group.Name;
                groupWithUser.TeamLeaderId = group.TeamLeaderId;
                var groupsUsers = _context.GroupsUsers.Include(gu => gu.User).ToList().Where(gu => gu.Group.Id == group.Id);
                var users = new List<User>();
                foreach (var groupUser in groupsUsers)
                {
                    users.Add(groupUser.User);
                }
                groupWithUser.Users = _mapper.Map<IEnumerable<UserDto>>(users);
                groupsWithUsers.Add(groupWithUser);
            }
            return groupsWithUsers;
        }

        public async Task<IEnumerable<GroupDto>> GetFilteredGroups(FilterDto filter)
        {
            var groups = new List<Group>();
            if (filter.UserId != null && filter.TeamLeaderId == null)
            {
                groups = await _queryHelper.GetGroupsForUser(filter.UserId.Value);
            } else if (filter.UserId == null && filter.TeamLeaderId != null) {
                groups = await _context.Groups.Where(g => g.TeamLeaderId == filter.TeamLeaderId).ToListAsync();
            } else if (filter.UserId != null && filter.TeamLeaderId != null) {
                groups = await _queryHelper.GetGroupsForUser(filter.UserId.Value);
                groups.AddRange(await _context.Groups.Where(g => groups.FirstOrDefault(group => group.Id == g.Id) == null && g.TeamLeaderId == filter.TeamLeaderId).ToListAsync());
            }
            else 
            {
                groups = await _context.Groups.ToListAsync();
            }

            var groupsDto = _mapper.Map<List<GroupDto>>(groups);
            foreach (var groupDto in groupsDto)
            {
                var users = from user in _context.Users
                            join groupUser in _context.GroupsUsers on user.Id equals groupUser.User.Id
                            where groupUser.Group.Id == groupDto.Id
                            select _mapper.Map<User>(user);
                groupDto.Users = _mapper.Map<List<UserDto>>(await users.ToListAsync());
            }
            return groupsDto;
        }

        public async Task<GroupDto> RemoveUserFromGroup(int userId, int groupId)
        {
            var groupUser = await _context.GroupsUsers.Include(g => g.Group).FirstAsync(g => g.Group.Id == groupId && g.User.Id == userId );
            _context.GroupsUsers.Attach(groupUser);
            _context.GroupsUsers.Remove(groupUser);
            var group = await _context.Groups.FirstOrDefaultAsync(g => g.Id == groupId);
            group.UsersAmount--;
            await _context.SaveChangesAsync();
            return _mapper.Map<GroupDto>(groupUser.Group);
        }
    }
}