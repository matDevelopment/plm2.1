using System;
using System.Collections.Generic;
using System.IO;
using PLM.Dtos;
using PLM.IRepositories;

namespace PLM.Repositories
{
    public class FileRepository : IFileRepository
    {
        public FileRepository()
        {
        }

        public void AddDirectory(string path)
        {
            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(path))
                {
                    return;
                }

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(path);

            }
            catch (Exception e)
            {
                return;
            }
        }

        public void AddFile(string path)
        {
            throw new System.NotImplementedException();
        }

        public List<DirectoryDto> GetFilesAndDirectories(string path)
        {
            string[] fileEntries = new string[0];
            string[] directoriesEntries = new string[0];
            try
            {
                fileEntries = Directory.GetFiles(path);
            }
            catch
            {

            }
            try
            {
                directoriesEntries = Directory.GetDirectories(path);
            }
            catch
            {

            }
            List<DirectoryDto> directories = new List<DirectoryDto>();

            foreach (string fileName in directoriesEntries)
            {
                var fileDirectory = new DirectoryDto();
                fileDirectory.Name = Path.GetFileName(fileName);
                fileDirectory.HasChildren = true;
                directories.Add(fileDirectory);
            }
            foreach (string fileName in fileEntries)
            {
                var fileDirectory = new DirectoryDto();
                fileDirectory.Name = Path.GetFileName(fileName);
                fileDirectory.HasChildren = false;
                directories.Add(fileDirectory);
            }
            return directories;
        }

        public void RemoveDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }

        public void RemoveFile(string path)
        {
            throw new System.NotImplementedException();
        }

        public FileStream DownloadFile(string path)
        {

            var stream = new FileStream(path, FileMode.Open);
            return stream;
        }
    }
}