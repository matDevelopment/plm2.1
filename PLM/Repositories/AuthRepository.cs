using System.Threading.Tasks;
using PLM.IRepositories;
using PLM.Models;
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using Microsoft.AspNetCore.Identity;
using PLM.Dtos;

namespace PLM.Repositories
{
   public class AuthRepository : IAuthRepository
   {
      private readonly PLMContext _context;
      private readonly PLMDeletedContext _deletedContext;
    private readonly UserManager<User> _userManager;

        public AuthRepository(PLMContext context, PLMDeletedContext deletedContext, UserManager<User> userManager)
      {
         _context = context;
         _deletedContext = deletedContext;
         _userManager = userManager;
        }
      public async Task<User> Login(string username, string password)
      {
        var user = await _context.Users.FirstOrDefaultAsync(x => x.Name == username);

        if (user == null)
            return null;

        return user;
            
      }

      private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
      {
         using(var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
          {
              var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
              for(int i = 0; i < computedHash.Length; i++)
              {
                  if (computedHash[i] != passwordHash[i]) 
                    return false;
              }
          }
          return true;
      }

      public async Task<User> Register(User user, string password)
      {
         byte[] passwordHash, passwordSalt;
         CreatePasswordHash(password, out passwordHash, out passwordSalt);
         await _context.Users.AddAsync(user);
         await _context.SaveChangesAsync();

         return user;
      }

      private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
      {
          using(var hmac = new System.Security.Cryptography.HMACSHA512())
          {
              passwordSalt = hmac.Key;
              passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
          }
         
      }

      public async Task<bool> UserExists(string username)
      {
         if (await _context.Users.AnyAsync(x => x.Name == username))
            return true;

        return false;
      }

      public async Task<IEnumerable<User>> AddOrCompareUsersFromActiveDirectory(string domain, string type)
        {
            var users = new List<User>();
            using (var context = new PrincipalContext(ContextType.Domain, domain))
            {
                using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                {
                    foreach (var result in searcher.FindAll())
                    {
                        DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                        if (!String.IsNullOrWhiteSpace((string)de.Properties["givenName"].Value) && await _context.Users.FirstOrDefaultAsync(u => u.UserName == (string)de.Properties["givenName"].Value ) == null)
                        {
                            var user = new User
                            {
                                UserName = (string)de.Properties["samAccountName"].Value,
                                Name = (string)de.Properties["samAccountName"].Value,
                                FirstName = (string)de.Properties["givenName"].Value,
                                Surname = (string)de.Properties["sn"].Value,
                                Domain = domain,
                            };
                            var password = "qwert1";
                            //password = password.GenerateRandomString();
                            await _userManager.CreateAsync(user, password);
                            users.Add(await _userManager.FindByNameAsync(user.UserName));
                        }
                    }
                }
            }
            return users;
        }
   }
}