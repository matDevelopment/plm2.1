using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PLM.Dtos;
using PLM.IHelpers;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    [AllowAnonymous]
    public class UserRepository : IUserRepository
    {
        private readonly PLMContext _context;
        private readonly UserManager<User> _userManager;
        private readonly IQueryHelper _queryHelper;

        public UserRepository(PLMContext context, UserManager<User> userManager, IQueryHelper queryHelper)
        {
            _context = context;
            _userManager = userManager;
            _queryHelper = queryHelper;
        }

        public async Task<User> AddUser(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<User> AddUserToGroup(User user, Group group)
        {
            await _context.GroupsUsers.AddAsync(new GroupUser { User = await _context.Users.FirstAsync(u => u.Id == user.Id), Group = await _context.Groups.FirstAsync(g => g.Id == group.Id) });
            await _context.SaveChangesAsync();
            var groupToEdit = _context.Groups.FirstOrDefault(g => g.Id == group.Id);
            groupToEdit.UsersAmount++;
            return user;
        }

        public void ChangeUserRole()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Object>> GetUsers()
        {
            var userList = await (from user in _context.Users
                                  orderby user.UserName
                                  select new
                                  {
                                      Id = user.Id,
                                      UserName = user.UserName,
                                      Name = user.UserName,
                                      FirstName = user.FirstName,
                                      Surname = user.Surname,
                                      Email = user.Email,
                                      Roles = (from UserRole in user.UserRoles
                                               join role in _context.Roles
                                               on UserRole.RoleId
                                               equals role.Id
                                               select role.Name).ToList()
                                  }).ToListAsync();
            return userList;
        }

        public async Task<bool> ChangeRole(string userName, string[] RoleNames)
        {
            var user = await _userManager.FindByNameAsync(userName);
            var userRoles = await _userManager.GetRolesAsync(user);
            _userManager.RemoveFromRolesAsync(user, userRoles).Wait();

            var result = await _userManager.AddToRolesAsync(user, RoleNames);

            return result.Succeeded;
        }

        public async Task<IEnumerable<User>> GetUsersForUser(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            List<User> users = new List<User>();
            List<GroupUser> groupUsers = new List<GroupUser>();
            var userGroups = _context.GroupsUsers.Include(gu => gu.Group).Where(g => g.User.Id == user.Id);
            foreach (var userGroup in userGroups)
            {
                groupUsers.AddRange(_context.GroupsUsers.Include(gu => gu.User).Where(gu => ((groupUsers.FirstOrDefault(g => g.Id == userGroup.Group.Id) == null) && gu.Group.Id == userGroup.Group.Id)).ToList());
            }
            foreach (var groupUser in groupUsers)
            {
                users.Add(await _context.Users.FirstAsync(u => (users.FirstOrDefault(us => us.Id == groupUser.User.Id) == null) && u.Id == groupUser.User.Id));
            }
            return users;
        }

        public async Task<IEnumerable<Role>> GetRoles()
        {
            return (await _context.Roles.ToListAsync());
        }

        public async Task<IEnumerable<User>> GetUsersForEcpDay(DateTime date, string userName)
        {
            IEnumerable<User> userUsers = new List<User>();
            var usersToReturn = new List<User>();
            if (userName != null)
                userUsers = await GetUsersForUser(userName);

            var ecps = await _context.Ecps.Where(e => e.Date == date).ToListAsync();
            var users = new List<User>();
            foreach (var ecp in ecps)
            {
                users.AddRange(await _context.Users.Where(u => u.Id == ecp.UserId).ToListAsync());
            }

            foreach (var userUser in userUsers)
            {
                if (users.Any(u => u.Id == userUser.Id))
                    usersToReturn.Add(userUser);
            }
            return usersToReturn;
        }

        public Task<IEnumerable<User>> GetAllUserWithVisibility()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetTeamLeaderUsersForEcpDay(int teamLeaderId, DateTime date)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<User>> GetUsersForGroup(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetUsersFromGroupForTeamLeader(int userId)
        {
            throw new NotImplementedException();
        }

        public void RemoveUserFromGroup(User user, Group group)
        {
            var groupuser = _context.GroupsUsers.First(gu => gu.Group.Id == group.Id && gu.User.Id == user.Id);
            _context.GroupsUsers.Attach(groupuser);
            _context.GroupsUsers.Remove(groupuser);
            var groupToEdit = _context.Groups.FirstOrDefault(g => g.Id == group.Id);
            groupToEdit.UsersAmount--;
            _context.SaveChanges();
        }

        public async Task<List<User>> GetFilteredUsers(FilterDto filterDto)
        {
            var users = await _context.Users.ToListAsync();
            if (filterDto.TeamLeaderId != null)
            {
                users = new List<User>();
                var groups = await _context.Groups.Where(g => g.TeamLeaderId == filterDto.TeamLeaderId).ToListAsync();
                users.Add(await _context.Users.FirstAsync(u => u.Id == filterDto.TeamLeaderId.Value));
                foreach (var group in groups)
                {
                    foreach (var user in await _queryHelper.getUsersInGroup(group.Id))
                    {
                        if (users.FirstOrDefault(u => u.Id == user.Id) == null)
                        {
                            users.Add(user);
                        }
                    }
                }
            }

            if (filterDto.UserId != null)
            {
                users = new List<User>();
                var groups = await _queryHelper.GetGroupsForUser(filterDto.UserId.Value);
                foreach(var group in groups) {
                    foreach (var user in await _queryHelper.getUsersInGroup(group.Id))
                    {
                        if (users.FirstOrDefault(u => u.Id == user.Id) == null)
                        {
                            users.Add(user);
                        }
                    }
                }
            }

            return users;
        }

        public async Task<User> GetUser(int id) {
            return await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<List<User>> GetTeamLeaders()
        {
            return await _context.Users.Where(u => u.UserRoles.FirstOrDefault(role => role.Role.Name == Helpers.RolesHelper.User) == null).ToListAsync();
        }

        public async Task<User> EditUser(User user)
        {
            var userToEdit = await _userManager.FindByIdAsync(user.Id.ToString());
            var token = await _userManager.GenerateChangeEmailTokenAsync(userToEdit, user.Email);
            await _userManager.ChangeEmailAsync(userToEdit, user.Email, token);
            userToEdit.UserName = user.Name;
            userToEdit.Name = user.Name;
            userToEdit.FirstName = user.FirstName;
            userToEdit.NormalizedUserName = user.Name.ToUpper();
            userToEdit.Surname = user.Surname;
            await _context.SaveChangesAsync();
            return user;
        }
    }
}