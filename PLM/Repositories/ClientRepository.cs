using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private readonly PLMContext _context;
        public ClientRepository(PLMContext context)
        {
            _context = context;
        }

        public async Task<Client> AddNewClient(Client client)
        {
            await _context.Clients.AddAsync(client);
            await _context.SaveChangesAsync();
            return client;
        }

        public async Task<Client> GetClient(int id)
        {
            return await _context.Clients.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<Client> RemoveClient(Client client) {
            var clientToRemove = await _context.Clients.FirstAsync(c => c.Id == client.Id);
            _context.Clients.Attach(clientToRemove);
            _context.Clients.Remove(clientToRemove);
            _context.SaveChanges();
            return clientToRemove;
        }

        public async Task<IEnumerable<Client>> GetClients()
        {
            return await _context.Clients.ToListAsync();
        }

        public async Task<Client> EditClient(Client client) {
            var clientToEdit = await _context.Clients.FirstAsync(c => c.Id == client.Id);
            _context.Entry(clientToEdit).CurrentValues.SetValues(client);
            await _context.SaveChangesAsync();
            return await _context.Clients.FirstAsync(c => c.Id == client.Id);
        }

        public async Task<IEnumerable<Representative>> GetRepresentativesAsync(int id) {
            return await _context.Representatives.Where(r => r.ClientId == id).ToListAsync();
        }

        public async Task<Representative> AddRepresentativeAsync(Representative representative) {
            await _context.Representatives.AddAsync(representative);
            await _context.SaveChangesAsync();
            return representative;
        }

        public async Task<Representative> RemoveRepresentativeAsync(Representative representative) {
            var representativeToRemove = await _context.Representatives.FirstAsync(c => c.Id == representative.Id);
            _context.Representatives.Attach(representativeToRemove);
            _context.Representatives.Remove(representativeToRemove);
            _context.SaveChanges();
            return representativeToRemove;
        }

        public async Task<Representative> EditRepresentativesAsync(Representative representative) {
            var representativeToEdit = await _context.Representatives.FirstAsync(c => c.Id == representative.Id);
            _context.Entry(representativeToEdit).CurrentValues.SetValues(representative);
            await _context.SaveChangesAsync();
            return await _context.Representatives.FirstAsync(c => c.Id == representative.Id);
        }
    }
}