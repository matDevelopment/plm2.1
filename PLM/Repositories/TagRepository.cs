using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PLM.DtosModels;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    public class TagRepository : ITagRepository
    {
        private readonly PLMContext _context;

        public TagRepository(PLMContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<CaseTagChild>> GetCaseTagsChild(int id)
        {
            return await _context.CaseTagChildren.Where(t => t.CaseTagParentId == id).ToListAsync();
        }

        public async Task<IEnumerable<CaseTagParent>> GetCaseTagsParent()
        {
            return await _context.CaseTagParents.ToListAsync();
        }

        public async Task<IEnumerable<TaskTagAccessory>> GetTaskTagsAccessory(int id)
        {
            return await _context.TaskTagsAccessory.Where(t => t.TaskTagMainId == id).ToListAsync();
        }

        public async Task<IEnumerable<TaskTagAccessoryMinor>> GetTaskTagsAccessoryMinor(int id)
        {
            return await _context.TaskTagsAccessoryMinor.Where(t => t.TaskTagAccessoryId == id).ToListAsync();
        }

        public async Task<IEnumerable<TaskTagMain>> GetTaskTagsMain(int id)
        {
            return await _context.TaskTagsMain.Where(t => t.CaseTagParentId == id).ToListAsync();
        }

        public async Task<CaseTagParent> AddCaseTagParent(TagDto tag)
        {
            var tagForAdd = new CaseTagParent { Name = tag.Name };
            await _context.CaseTagParents.AddAsync(tagForAdd);
            await _context.SaveChangesAsync();
            return tagForAdd;
        }

        public async Task<CaseTagChild> AddCaseTagChild(TagDto tag)
        {
            var tagForAdd = new CaseTagChild { Name = tag.Name, CaseTagParentId = tag.CaseTagParentId ?? default(int) };
            await _context.CaseTagChildren.AddAsync(tagForAdd);
            await _context.SaveChangesAsync();
            return tagForAdd;
        }

        public async Task<TaskTagMain> AddTaskTagMain(TagDto tag)
        {
            var tagForAdd = new TaskTagMain { Name = tag.Name, CaseTagParentId = tag.CaseTagParentId.Value };
            await _context.TaskTagsMain.AddAsync(tagForAdd);
            await _context.SaveChangesAsync();
            return tagForAdd;
        }
        public async Task<TaskTagAccessory> AddTaskTagAccessory(TagDto tag)
        {
            var tagForAdd = new TaskTagAccessory { Name = tag.Name, TaskTagMainId = tag.CaseTagParentId ?? default(int) };
            await _context.TaskTagsAccessory.AddAsync(tagForAdd);
            await _context.SaveChangesAsync();
            return tagForAdd;
        }
        public async Task<TaskTagAccessoryMinor> AddTaskTagAccessoryMinor(TagDto tag)
        {
            var tagForAdd = new TaskTagAccessoryMinor { Name = tag.Name, TaskTagAccessoryId = tag.CaseTagParentId ?? default(int) };
            await _context.TaskTagsAccessoryMinor.AddAsync(tagForAdd);
            await _context.SaveChangesAsync();
            return tagForAdd;
        }

        public async Task<CaseTagParent> RemoveCaseTagParent(TagDto tag)
        {
            var tagForRemove = await _context.CaseTagParents
            .FirstAsync(t => t.Id == tag.Id);
            _context.CaseTagParents.Attach(tagForRemove);
            _context.CaseTagParents.Remove(tagForRemove);
            await _context.SaveChangesAsync();
            return tagForRemove;
        }

        public async Task<CaseTagChild> RemoveCaseTagChild(TagDto tag)
        {
            var tagForRemove = await _context.CaseTagChildren.FirstAsync(t => t.Id == tag.Id);
            _context.CaseTagChildren.Attach(tagForRemove);
            _context.CaseTagChildren.Remove(tagForRemove);
            await _context.SaveChangesAsync();
            return tagForRemove;
        }

        public async Task<TaskTagMain> RemoveTaskTagMain(TagDto tag)
        {
            var tagForRemove = await _context.TaskTagsMain.FirstAsync(t => t.Id == tag.Id);
            _context.TaskTagsMain.Attach(tagForRemove);
            _context.TaskTagsMain.Remove(tagForRemove);
            await _context.SaveChangesAsync();
            return tagForRemove;
        }
        public async Task<TaskTagAccessory> RemoveTaskTagAccessory(TagDto tag)
        {
            var tagForRemove = await _context.TaskTagsAccessory.FirstAsync(t => t.Id == tag.Id);
            _context.TaskTagsAccessory.Attach(tagForRemove);
            _context.TaskTagsAccessory.Remove(tagForRemove);
            await _context.SaveChangesAsync();
            return tagForRemove;
        }
        public async Task<TaskTagAccessoryMinor> RemoveTaskTagAccessoryMinor(TagDto tag)
        {
            var tagForRemove = await _context.TaskTagsAccessoryMinor.FirstAsync(t => t.Id == tag.Id);
            _context.TaskTagsAccessoryMinor.Attach(tagForRemove);
            _context.TaskTagsAccessoryMinor.Remove(tagForRemove);
            await _context.SaveChangesAsync();
            return tagForRemove;
        }

        public async Task<CaseTagParent> EditCaseTagParent(TagDto tag)
        {
            var tagForEdit = await _context.CaseTagParents.FirstAsync(t => t.Id == tag.Id);
            tagForEdit.Name = tag.Name;
            await _context.SaveChangesAsync();
            return tagForEdit;
        }

        public async Task<CaseTagChild> EditCaseTagChild(TagDto tag)
        {
            var tagForEdit = await _context.CaseTagChildren.FirstAsync(t => t.Id == tag.Id);
            tagForEdit.Name = tag.Name;
            await _context.SaveChangesAsync();
            return tagForEdit;
        }

        public async Task<TaskTagMain> EditTaskTagMain(TagDto tag)
        {
            var tagForEdit = await _context.TaskTagsMain.FirstAsync(t => t.Id == tag.Id);
            tagForEdit.Name = tag.Name;
            await _context.SaveChangesAsync();
            return tagForEdit;
        }

        public async Task<TaskTagAccessory> EditTaskTagAccessory(TagDto tag)
        {
            var tagForEdit = await _context.TaskTagsAccessory.FirstAsync(t => t.Id == tag.Id);
            tagForEdit.Name = tag.Name;
            await _context.SaveChangesAsync();
            return tagForEdit;
        }

        public async Task<TaskTagAccessoryMinor> EditTaskTagAccessoryMinor(TagDto tag)
        {
            var tagForEdit = await _context.TaskTagsAccessoryMinor.FirstAsync(t => t.Id == tag.Id);
            tagForEdit.Name = tag.Name;
            await _context.SaveChangesAsync();
            return tagForEdit;
        }

        public async Task<TaskTagMain> CopyTaskTagMainStructure(TaskTagMain tag) {
            var taskTagMain = await _context.TaskTagsMain.FirstAsync(t => t.Id == tag.Id);
            var newTag = new TaskTagMain {
                Name = taskTagMain.Name,
                CaseTagParentId = taskTagMain.CaseTagParentId
            };
            await _context.TaskTagsMain.AddAsync(newTag);
            await _context.SaveChangesAsync();
            var tasksTagAccesory = await _context.TaskTagsAccessory.Where(t => t.TaskTagMainId == tag.Id).ToListAsync();
            foreach (var taskTagAccessory in tasksTagAccesory) { 
               await CopyTaskTagAccessoryStructure(taskTagAccessory, newTag.Id);
            }
            return newTag;
        }

        public async Task<TaskTagAccessory> CopyTaskTagAccessoryStructure(TaskTagAccessory taskTagAccessory, int taskTagMainId = 0) {
                if (taskTagMainId == 0) {
                    taskTagMainId = taskTagAccessory.TaskTagMainId;
                } 
                var newTaskTagAccessory = new TaskTagAccessory { Name = taskTagAccessory.Name, TaskTagMainId = taskTagMainId };
                await _context.TaskTagsAccessory.AddAsync(newTaskTagAccessory);
                await _context.SaveChangesAsync();
                var taskTagsAccessoryMinor = await _context.TaskTagsAccessoryMinor.Where(t => t.TaskTagAccessoryId == taskTagAccessory.Id).ToListAsync();
                foreach (var taskTagAccessoryMinor in taskTagsAccessoryMinor) {
                    await CopyTaskTagAccessoryMinorStructure(taskTagAccessoryMinor, newTaskTagAccessory.Id);
                }
                return newTaskTagAccessory;
        }

        public async Task<TaskTagAccessoryMinor> CopyTaskTagAccessoryMinorStructure(TaskTagAccessoryMinor taskTagAccessoryMinor, int taskTagAccessoryId = 0) {
                if (taskTagAccessoryId == 0) {
                    taskTagAccessoryId = taskTagAccessoryMinor.TaskTagAccessoryId;
                } 
                var newTaskTagAccessoryMinor = new TaskTagAccessoryMinor { Name = taskTagAccessoryMinor.Name, TaskTagAccessoryId = taskTagAccessoryId };
                await _context.TaskTagsAccessoryMinor.AddAsync(newTaskTagAccessoryMinor);
                await _context.SaveChangesAsync();
                return newTaskTagAccessoryMinor;
        }
    }
}