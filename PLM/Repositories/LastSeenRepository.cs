using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PLM.DtosModels;
using PLM.IHelpers;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    public class LastSeenRepository : ILastSeenRepository
    {
        private readonly PLMContext _context;
        private readonly IQueryHelper _queryHelper;
        private readonly IMapper _mapper;

        public LastSeenRepository(PLMContext context, IQueryHelper queryHelper, IMapper mapper)
        {
            _context = context;
            _queryHelper = queryHelper;
            _mapper = mapper;
        }

        public async Task<LastSeen> AddLastSeen(LastSeen lastSeen)
        {
            if (await checkIfLastSeenIsAlreadyAdded(lastSeen))
            {
                var lastSeenArr = await _context.LastSeen.Where(l => l.UserId == lastSeen.UserId).ToListAsync();
                if (lastSeenArr.Count == 10)
                {
                    _context.LastSeen.Remove(lastSeenArr[0]);
                }
                await _context.LastSeen.AddAsync(lastSeen);
                await _context.SaveChangesAsync();
            }
            return lastSeen;
        }

        public async Task<bool> checkIfLastSeenIsAlreadyAdded(LastSeen lastSeen)
        {
            var allow = false;
            if (lastSeen.CaseId != null)
            {
                if (await _context.LastSeen.FirstOrDefaultAsync(l => l.CaseId == lastSeen.CaseId && l.UserId == lastSeen.UserId) == null)
                {
                    allow = true;
                }
            }

            if (lastSeen.TaskId != null)
            {
                if (await _context.LastSeen.FirstOrDefaultAsync(l => l.TaskId == lastSeen.TaskId && l.UserId == lastSeen.UserId) == null)
                {
                    allow = true;
                }
            }
            return allow;
        }

        public async Task<IEnumerable<LastSeenDto>> GetLastSeen(int userId)
        {
            var list = await _context.LastSeen.Where(l => l.UserId == userId).ToListAsync();
            Stack<LastSeenDto> stack = new Stack<LastSeenDto>();
            foreach (var lastSeen in list) {
                var lastSeenDto = _mapper.Map<LastSeenDto>(lastSeen);
                if (lastSeen.CaseId != null) {
                    var cases =  await _queryHelper.GetFilteredCases(new Dtos.FilterDto { CaseId = lastSeenDto.CaseId });
                    lastSeenDto.Case = _mapper.Map<CaseDto>(cases[0]);
                }
                if (lastSeenDto.TaskId != null) {
                    var tasks = await _queryHelper.GetFilteredTasks(new Dtos.FilterDto { TaskId = lastSeenDto.TaskId });
                    lastSeenDto.Task = tasks[0];
                    lastSeenDto.Task = _queryHelper.GetUsersForTask(_mapper.Map<Models.Task>(lastSeenDto.Task));
                }
                stack.Push(lastSeenDto);
            }
            return stack;
        }
    }
}