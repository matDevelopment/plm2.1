using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PLM.Models;

namespace PLM.Repositories
{
    public class TemplateRepository : ITemplateRepository
    {
        private readonly PLMContext _context;

        public TemplateRepository(PLMContext context)
        {
            _context = context;
        }

        public async Task<TaskTemplate> AddTaskTemplate(TaskTemplate template)
        {
            var templateToAdd = new TaskTemplate
            {
                Name = template.Name,
                TaskName = template.TaskName,
                CaseId = template.CaseId,
                PriorityId = template.PriorityId,
                TimeEstimated = template.TimeEstimated,
                TaskTagMainId = template.TaskTagMainId,
                TaskTagAccessoryId = template.TaskTagAccessoryId,
                TaskTagAccessoryMinorId = template.TaskTagAccessoryMinorId,
                TemplateId = template.TemplateId
            };
            await _context.TaskTemplates.AddAsync(templateToAdd);
            await _context.SaveChangesAsync();
            return await _context.TaskTemplates
            .Include(t => t.Case)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority)
            .FirstAsync(t => t.Id == templateToAdd.Id);
        }

        public async Task<Template> AddTemplate(Template template)
        {
            await _context.Templates.AddAsync(template);
            await _context.SaveChangesAsync();
            return await _context.Templates
            .Include(t => t.CaseTagParent)
            .Include(t => t.CaseTagChild)
            .Include(t => t.Client)
            .Include(t => t.Priority)
            .Include(t => t.Representative)
            .FirstAsync(t => t.Id == template.Id);
        }

        public async Task<TaskTemplate> EditTaskTemplate(TaskTemplate template)
        {
            var editedTemplate = await _context.TaskTemplates
           .Include(t => t.Case)
           .Include(t => t.TaskTagMain)
           .Include(t => t.TaskTagAccessory)
           .Include(t => t.TaskTagAccessoryMinor)
           .Include(t => t.Priority)
           .FirstAsync(t => t.Id == template.Id);

            _context.Entry(editedTemplate).CurrentValues.SetValues(template);
            await _context.SaveChangesAsync();

            return await _context.TaskTemplates
            .Include(t => t.Case)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority)
            .FirstAsync(t => t.Id == template.Id); ;
        }

        public async Task<Template> EditTemplate(Template template)
        {
            var editedTemplate = await _context.Templates.FirstAsync(t => t.Id == template.Id);

            _context.Entry(editedTemplate).CurrentValues.SetValues(template);
            
            await _context.SaveChangesAsync();

            return await _context.Templates
            .Include(t => t.CaseTagParent)
            .Include(t => t.CaseTagChild)
            .Include(t => t.Client)
            .Include(t => t.Priority)
            .Include(t => t.Representative)
            .Include(t => t.TaskTemplates)
            .Include(t => t.TaskTemplates)
            .FirstAsync(t => t.Id == template.Id);
        }

        public async Task<TaskTemplate> GetTaskTemplate(int id)
        {
            return await _context.TaskTemplates
            .Include(t => t.Case)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority).FirstAsync(t => t.Id == id);
        }

        public async Task<IEnumerable<TaskTemplate>> GetTaskTemplates()
        {
            return await _context.TaskTemplates
            .Include(t => t.Case)
            .Include(t => t.Template)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority).ToListAsync();
        }

        public async Task<Template> GetTemplate(int id)
        {
            return await _context.Templates
            .Include(t => t.CaseTagParent)
            .Include(t => t.CaseTagChild)
            .Include(t => t.Client)
            .Include(t => t.Priority)
            .Include(t => t.Representative)
            .FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<IEnumerable<Template>> GetTemplates()
        {
            var templates =  await _context.Templates
            .Include(t => t.CaseTagParent)
            .Include(t => t.CaseTagChild)
            .Include(t => t.Client)
            .Include(t => t.Priority)
            .Include(t => t.Representative)
            .ToListAsync();

            foreach (var template in templates) {
                template.TaskTemplates = await _context.TaskTemplates
                .Include(t => t.TaskTagMain)
                .Include(t => t.TaskTagAccessory)
                .Include(t => t.TaskTagAccessoryMinor)
                .Where(t => t.TemplateId == template.Id).ToListAsync();
            }
            return templates;
        }
 
        public async Task<TaskTemplate> RemoveTaskTemplate(TaskTemplate template)
        {
            var templateToRemove = await _context.TaskTemplates.FirstAsync(c => c.Id == template.Id);
            _context.TaskTemplates.Attach(templateToRemove);
            _context.TaskTemplates.Remove(templateToRemove);
            _context.SaveChanges();
            return template;
        }

        public async Task<Template> RemoveTemplate(Template template)
        {
            var templateToRemove = await _context.Templates.FirstAsync(c => c.Id == template.Id);
            _context.Templates.Attach(templateToRemove);
            _context.Templates.Remove(templateToRemove);
            _context.SaveChanges();
            return template;
        }

        public async Task<Template> CopyCaseToTemplate(int caseId)
        {
            var caseModel = await _context.Cases.FirstAsync(c => c.Id == caseId);
            var tasks = await _context.Tasks.Where(t => t.CaseId == caseId).ToListAsync();

            var newTemplate = new Template
            {
                Name = "Szablon: " + caseModel.Name,
                Order = caseModel.Order,
                CaseName = caseModel.Name,
                PriorityId = caseModel.PriorityId,
                ClientId = caseModel.ClientId,
                TimeEstimated = caseModel.TimeEstimated,
                CaseTagParentId = caseModel.CaseTagParentId,
                CaseTagChildId = caseModel.CaseTagChildId,
                RepresentativeId = caseModel.RepresentativeId,
                CasesCount = 1,
            };
            _context.Templates.Add(newTemplate);
            await _context.SaveChangesAsync();

            var nameCount = 0;
            var newTaskTemplates = new List<TaskTemplate>();

            foreach (var task in tasks)
            {
                newTaskTemplates.Add(new TaskTemplate
                {
                    Name = "Szablon: " + task.Name,
                    TaskName = task.Name,
                    TemplateId = newTemplate.Id,
                    PriorityId = task.PriorityId,
                    TimeEstimated = task.TimeEstimated,
                    TaskTagMainId = task.TaskTagMainId,
                    TaskTagAccessoryId = task.TaskTagAccessoryId,
                    TaskTagAccessoryMinorId = task.TaskTagAccessoryMinorId,
                    CaseTagId = caseModel.CaseTagParentId.Value,
                    NameCount = nameCount,
                });
                nameCount++;
            }

            newTemplate.TaskTemplates = newTaskTemplates;

            _context.TaskTemplates.AddRange(newTaskTemplates);
            await _context.SaveChangesAsync();
            return newTemplate;
        }

        public async Task<TaskTemplate> CopyTaskToTaskTemplate(int taskId)
        {
            var task = await _context.Tasks.FirstAsync(t => t.Id == taskId);
            var caseModel = await _context.Cases.FirstAsync(c => c.Id == task.CaseId);
            var newTaskTemplate = new TaskTemplate
            {
                Name = "Szablon: " + task.Name,
                TaskName = task.Name,
                PriorityId = task.PriorityId,
                TimeEstimated = task.TimeEstimated,
                TaskTagMainId = task.TaskTagMainId,
                TaskTagAccessoryId = task.TaskTagAccessoryId,
                TaskTagAccessoryMinorId = task.TaskTagAccessoryMinorId,
                CaseTagId = caseModel.CaseTagParentId.Value,
                NameCount = 0,
            };

            await _context.AddAsync(newTaskTemplate);
            await _context.SaveChangesAsync();
            return newTaskTemplate;
        }
    }
}