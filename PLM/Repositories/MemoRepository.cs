using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PLM.Dtos;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    public class MemoRepository : IMemoRepository
    {
        private readonly PLMContext _context;
        private readonly PLMDeletedContext _deletedContext;
        private IEmailRepository _emailRepository;
        public MemoRepository(PLMContext context, PLMDeletedContext deletedContext, IEmailRepository emailRepository)
        {
            _context = context;
            _deletedContext = deletedContext;
            _emailRepository = emailRepository;
        }

        public async Task<Memo> GetMemo(int memoId)
        {
            return await _context.Memos
                .Include(m => m.Assignee)
                .Include(m => m.Reporter)
                .SingleAsync(x => x.Id == memoId);
        }

        public async Task<Memo> AddNewMemo(Memo memo)
        {


            switch (memo.Type)
            {
                case "Case":
                    var caseModel = await _context.Cases.FirstAsync(c => c.Id == memo.ParentId);
                    caseModel.MemoCount = +1;
                    memo.FolderId = caseModel.FolderId + "//notatka" + caseModel.MemoCount.ToString();
                    break;
                case "Task":
                    var taskModel = await _context.Tasks.FirstAsync(c => c.Id == memo.ParentId);
                    taskModel.MemoCount = +1;
                    memo.FolderId = taskModel.FolderId + "//notatka" + taskModel.MemoCount.ToString();
                    break;
            }

            await _context.Memos.AddAsync(memo);
            await _context.SaveChangesAsync();

            return await _context.Memos
                .Include(m => m.Assignee)
                .Include(m => m.Reporter)
                .SingleAsync(x => x.Id == memo.Id);
        }

        public async Task<Memo> UpdateMemo(Memo memo)
        {
            var editedMemo = await _context.Memos
                .Include(m => m.Assignee)
                .Include(m => m.Reporter)
                .Include(m => m.Group)
                .SingleAsync(x => x.Id == memo.Id);

            editedMemo.AssigneeId = memo.AssigneeId;
            editedMemo.ReporterId = memo.ReporterId;
            editedMemo.GroupId = memo.GroupId;
            editedMemo.ParentId = memo.ReporterId;
            editedMemo.Description = memo.Description;
            editedMemo.Time = memo.Time;
            editedMemo.Title = memo.Title;
            editedMemo.Type = memo.Type;

            await _context.SaveChangesAsync();
            return editedMemo;
        }

        public async Task<IEnumerable<Memo>> GetMemosForParent(int parentId, string memoType)
        {
            return await _context.Memos
            .Include(m => m.Assignee)
            .Include(m => m.Reporter)
            .Where(x => x.Type == memoType)
            .Where(x => x.ParentId == parentId)
            .ToArrayAsync();
        }

        public async Task<IEnumerable<Memo>> GetMemosForUser(int userId)
        {
            return await _context.Memos
                .Where(x => x.Assignee.Id == userId)
                .ToArrayAsync();
        }
        public async Task<IEnumerable<Memo>> GetMemosForParentForUser(int parentId, string type, int userId)
        {
            return await _context.Memos
            .Where(x => x.Type == type)
            .Where(x => x.ParentId == parentId)
            .Where(x => x.Assignee.Id == userId)
            .ToArrayAsync();
        }

        public async Task<Memo> DeleteMemo(Memo memo)
        {
            var deletedMemo = await _context.Memos.FirstAsync(m => m.Id == memo.Id);
            _context.Attach(deletedMemo);
            _context.Remove(deletedMemo);
            switch (memo.Type)
            {
                case "Case":
                    var caseModel = await _context.Cases.FirstAsync(c => c.Id == memo.ParentId);
                    caseModel.MemoCount = -1;
                    break;
                case "Task":
                    var taskModel = await _context.Tasks.FirstAsync(c => c.Id == memo.ParentId);
                    taskModel.MemoCount = -1;
                    break;
            }
            _context.SaveChanges();
            return deletedMemo;
        }
    }
}