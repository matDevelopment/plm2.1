using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PLM.Dtos;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    public class FilterRepository : IFilterRepository
    {
        private PLMContext _context;
        public FilterRepository(PLMContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Priority>> GetPriorities()
        {
            return await _context.Priorites.ToListAsync();
        }

        public async Task<IEnumerable<Status>> GetStatuses()
        {
            return await _context.Statuses.ToListAsync();
        }

        public async Task<FilterNamesDto> GetFilters(FilterDto filter) {
            var filterNames = new FilterNamesDto();
            if (filter.CaseId != null)
                filterNames.Case = (await _context.Cases.FirstAsync(c => c.Id == filter.CaseId)).Name;
            if (filter.CaseTagChildId != null)
                filterNames.CaseTagChild = (await _context.CaseTagChildren.FirstAsync(c => c.Id == filter.CaseTagChildId)).Name;
            if (filter.CaseTagParentId != null)
                filterNames.CaseTagParent = (await _context.CaseTagParents.FirstAsync(c => c.Id == filter.CaseTagParentId)).Name;
            if (filter.Date != null)
                filterNames.Date = filter.Date;
            if (filter.EcpDate != null)
                filterNames.EcpDate = filter.EcpDate;
            if (filter.StartDate != null)
                filterNames.StartDate = filter.StartDate;
            if (filter.EndDate != null)
                filterNames.EndDate = filter.EndDate;
            if (filter.StartTime != null)
                filterNames.StartTime = filter.StartTime;
            if (filter.FinishTime != null)
                filterNames.FinishTime = filter.FinishTime;
            if (filter.TimeEstimated != null)
                filterNames.TimeEstimated = filter.TimeEstimated;
            if (filter.TimeTaken != null)
                filterNames.TimeTaken = filter.TimeTaken;
            if (filter.UserId != null)
                filterNames.User = (await _context.Users.FirstAsync(u => u.Id == filter.UserId)).Name;
            if (filter.GroupId != null)
                filterNames.Group = (await _context.Groups.FirstAsync(g => g.Id == filter.GroupId)).Name;
            if (filter.TaskId != null)
                filterNames.Task = (await _context.Tasks.FirstAsync(t => t.Id == filter.TaskId)).Name;
            if (filter.TaskUserId != null)
                filterNames.TaskUser = (await _context.Users.FirstAsync(u => u.Id == filter.TaskUserId)).Name;
            if (filter.UserName != null)
                filterNames.UserName = filter.UserName;
            if (filter.TaskTagMainId != null)
                filterNames.TaskTagMain = (await _context.TaskTagsMain.FirstAsync(t => t.Id == filter.TaskTagMainId)).Name;
            if (filter.TaskTagAccessoryId != null)
                filterNames.TaskTagAccessory = (await _context.TaskTagsAccessory.FirstAsync(t => t.Id == filter.TaskTagAccessoryId)).Name;
            if (filter.TaskTagAccessoryMinorId != null)
                filterNames.TaskTagAccessoryMinor = (await _context.TaskTagsAccessoryMinor.FirstAsync(t => t.Id == filter.TaskTagAccessoryMinorId)).Name;
            if (filter.TeamLeaderId != null)
                filterNames.TeamLeader = (await _context.Users.FirstAsync(u => u.Id == filter.TeamLeaderId)).Name;
            if (filter.LastSeenUser != null)
                filterNames.LastSeenUser = (await _context.Users.FirstAsync(u => u.Id == filter.LastSeenUser)).Name;
            return filterNames;
        }   

    }
}