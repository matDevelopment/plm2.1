using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.Helpers;
using PLM.IHelpers;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly PLMContext _context;
        private IEmailRepository _emailRepository;
        private readonly IQueryHelper _queryHelper;

        public TaskRepository(PLMContext context, IEmailRepository emailRepository, IQueryHelper queryHelper)
        {
            _context = context;
            _emailRepository = emailRepository;
            _queryHelper = queryHelper;
        }

        public async Task<TaskDto> AddTask(TaskDto task)
        {
            string order = null;
            var taskForAdd = new Models.Task
            {
                Name = task.Name,
                Description = task.Description,
                StatusId = task.StatusId,
                Order = task.Order,
                PriorityId = task.PriorityId,
                CaseId = task.CaseId,
                GroupId = task.GroupId,
                StartDate = task.StartDate,
                EndDate = task.EndDate,
                TimeEstimated = task.TimeEstimated,
                TimeTaken = task.TimeTaken,
                TaskTagMainId = task.TaskTagMainId,
                TaskTagAccessoryId = task.TaskTagAccessoryId,
                TaskTagAccessoryMinorId = task.TaskTagAccessoryMinorId,
                DestinatedEmployees = task.DestinatedEmployees,
            };

            var _case = await _context.Cases.FirstOrDefaultAsync(c => c.Id == taskForAdd.CaseId);
            if (String.IsNullOrWhiteSpace(_case.Order))
                taskForAdd.Order = null;
            _case.TasksCount += 1;
            _case.TasksTime += (int)taskForAdd.TimeEstimated;
            taskForAdd.FolderId = _case.FolderId + "//Zadanie" + _case.TasksCount;
            _context.Tasks.Add(taskForAdd);
            await _context.SaveChangesAsync();

            foreach(var user in task.Users) {
                await _context.TasksUsers.AddAsync(new TaskUser{ TaskId = taskForAdd.Id, UserId = user.Id});
            }

            await _context.SaveChangesAsync();

            var newTask = await _context.Tasks
            .Include(t => t.Case)
            .Include(t => t.Group)
            .Include(t => t.TasksUsers)
            .Include(t => t.Case.Client)
            .Include(t => t.Case.Priority)
            .Include(t => t.Case.Status)
            .Include(t => t.Case.User)
            .Include(t => t.Case.Group)
            .Include(t => t.Case.CaseTagParent)
            .Include(t => t.Case.CaseTagChild)
            .Include(t => t.Case.Representative)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority)
            .Include(t => t.Status)
            .FirstAsync(c => c.Id == taskForAdd.Id);

            return _queryHelper.GetUsersForTask(newTask);
        }

        public async Task<Models.Task> GetTaskById(int id)
        {
            return await _context.Tasks
                       .Include(t => t.Case)
            .Include(t => t.Group)
            .Include(t => t.TasksUsers)
            .Include(t => t.Case.Client)
            .Include(t => t.Case.Priority)
            .Include(t => t.Case.Status)
            .Include(t => t.Case.User)
            .Include(t => t.Case.Group)
            .Include(t => t.Case.CaseTagParent)
            .Include(t => t.Case.CaseTagChild)
            .Include(t => t.Case.Representative)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority)
            .Include(t => t.Status)
            .FirstAsync(t => t.Id == id);
        }
        public async Task<TaskDto> EditTask(TaskDto task)
        {
            var editedTask = await _context.Tasks
            .Include(t => t.Case)
            .Include(t => t.Group)
            .Include(t => t.TasksUsers)
            .Include(t => t.Case.Client)
            .Include(t => t.Case.Priority)
            .Include(t => t.Case.Status)
            .Include(t => t.Case.User)
            .Include(t => t.Case.Group)
            .Include(t => t.Case.CaseTagParent)
            .Include(t => t.Case.CaseTagChild)
            .Include(t => t.Case.Representative)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority)
            .Include(t => t.Status)
            .FirstAsync(c => c.Id == task.Id);
            int taskTimeEstimated = (int)editedTask.TimeEstimated;
            var caseToSubtractTime = await _context.Cases.FirstOrDefaultAsync(c => c.Id == editedTask.CaseId);
            caseToSubtractTime.TasksTime -= taskTimeEstimated;
            editedTask.Name = task.Name;
            editedTask.GroupId = task.GroupId;
            editedTask.Description = task.Description;
            editedTask.StatusId = task.StatusId;
            editedTask.PriorityId = task.PriorityId;
            editedTask.StartDate = task.StartDate;
            editedTask.EndDate = task.EndDate;
            editedTask.TimeEstimated = task.TimeEstimated;
            editedTask.TimeTaken = task.TimeTaken;
            editedTask.TaskTagMainId = task.TaskTagMainId;
            editedTask.TaskTagAccessoryId = task.TaskTagAccessoryId;
            editedTask.TaskTagAccessoryMinorId = task.TaskTagAccessoryMinorId;
            foreach(var taskUser in await _context.TasksUsers.Where(tu => tu.TaskId == task.Id).ToListAsync()) {
                _context.TasksUsers.Attach(taskUser);
                _context.TasksUsers.Remove(taskUser);
            }

            foreach(var user in task.Users) {
                await _context.TasksUsers.AddAsync(new TaskUser { UserId = user.Id, TaskId = task.Id});
            };

            var caseToAddTime = await _context.Cases.FirstOrDefaultAsync(c => c.Id == editedTask.CaseId);
            caseToAddTime.TasksTime += (int)editedTask.TimeEstimated;
            await _context.SaveChangesAsync();

            return _queryHelper.GetUsersForTask(await _context.Tasks
            .Include(t => t.Case)
            .Include(t => t.Group)
            .Include(t => t.TasksUsers)
            .Include(t => t.Case.Client)
            .Include(t => t.Case.Priority)
            .Include(t => t.Case.Status)
            .Include(t => t.Case.User)
            .Include(t => t.Case.Group)
            .Include(t => t.Case.CaseTagParent)
            .Include(t => t.Case.CaseTagChild)
            .Include(t => t.Case.Representative)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority)
            .Include(t => t.Status)
            .FirstAsync(c => c.Id == task.Id));
        }
        public async Task<List<TaskDto>> GetFilteredTasks(FilterDto taskFilter)
        {
            return await _queryHelper.GetFilteredTasks(taskFilter);
        }

        public async Task<IEnumerable<TaskDto>> GetTasks()
        {
            var tasks =  await _context.Tasks.Include(t => t.Case)
            .Include(t => t.Case)
            .Include(t => t.Group)
            .Include(t => t.TasksUsers)
            .Include(t => t.Case.Client)
            .Include(t => t.Case.Priority)
            .Include(t => t.Case.Status)
            .Include(t => t.Case.User)
            .Include(t => t.Case.Group)
            .Include(t => t.Case.CaseTagParent)
            .Include(t => t.Case.CaseTagChild)
            .Include(t => t.Case.Representative)
            .Include(t => t.TaskTagMain)
            .Include(t => t.TaskTagAccessory)
            .Include(t => t.TaskTagAccessoryMinor)
            .Include(t => t.Priority)
            .Include(t => t.Status)
            .Include(t => t.Status).ToListAsync();

            return _queryHelper.GetUsersForTasks(tasks);
        }
        public async Task<TaskDto> RemoveTask(TaskDto task)
        {
            var lastSeen = await _context.LastSeen.FirstOrDefaultAsync(c => c.TaskId == task.Id);
            if (lastSeen != null) {
                _context.LastSeen.Attach(lastSeen);
                _context.LastSeen.Remove(lastSeen);
            }
            var taskToRemove = await _context.Tasks.FirstAsync(c => c.Id == task.Id);
            _context.Tasks.Attach(taskToRemove);
            var caseToSubtractTime = await _context.Cases.FirstOrDefaultAsync(c => c.Id == taskToRemove.CaseId);
            caseToSubtractTime.TasksCount -= 1;
            caseToSubtractTime.TasksTime -= (int)taskToRemove.TimeEstimated;
            _context.Tasks.Remove(taskToRemove);
            _context.SaveChanges();
            return task;
        }

        public async Task<IEnumerable<Models.Task>> GetTaskForEcp(DateTime date)
        {
            var ecps = await _context.Ecps.Where(e => e.Date == date).ToListAsync();
            var tasks = new List<Models.Task>();
            foreach (var ecp in ecps)
            {
                if (ecp.TaskId != null)
                    tasks.AddRange(await _context.Tasks.Where(t => t.Id == ecp.TaskId).ToListAsync());
            }
            return tasks;
        }

        public async Task<Models.Task> ChangeStatus(Models.Task task)
        {
            var caseToEdit = await _context.Tasks.Include(t => t.Status).FirstAsync(t => t.Id == task.Id);
            caseToEdit.StatusId = task.StatusId;
            await _context.SaveChangesAsync();
            return await _context.Tasks.Include(c => c.Status).FirstAsync(c => c.Id == task.Id);
        }

        public async Task<Models.Task> SetBimFolderId(int taskId, string FolderId) {
            var task = await _context.Tasks.FirstAsync(t => t.Id == taskId);
            task.BimFolderId = FolderId;
            await _context.SaveChangesAsync();
            return task;
        }
    }
}