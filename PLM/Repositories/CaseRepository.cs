using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using PLM.Dtos;
using PLM.Helpers;
using PLM.IHelpers;
using PLM.IRepositories;
using PLM.Models;

namespace PLM.Repositories
{
    public class CaseRepository : ICaseRepository
    {
        private readonly PLMContext _context;
        private readonly PLMDeletedContext _deletedContext;
        private readonly IQueryHelper _queryHelper;
        private readonly IHostingEnvironment _env;

        public CaseRepository(PLMContext context, PLMDeletedContext deletedContext, IQueryHelper queryHelper, IHostingEnvironment env)
        {
            _context = context;
            _deletedContext = deletedContext;
            _queryHelper = queryHelper;
            _env = env;
        }
        public async Task<Case> AddNewCase(Case _case)
        {
            var caseToAdd = new Case
            {
                Name = _case.Name,
                Order = _case.Order,
                Description = _case.Description,
                StatusId = _case.StatusId,
                PriorityId = _case.PriorityId,
                ClientId = _case.ClientId,
                UserId = _case.UserId,
                GroupId = _case.GroupId,
                StartDate = _case.StartDate,
                EndDate = _case.EndDate,
                TimeEstimated = _case.TimeEstimated,
                TimeTaken = _case.TimeTaken,
                CaseTagParentId = _case.CaseTagParentId,
                CaseTagChildId = _case.CaseTagChildId,
                NotificationId = _case.NotificationId,
                TemplateId = _case.TemplateId
            };
            await _context.Cases.AddAsync(caseToAdd);
            if (_case.TemplateId != null)
            {
                var template = await _context.Templates.FirstAsync(t => t.Id == _case.TemplateId);
                template.CasesCount += 1;
            }

            var folderId = await _context.FolderCount.FirstAsync();
            folderId.Amount = folderId.Amount + 1;
            caseToAdd.FolderId = "projekt" + folderId.Amount.ToString();
            await _context.SaveChangesAsync();

            return await _context.Cases
            .Include(c => c.Client)
            .Include(c => c.Group)
            .Include(c => c.User)
            .Include(c => c.CaseTagChild)
            .Include(c => c.CaseTagParent)
            .Include(c => c.Status)
            .Include(c => c.Representative)
            .Include(c => c.Priority).FirstAsync(c => c.Id == caseToAdd.Id);
        }
        public async Task<Case> EditCase(Case @case)
        {
            var editedCase = await _context.Cases.SingleAsync(c => c.Id == @case.Id);
            _context.Entry(editedCase).CurrentValues.SetValues(@case);
            await _context.SaveChangesAsync();

            return await _context.Cases.Include(c => c.Client)
                .Include(c => c.Priority)
                .Include(c => c.Status)
                .Include(c => c.User)
                .Include(c => c.Group)
                .Include(c => c.Representative)
                .Include(c => c.CaseTagParent)
                .Include(c => c.CaseTagChild)
                .FirstAsync(c => c.Id == @case.Id);
        }

        public async Task<Case> GetCase(int id)
        {
            var tmcase = await _context.Cases.Include(c => c.Client)
               .Include(c => c.Priority)
               .Include(c => c.Status)
               .Include(c => c.User)
               .Include(c => c.Group)
               .Include(c => c.Representative)
               .Include(c => c.CaseTagParent)
               .Include(c => c.CaseTagChild)
               .FirstAsync(c => c.Id == id);

            return tmcase;
        }

        public async Task<IEnumerable<Case>> GetCases()
        {
            return await _context.Cases.Include(c => c.Client)
                .Include(c => c.Priority)
                .Include(c => c.Status)
                .Include(c => c.User)
                .Include(c => c.Group)
                .Include(c => c.Representative)
                .Include(c => c.CaseTagParent)
                .Include(c => c.CaseTagChild).ToListAsync();
        }

        public async Task<List<Case>> GetFilteredCases(FilterDto caseFilter)
        {
            return await _queryHelper.GetFilteredCases(caseFilter);
        }

        public async Task<Case> SetBimFolderId(int caseId, string FolderId) {
           var caseModel = await _context.Cases.FirstAsync(c => c.Id == caseId);
           var tasks = await _context.Tasks.Where(t =>  t.CaseId == caseId).ToListAsync();
           foreach (var task in tasks) {
               if (task.FolderId != null) {
                   task.BimFolderId = FolderId;
               }
           }
           caseModel.BimFolderId = FolderId;
           await _context.SaveChangesAsync();
           return caseModel;
        }

        public async Task<Case> RemoveCase(Case @case)
        {
            var lastSeen = await _context.LastSeen.FirstOrDefaultAsync(c => c.CaseId == @case.Id);
            if (lastSeen != null) {
                _context.LastSeen.Attach(lastSeen);
                _context.LastSeen.Remove(lastSeen);
            }

            var notifications = await _context.Notifications.Where(n => n.CaseId == @case.Id).ToListAsync();
            foreach (var notification in notifications) {
                _context.Notifications.Attach(notification);
                _context.Notifications.Remove(notification);
            }
             _context.SaveChanges();
            var caseToRemove = await _context.Cases.FirstAsync(c => c.Id == @case.Id);
            _context.Cases.Attach(caseToRemove);
            _context.Cases.Remove(caseToRemove);
            if (@case.TemplateId != null)
            {
                var template = await _context.Templates.FirstAsync(t => t.Id == @case.TemplateId);
                template.CasesCount -= 1;
            }
            _context.SaveChanges();
            return @case;
        }

        public async Task<Case> ChangeStatus(Case @case)
        {
            var caseToEdit = await _context.Cases.Include(c => c.Status).FirstAsync(c => c.Id == @case.Id);
            caseToEdit.StatusId = @case.StatusId;
            if (@case.StatusId == 3)
            {
                var tasks = await _context.Tasks.Where(t => t.CaseId == @case.Id).ToListAsync();
                foreach (var task in tasks)
                {
                    task.StatusId = 3;
                }
            }
            await _context.SaveChangesAsync();
            return await _context.Cases.Include(c => c.Status).FirstAsync(c => c.Id == @case.Id);
        }
    }
}