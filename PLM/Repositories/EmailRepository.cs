using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PLM.IRepositories;
using PLM.Models;
using PLM.Helpers;
using System.Net;
using System.Net.Mail;
using PLM.BaseModels;
using PLM.DtosModels;

namespace PLM.Repositories
{
    public class EmailRepository : IEmailRepository
    {
        private PLMContext _context;

        public EmailRepository(PLMContext context)
        {
            _context = context;
        }

        public string GetIPAddress()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[1];

            return ipAddress.ToString();
        }
        public void CreateEmail(string subject, string messageBody, User user)
        {
            
            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            message.To.Add(user.Email);
            message.Subject = subject;
            message.From = new MailAddress("testusermatnet@gmail.com");
            message.Body = messageBody;
            SendEmail(message);
        }

        public void CreateEmail(string subject, string messageBody, IEnumerable<UserDto> users) {
            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            foreach(var user in users) {
                message.To.Add(user.Email);
            }
            message.Subject = subject;
            message.From = new MailAddress("testusermatnet@gmail.com");
            message.Body = messageBody;
            SendEmail(message);
        }

        public void SendEmail(MailMessage message)
        {
            object userToken = new object();
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("testusermatnet", "!!qaz123")
            };
            smtp.SendAsync(message, userToken);
        }
        public void OutlookCalendar()
        {
            Microsoft.Office.Interop.Outlook.Application outlookApp = new Microsoft.Office.Interop.Outlook.Application();
            Microsoft.Office.Interop.Outlook.AppointmentItem appt = outlookApp.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olAppointmentItem)
                as Microsoft.Office.Interop.Outlook.AppointmentItem;
            appt.Subject = "Customer Review";
            appt.MeetingStatus = Microsoft.Office.Interop.Outlook.OlMeetingStatus.olMeeting;
            appt.Location = "36/2021";
            appt.Start = DateTime.Parse("04/04/2019 10:00 AM");
            appt.End = DateTime.Parse("04/10/2019 11:00 AM");
            appt.Body = "Testing";
            appt.Display(false);
        }
    }
}