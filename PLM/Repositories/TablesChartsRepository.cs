using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using PLM.Dtos;
using PLM.DtosModels;
using PLM.Helpers;
using PLM.IHelpers;
using PLM.IRepositories;
using PLM.Models;


namespace PLM.Repositories
{
    public class TablesChartsRepository : ITablesChartsRepository
    {
        private readonly PLMContext _context;
        private readonly PLMDeletedContext _deletedContext;
        private readonly IMapper _mapper;
        private readonly IQueryHelper _queryHelper;

        public TablesChartsRepository(PLMContext context, PLMDeletedContext deletedContext, IMapper mapper, IQueryHelper queryHelper)
        {
            _context = context;
            _deletedContext = deletedContext;
            _mapper = mapper;
            _queryHelper = queryHelper;
        }
        public async Task<List<User>> GetUsersForCase(int caseId, int? userId, int? groupId)
        {
            List<User> users = new List<User>();
            List<TaskUser> filteredUsers = new List<TaskUser>();
            var allTasks = await _context.Tasks.Include(t => t.TasksUsers).Where(t => t.CaseId == caseId).ToListAsync();
            foreach (var allTask in allTasks)
            {
                filteredUsers.AddRange(allTask.TasksUsers.Where(tu => filteredUsers.FirstOrDefault(u => u.UserId == tu.UserId) == null).ToList());
            }

            foreach (var taskUser in filteredUsers)
            {
                users.Add(await _context.Users.FirstAsync(tu => tu.Id == taskUser.UserId));
            }

            return users;
        }

        public async Task<List<Models.Task>> GetTasksForUser(FilterDto subFilter)
        {
            List<Models.Task> tasks = new List<Models.Task>();
            var allTasks = await _queryHelper.GetFilteredTasks(subFilter);
            return tasks;

        }

        public async Task<UserTaskEcpDto> GetUserWithTasks(FilterDto filter, FilterDto subFilter, User _user)
        {
            var userTaskEcp = new UserTaskEcpDto();
            userTaskEcp.User = _mapper.Map<UserDto>(_user);
            subFilter.UserId = _user.Id;
            userTaskEcp.Tasks = await _queryHelper.GetFilteredTasks(subFilter);
            userTaskEcp.Ecps = new List<EcpDto>();
            foreach (var task in userTaskEcp.Tasks)
            {
                var ecps = _mapper.Map<List<EcpDto>>(await _queryHelper.GetEcps(new FilterDto
                { StartTime = filter.StartTime, FinishTime = filter.FinishTime, TaskId = task.Id }));
                foreach (var ecp in ecps)
                {
                    if (userTaskEcp.Ecps.FirstOrDefault(e => e.Id == ecp.Id) == null)
                    {
                        userTaskEcp.Ecps.Add(ecp);
                    }
                }
            }
            return userTaskEcp;

        }
        public async Task<List<UserTaskEcpDto>> GetUsersWithTasks(FilterDto filter, FilterDto subFilter)
        {
            var usersToReturn = new List<UserTaskEcpDto>();
            var users = new List<User>();
            if (filter.UserId == null && filter.GroupId == null)
                users = await _context.Users.ToListAsync();
            else if (filter.UserId != null && filter.GroupId == null)
                users = await _context.Users.Where(u => u.Id == filter.UserId).ToListAsync();
            else if (filter.UserId == null && filter.GroupId != null)
            {
                var usersInGroup = await _queryHelper.GetUsersInGroup(filter.GroupId.Value);
                foreach (var userInGroup in usersInGroup)
                {
                    if (users.FirstOrDefault(u => u.Id == userInGroup.Id) == null)
                        users.Add(userInGroup);
                }
            }
            foreach (var _user in users)
            {
                usersToReturn.Add(await GetUserWithTasks(filter, subFilter, _user));
            }
            return usersToReturn;
        }
        public async Task<List<CaseTasksDto>> GetCasesWithTasks(FilterDto filter, FilterDto subFilter)
        {
            List<TaskDto> allTasks = new List<TaskDto>();
            List<CaseTasksDto> casesTasks = new List<CaseTasksDto>();
            allTasks = await _queryHelper.GetFilteredTasks(subFilter);

            var cases = await _queryHelper.GetFilteredCases(filter);
            foreach (var _case in cases)
            {
                var caseTasks = new CaseTasksDto
                {
                    Case = _mapper.Map<CaseDto>(_case),
                    Tasks = new List<TaskDto>(),
                    Ecps = new List<EcpDto>()
                };
                foreach (var task in allTasks)
                {
                    if (task.CaseId == _case.Id)
                    {
                        caseTasks.Tasks.Add(_mapper.Map<TaskDto>(task));
                        filter.TaskId = task.Id;
                        caseTasks.Ecps.AddRange(_mapper.Map<List<EcpDto>>(await _queryHelper.GetEcps(new FilterDto
                        { StartTime = filter.StartTime, FinishTime = filter.FinishTime, CaseId = filter.CaseId, TaskId = filter.TaskId })));
                    }
                }
                casesTasks.Add(caseTasks);
            }
            return casesTasks;
        }

        public async Task<List<CaseUserEcpDto>> GetCasesWithUsers(FilterDto filter, FilterDto subFilter)
        {
            var cases = new List<CaseUserEcpDto>();
            foreach (var _case in await _queryHelper.GetFilteredCases(filter))
            {
                cases.Add(await GetCaseWithUsers(filter, subFilter, _case));
            }
            return cases;
        }

        public async Task<CaseUserEcpDto> GetCaseWithUsers(FilterDto filter, FilterDto subFilter, Case _case)
        {
            var caseUserEcp = new CaseUserEcpDto();
            caseUserEcp.Case = _mapper.Map<CaseDto>(_case);
            caseUserEcp.Users = _mapper.Map<List<UserDto>>(await GetUsersForCase(_case.Id, subFilter.UserId, subFilter.GroupId));
            caseUserEcp.Ecps = new List<EcpDto>();
            foreach (var user in caseUserEcp.Users)
            {
                var ecps = _mapper.Map<List<EcpDto>>(await _queryHelper.GetEcps(new FilterDto
                {
                    StartTime = filter.StartTime,
                    FinishTime = filter.FinishTime,
                    CaseId = _case.Id,
                    UserId = user.Id
                }));
                foreach (var ecp in ecps)
                {
                    if (caseUserEcp.Ecps.FirstOrDefault(e => e.Id == ecp.Id) == null)
                    {
                        caseUserEcp.Ecps.Add(ecp);
                    }
                }
            }
            return caseUserEcp;
        }

        public async Task<List<GroupWithAllUsersDto>> GetUsersForGroups(FilterDto filter, FilterDto subfilter)
        {
            var groups = new List<GroupWithAllUsersDto>();
            foreach (var _group in await _context.Groups.ToListAsync())
            {
                groups.Add(await GetUsersWithGroup(filter, subfilter, _group.Id));
            }

            return groups;
        }

        public async Task<GroupWithAllUsersDto> GetUsersWithGroup(FilterDto filter, FilterDto subfilter, int GroupId)
        {
            var groupWithUsers = new GroupWithAllUsersDto();
            groupWithUsers.Ecps = _mapper.Map<List<EcpDto>>(await _queryHelper.GetEcps(filter));
            groupWithUsers.Group = _mapper.Map<GroupDto>(await _context.Groups.FirstOrDefaultAsync(g => g.Id == GroupId));
            groupWithUsers.Users = _mapper.Map<List<UserDto>>(await _queryHelper.getUsersInGroup(GroupId));
            groupWithUsers.TeamLeader = _mapper.Map<UserDto>(await _context.Users.FirstOrDefaultAsync(u => u.Id == groupWithUsers.Group.TeamLeaderId));

            return groupWithUsers;
        }

        public async Task<byte[]> ExportJson(string json, IHostingEnvironment env)
        {
            string webRootFolder = ValuesHelper.FileDirectory;
            string fileName = @"exceljson.xlsx";
            FileInfo file = new FileInfo(Path.Combine(webRootFolder, fileName));
            byte[] resultPackage = null;
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(webRootFolder, fileName));
                ExcelPackage _package = new ExcelPackage(file);
            }
            else
            {
                file = new FileInfo(Path.Combine(webRootFolder, fileName));
                ExcelPackage _package = new ExcelPackage(file);
            }
            var jObjects = JObject.Parse(json);



            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("PLM");

                ExcelRange mainHeader = worksheet.Cells[1, 1, 1, jObjects["Names"].Children().Count()];
                mainHeader.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                mainHeader.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(212, 212, 212)); ;

                var mainHeaderColumn = 1;
                var mainHeaderRow = 1;

                foreach (var jObjectName in jObjects["Names"].Children())
                {
                    worksheet.SetValue(mainHeaderRow, mainHeaderColumn, jObjectName.ToString());
                    mainHeaderColumn++;
                }

                mainHeaderColumn = 1;
                mainHeaderRow++;

                foreach (var jObject in jObjects["Data"].Children())
                {
                    foreach (var jObjectName in jObjects["ObjectNames"].Children())
                    {
                        ExcelRange modelHeader = worksheet.Cells[mainHeaderRow, mainHeaderColumn, mainHeaderRow, jObjects["Names"].Children().Count()];
                        modelHeader.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        modelHeader.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(240, 240, 240));
                        worksheet.SetValue(mainHeaderRow, mainHeaderColumn, jObject[jObjectName.ToString()].ToString()); mainHeaderColumn++;
                    }

                    try
                    {
                        var result = jObjects["SubNames"];
                        mainHeaderColumn = 2;
                        mainHeaderRow++;
                        foreach (var jObjectName in jObjects["SubNames"].Children())
                        {
                            ExcelRange subHeader = worksheet.Cells[mainHeaderRow, mainHeaderColumn, mainHeaderRow, jObjects["SubNames"].Children().Count() + 1];
                            subHeader.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            subHeader.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(212, 212, 212));
                            worksheet.SetValue(mainHeaderRow, mainHeaderColumn, jObjectName.ToString());
                            mainHeaderColumn++;
                        }

                        mainHeaderColumn = 2;
                        mainHeaderRow++;

                        foreach (var jSubObject in jObject["SubModels"].Children())
                        {
                            foreach (var jSubObjectName in jObjects["ObjectSubNames"].Children())
                            {
                                worksheet.SetValue(mainHeaderRow, mainHeaderColumn, jSubObject[jSubObjectName.ToString()].ToString()); mainHeaderColumn++;
                            }
                            mainHeaderColumn = 2;
                            mainHeaderRow++;
                        }
                        mainHeaderColumn = 1;
                    }
                    catch
                    {

                    }

                    mainHeaderRow++;
                }

                string WorksheetStart = worksheet.Dimension.Start.Address;
                string WorksheetEnd = worksheet.Dimension.End.Address;
                worksheet.Cells[WorksheetStart + ":" + WorksheetEnd].AutoFitColumns();

                package.Save();
                resultPackage = package.GetAsByteArray();
                return resultPackage;
            }
        }
    }
}



